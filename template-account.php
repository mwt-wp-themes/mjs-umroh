<?php
/**
 * Template name: Account
 */

global $mwt, $mwt_option;
if( !is_user_logged_in() ) {
  wp_redirect( get_permalink( $mwt_option['login-page-id'] ) );
}

$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : 'profile';
$user_info = get_userdata( get_current_user_id() );

// // Set the image size. Accepts all registered images sizes and array(int, int)
// $size = 'thumbnail';
// // Get the image URL using the author ID and image size params
// $avatar_url = get_cupp_meta( $user_info->ID, $size );
// if( empty( $avatar_url ) )
//   $avatar_url = get_avatar_url( $user_info->ID );
// Mwt_Mjs::user_update_profile_action();
$role = implode(', ', $user_info->roles);
$role = strtolower( str_replace( ' ', '_', $role ) );
if( $role == 'staf_cs' )  {
 $role = 'staf_mjs'; 
} elseif( $role == "mitra" ) {
  $path = parse_url(get_option('siteurl'), PHP_URL_PATH);
  $host = parse_url(get_option('siteurl'), PHP_URL_HOST);
  $expiry = strtotime('+1 month');
  setcookie( '_mjs_mitra', $user_info->ID, $expiry, $path, $host );
}

get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi"><!--primary-->
		<main id="main" class="site-main batasna_kabeh halaman">
      
      <div class="batasna_wrap">

          <div class="breadcumb">
              <div class="clr"></div>

              <!-- PERUBAHAN 10 April 2016 -->
              <div class="brdcmb_knc">
                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
                        <strong><span itemprop="title">Beranda</span></strong>
                  </a>
                  </span>

                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="#" title="" itemprop="url">/
                        <span itemprop="title">My Account</span>
                  </a>
                  </span>

              </div>

              <div class="brdcmb_kth">
              </div>

              <div class="clr"></div>
          </div>

          <h1><?php echo get_the_title(); ?></h1>
          <div class="separator separator_hideung"><span></span></div>

          <div class="halaman_konten">
              <div class="clr"></div>
            
                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-5">
                      <style>
                      .custom-restricted-width {
                          /* To limit the menu width to the content of the menu: */
                          display: inline-block;
                          /* Or set the width explicitly: */
                          /* width: 10em; */
                      }
                      </style>
                      <div class="pure-menu custom-restricted-width">
              <!--             <span class="pure-menu-heading">My Account</span> -->

                          <ul class="pure-menu-list">
                            <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'profile', get_permalink() ); ?>" class="pure-menu-link">My Profile</a></li>
                            <?php if( $role == "mitra" ) : ?>
                              <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'sales', get_permalink() ); ?>" class="pure-menu-link">My Customer</a></li>
                            <?php elseif( $role == "staf_mjs" ) :?>
                            <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'assignment', get_permalink() ); ?>" class="pure-menu-link">Assignments</a></li>
                            <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'mitra', get_permalink() ); ?>" class="pure-menu-link">Data Mitra</a></li>
                            <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'add-mitra', get_permalink() ); ?>" class="pure-menu-link">Tambah Mitra</a></li>
                            <?php elseif( $role == "staf_spv" || $role == "staf_manager" ) :?>
                            <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'assignment', get_permalink() ); ?>" class="pure-menu-link">Assignments</a></li>
                            <?php endif; ?>
                              <li class="pure-menu-item"><a href="<?php echo add_query_arg( 'section', 'ganti-password', get_permalink() ); ?>" class="pure-menu-link">Ganti Password</a></li>
                              <li class="pure-menu-item"><a href="<?php echo wp_logout_url("/"); ?>" class="pure-menu-link" onclick="return confirm('Yakin ingin logout?');">Logout</a></li>
                          </ul>
                      </div>
                    </div>
                    <div class="pure-u-1 pure-u-md-4-5">

                      <?php 
                      if( in_array( $role, ['mitra', 'staf_mjs', 'staf_spv', 'staf_manager'] ) ) {
                        get_template_part( 'template-parts/content', 'account-' . $role ); 
                      } ?>

                      <?php if( $section == "profile" ) : ?>

                      <div style="text-align:center;margin:1rem">
                        <?php user_update_profile_action(); ?>
                      </div>
                      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" class="pure-form pure-form-aligned">
                          <fieldset>
                              <div class="pure-control-group">
                                  <label for="username">Username</label>
                                  <input id="username" type="text" value="<?php echo $user_info->user_login; ?>" disabled>
                                  <span class="pure-form-message-inline">Username tidak dapat diubah.</span>
                              </div>

                              <div class="pure-control-group">
                                  <label for="first_name">Nama Lengkap</label>
                                  <input id="first_name" type="text" placeholder="Full name" name="first_name" value="<?php echo $user_info->first_name . ' ' . $user_info->last_name; ?>">
                              </div>

                              <div class="pure-control-group">
                                  <label for="email">Email Address</label>
                                  <input id="email" type="email" value="<?php echo $user_info->user_email; ?>" disabled>
                                  <span class="pure-form-message-inline">Email tidak dapat diubah.</span>
                              </div>

                              <div class="pure-control-group">
                                  <label for="nomor_telp">Nomor Telp</label>
                                  <input id="nomor_telp" name="nomor_telp" type="text" value="<?php echo get_user_meta( $user_info->ID, '_nomortelp', true ); ?>">
                              </div>

                              <div class="pure-control-group">
                                  <label for="nomor_hp">Nomor HP</label>
                                  <input id="nomor_hp" name="nomor_hp" type="text" value="<?php echo get_user_meta( $user_info->ID, '_nomorhp', true ); ?>">
                              </div>
                            
                              <?php if( $role == "mitra" ) : ?>
                              <div class="pure-controls">
                              <p>
                                Rekening Bank (Untuk penerimaan komisi)
                                </p><br>
                              </div>
                              <div class="pure-control-group">
                                  <label for="nama_bank">Nama Bank</label>
                                  <input id="nama_bank" name="nama_bank" type="text" value="<?php echo get_user_meta( $user_info->ID, '_nama_bank', true ); ?>">
                              </div>
                              <div class="pure-control-group">
                                  <label for="nomor_rekening">Nomor Rekening</label>
                                  <input id="nomor_rekening" name="nomor_rekening" type="text" value="<?php echo get_user_meta( $user_info->ID, '_norek', true ); ?>">
                              </div>
                              <?php endif; ?>

                              <div class="pure-controls">

                                  <button name="submit" type="submit" class="pure-button pure-button-primary">Submit</button>
                              </div>
                          </fieldset>
                      </form>
                      
                      <?php elseif( $section == "ganti-password" ) : 
                      if( isset( $_POST['submit-password'] ) ) {
                        $user_id = get_current_user_id();
                        $password = $_POST['password'];
                        wp_set_password( $password, $user_id );
                        echo '<div class="alert alert-success">';
                        echo 'Password anda berhasil diganti.';
                        echo '</div>';
                      }
                      ?>
                      
                      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" class="pure-form pure-form-aligned">
                          <fieldset>
                              <div class="pure-control-group">
                                  <label for="password">Password baru</label>
                                  <input id="password" type="password">
                              </div>
                            
                              <div class="pure-control-group">
                                  <label for="confirm_password">Ulangi password</label>
                                  <input id="confirm_password" type="password" name="password">
                                  <span id="message" class="pure-form-message-inline"></span>
                              </div>

                              <div class="pure-controls">
                                  <button id="submit-password" name="submit-password" type="submit" class="pure-button pure-button-primary" disabled="true">Submit</button>
                              </div>
                          </fieldset>
                      </form>
                      
                      <script>
                      $('#password, #confirm_password').on('keyup', function () {
                        if ($('#password').val() === $('#confirm_password').val()) {
                          $('#submit-password').prop( "disabled" , false );
                          $('#submit-password').removeClass('disabled');
                          $('#message').html('Matching').css('color', 'green');
                        } else {
                          $('#submit-password').prop( "disabled", true );
                          $('#submit-password').addClass('disabled');
                          $('#message').html('Not Matching').css('color', 'red');  
                        }
                      });
                      </script>
                      
                      <?php endif; ?>

                    </div>
                </div>

              <div class="clr"></div>
          </div>

      </div>

		</main><!-- #main -->
	</div><!-- #primary -->

  <script>
  $("body").find("#datatable1").DataTable();
  </script>

<?php
get_footer();
