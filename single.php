<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MJS Tour
 */

get_header();
?>


<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi">
	<main id="main" class="site-main batasna_kabeh halaman">
		<div class="batasna_wrap">
			<div class="breadcumb">
				<div class="clr"></div>
				<!-- PERUBAHAN 10 April 2016 -->
				<div class="brdcmb_knc">
					<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
					<strong><span itemprop="title">Beranda</span></strong>
					</a>
					</span>
					<span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="<?php echo get_the_permalink(); ?>" title="" itemprop="url">/
					<span itemprop="title"><?php echo get_the_title(); ?></span>
					</a>
					</span>
				</div>
				<div class="brdcmb_kth"></div>
				<div class="clr"></div>
			</div>
			<h1><?php echo get_the_title(); ?></h1>
			<div class="separator separator_hideung"><span></span></div>
			<div class="halaman_konten halaman_sidebar">
				<div class="clr"></div>
				<div class="hs_kenca teks_statis gambar_statis">
					<?php
						while ( have_posts() ) :
						  the_post();
						
						  get_template_part( 'template-parts/content', 'single' );
						
						endwhile; // End of the loop.
						?>
            <!--SHARE-->
            <div class="halaman_komentar">
              <h4>Sebarkan Ini</h4>
                <!--SHARE THIS-->
                <div id="social-share"></div>
                <!--SHARE THIS-->
                <script>
                $("#social-share").jsSocials({
                    showLabel: false,
                    showCount: true,
                    shareIn: "popup",
                    shares: ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "whatsapp"]
                });
                </script>
            </div>
            <!--SHARE-->
            <!--KOMENTAR-->
            <div class="halaman_komentar">
              <h4>Komentar Anda</h4>
              <?php
                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                  comments_template();
                endif;
                ?>
            </div>
            <!--KOMENTAR-->
				</div>
				<?php get_sidebar(); ?>
				<div class="clr"></div>
			</div>
		</div>
	</main>
	<!-- #main -->
</div>
<!-- #primary -->


<?php
get_footer();
