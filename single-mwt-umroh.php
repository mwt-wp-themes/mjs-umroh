<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MJS Tour
 */

global $mwt;
get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi">
		<main id="main" class="site-main batasna_kabeh halaman halaman_detail_umroh">
      
      <div class="batasna_wrap">

        <div class="breadcumb">
            <div class="clr"></div>

            <!-- PERUBAHAN 10 April 2016 -->
            <div class="brdcmb_knc">
                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		    				<a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
		    					<strong><span itemprop="title">Beranda</span></strong>
                </a>
                </span>

                <?php
                $kategori = Mwt::get_field( 'kategori' ); 
                if( !empty( $kategori ) ) { 
                  echo '<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">';
                  echo '<a href="' . add_query_arg( array(
                    'paket' => $kategori->term_id,
                    'tgl'   => '',
                    'order_by'  => ''
                  ), get_post_type_archive_link( 'mwt-umroh' ) ) . '" title="" itemprop="url">/<span itemprop="title">'.$kategori->name.'</span></a>';
                  echo '</span>';
                } 
                ?>

                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		    				<a href="<?php echo get_the_permalink(); ?>" title="" itemprop="url">/
		    					<span itemprop="title"><?php echo get_the_title(); ?></span>
                </a>
                </span>
            </div>
            <!-- PERUBAHAN 10 April 2016 -->

            <div class="brdcmb_kth">


            </div>

            <div class="clr"></div>
        </div>

        <h1><?php echo ( !empty( $kategori = Mwt::get_field( 'kategori', get_the_ID() ) ) ) ? $kategori->name : '' ?></h1>
        <div class="separator separator_hideung separator_hdu"><span></span></div>
        <h2 class="teks_ageung hl_h2"><?php echo get_the_title(); ?></h2>
      
        <div class="halaman_konten halaman_sidebar">
            <div class="clr"></div>

            <?php
            while ( have_posts() ) :
              the_post();

              get_template_part( 'template-parts/content', 'single-umroh' );

              //the_post_navigation();

            endwhile; // End of the loop.
            ?>

            <div class="clr"></div>
        </div>

      </div>

      <div class="hdu_fixed"><a href="#pesan_paket_2" class="baten baten_dosis"><span class="teks_ageung">Pesan Paket Sekarang</span></a></div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
