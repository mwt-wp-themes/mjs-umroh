<?php
/**
 * Template name: Kontak
 */
global $mwt, $mwt_option;
$page_id = get_the_ID();
$custom_pages = array( $mwt_option['galery-page-id'], $mwt_option['galery-video-page-id'], $mwt_option['account-page-id'], $mwt_option['login-page-id'], $mwt_option['thankyou-page-id'] );
$sidebar = ( in_array( $page_id, $custom_pages ) ) ? false : true;
if( $page_id == $mwt_option['galery-page-id'] || $page_id == $mwt_option['galery-video-page-id'] ) {
  $gal_type = ( $page_id == $mwt_option['galery-video-page-id'] ) ? "video" : "gambar";
  $container = 'halaman halaman_lis_artikel';
} else {
  $container = 'halaman'; 
}
get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi"><!--primary-->
		<main id="main" class="site-main batasna_kabeh halaman halaman_kontak">
      
      <div class="batasna_wrap">

          <div class="breadcumb">
              <div class="clr"></div>

              <!-- PERUBAHAN 10 April 2016 -->
              <div class="brdcmb_knc">
                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
                        <strong><span itemprop="title">Beranda</span></strong>
                  </a>
                  </span>

                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo get_the_permalink(); ?>" itemprop="url">/
                        <span itemprop="title"><?php echo get_the_title(); ?></span>
                  </a>
                  </span>
              </div>

              <div class="brdcmb_kth"></div>

              <div class="clr"></div>
          </div>

          <?php if( $page_id != $mwt_option['thankyou-page-id'] ) : ?>
          <h1><?php echo get_the_title(); ?></h1>
          <div class="separator separator_hideung"><span></span></div>
          <?php endif; ?>
        


<div class="halaman_konten">
	<div class="hk_lis">
		<h3><?php echo $mwt_option['contact-office']; ?></h3>
		<div class="clr"></div>
		<div class="hk_lis_kenca">
      <?php echo $mwt_option['contact-maps']; ?>
		</div>
		<div class="hk_lis_katuhu">
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Jam Operasional</strong></p>
				<p><?php echo $mwt_option['contact-jam-operasional']; ?></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Nomor Telepon</strong></p>
				<p><?php echo $mwt_option['contact-phone']; ?> / <?php echo $mwt_option['contact-mobile']; ?></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Fax</strong></p>
				<p><?php echo $mwt_option['contact-fax']; ?></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Email</strong></p>
				<p><a href="mailto:<?php echo $mwt_option['contact-email']; ?>"><?php echo $mwt_option['contact-email']; ?></a></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Alamat</strong></p>
				<p><?php echo $mwt_option['contact-address']; ?></p>
			</div>
		</div>
		<div class="clr"></div>
	</div>
	<div class="hk_lis">
		<h3><?php echo $mwt_option['contact-office2']; ?></h3>
		<div class="clr"></div>
		<div class="hk_lis_kenca">
			<?php echo $mwt_option['contact-maps-2']; ?>
		</div>
		<div class="hk_lis_katuhu">
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Jam Operasional</strong></p>
				<p><?php echo $mwt_option['contact-jam-operasional-2']; ?></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Nomor Telepon</strong></p>
				<p><?php echo $mwt_option['contact-phone2']; ?> / <?php echo $mwt_option['contact-mobile2']; ?></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Fax</strong></p>
				<p><?php echo $mwt_option['contact-fax2']; ?></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Email</strong></p>
				<p><a href="mailto:<?php echo $mwt_option['contact-email2']; ?>"><?php echo $mwt_option['contact-email2']; ?></a></p>
			</div>
			<div class="hk_lis_katuhu_kntk">
				<p class="teks_ageung"><strong>Alamat</strong></p>
				<p><?php echo $mwt_option['contact-address-2']; ?></p>
			</div>
		</div>
		<div class="clr"></div>
	</div>
</div>



      </div>

		</main><!-- #main -->


    <section class="batasna_kabeh halaman halaman_kontak halaman_kontak_form">
      <div class="batasna_wrap">
        <h1>Kami Telah Mengantar Tamu Allah Sejak Tahun 2008</h1>
        <div class="separator separator_hideung"><span></span></div>
        <div class="halaman_konten">
          <div class="hk_lis">
            <h3>Form Kontak</h3>
            <div class="clr"></div>
            <div class="">
              <div class="form form_border">
                <div id="kontak_kami_form">
                  <!-- kontak_kami_form -->
                  <div class="clr"></div>
                  <div class="blok_form">
                    <input id="nama" name="nama" class="input" placeholder="Nama Lengkap" type="text">
                  </div>
                  <div class="blok_form bf_katuhu">
                    <input id="email" name="email" class="input" placeholder="Email" type="text">
                  </div>
                  <div class="blok_form">
                    <input id="telepon" name="telepon" class="input" placeholder="No. Telepon" type="number">
                  </div>
                  <div class="blok_form bf_katuhu">
                    <input id="kota" name="kota" class="input" placeholder="Kota" type="text">
                  </div>
                  <div class="clr"></div>
                  <div class="blok_form blok_form_100">
                    <textarea name="pesan" id="pesan" class="input" placeholder="Tulis pesan Anda disini" rows="4"></textarea>
                  </div>
                  <div class="clr"></div>
                  <div class="blok_form blok_form_100">
                    <button id="submitKontak" class="baten baten_dosis"><span class="teks_ageung">Kirim Pesan Sekarang</span></button>
                    <span id="kontak_view"></span>
                  </div>
                  <div class="clr"></div>
                </div>
              </div>
            </div>
            <div class="hk_lis_katuhu"></div>
            <div class="clr"></div>
          </div>
        </div>
      </div>
    </section>


	</div><!-- #primary -->

<!--KOLEPS--><!-- FAQ, KARIR -->
<script>
  $(document).ready(function() {
  var hklps_lis = $('.hklps_lis'),
  hklps_lis_heding = $('.hklps_lis_heding'),
  hklps_lis_konten = $('.hklps_lis_konten');

  hklps_lis.children(hklps_lis_heding).on('click', function(){
    $(this).toggleClass("hklps_lh_muka").next(hklps_lis_konten).toggleClass("hklps_lk_muka").end().parent(".hklps_lis").siblings(".hklps_lis").children(hklps_lis_heding).removeClass("hklps_lh_muka").next(hklps_lis_konten).removeClass("hklps_lk_muka");
  });

  });
  </script>
<!--KOLEPS-->

<?php
get_footer();
