<?php
/**
 * Template name: FAQ
 */
global $mwt, $mwt_option;
$page_id = get_the_ID();
$custom_pages = array( $mwt_option['galery-page-id'], $mwt_option['galery-video-page-id'], $mwt_option['account-page-id'], $mwt_option['login-page-id'], $mwt_option['thankyou-page-id'] );
$sidebar = ( in_array( $page_id, $custom_pages ) ) ? false : true;
if( $page_id == $mwt_option['galery-page-id'] || $page_id == $mwt_option['galery-video-page-id'] ) {
  $gal_type = ( $page_id == $mwt_option['galery-video-page-id'] ) ? "video" : "gambar";
  $container = 'halaman halaman_lis_artikel';
} else {
  $container = 'halaman'; 
}
get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi"><!--primary-->
		<main id="main" class="site-main batasna_kabeh halaman halaman_koleps">
      
      <div class="batasna_wrap">

          <div class="breadcumb">
              <div class="clr"></div>

              <!-- PERUBAHAN 10 April 2016 -->
              <div class="brdcmb_knc">
                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
                        <strong><span itemprop="title">Beranda</span></strong>
                  </a>
                  </span>

                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo get_the_permalink(); ?>" itemprop="url">/
                        <span itemprop="title"><?php echo get_the_title(); ?></span>
                  </a>
                  </span>
              </div>

              <div class="brdcmb_kth"></div>

              <div class="clr"></div>
          </div>

          <?php if( $page_id != $mwt_option['thankyou-page-id'] ) : ?>
          <h1><?php echo get_the_title(); ?></h1>
          <div class="separator separator_hideung"><span></span></div>
          <?php endif; ?>
        
          <div class="halaman_konten <?php echo ( $sidebar ) ? 'halaman_sidebar' : ''; ?>">
              <div class="clr"></div>

              
              <div class="hs_kenca">
              
                
                <?php
                while ( have_posts() ) :
                  the_post(); ?>

                <div class="hklps_lis">
                  <div class="hklps_lis_heding hklps_lh_muka">Syarat, Rukun dan wajib umroh</div>
                  <div class="hklps_lis_konten hklps_lk_muka teks_statis">
                    <p>Syarat Umroh</p>
                    <p>&nbsp;</p>
                    <ul>
                      <li>Beragama Islam</li>
                      <li>Baligh</li>
                      <li>Berakal sehat</li>
                      <li>Merdeka ( Bukan budak )</li>
                      <li>Mampu</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>Rukun Umroh</p>
                    <p>&nbsp;</p>
                    <ul>
                      <li>Niat ihram dari Miqot</li>
                      <li>Tawaf</li>
                      <li>Sa'i</li>
                      <li>Tahallul ( gunting rambut )</li>
                      <li>Tertib</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>Wajib Umroh</p>
                    <p>&nbsp;</p>
                    <ul>
                      <li>Niat ihram umroh di Miqot</li>
                      <li>Meninggalkan larangan selama ihram</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>Tahapan atau urutan Kegiatan umroh</p>
                    <p>&nbsp;</p>
                    <ul>
                      <li>Berangkat menuju Miqot</li>
                      <li>Mandi kemudian berpakaian ihram di Miqot ( boleh juga dilakukan di pemondokkan sebelum berangkat miqot )</li>
                      <li>Berpakain ihram sambil sholat sunnah ihram 2 rakaat, dianjurkan dalam sholat sunnah ihram (setelah membaca al-fatihah ) membaca surat al-kafirun pada rakaat pertama dan membaca surah al-Ikhlas pada rakaat kedua<br>Melafazkan niat umroh minimal membaca " labbaikallahu umrotan " atau yang lengkap membaca " nawaitul 'umrota wa ahromtu bihaa lillahi ta'aalaa</li>
                      <li>Selanjutnya seluruh jamaah menuju Mekkah dengan menempuh perjalanan sejauh 450 KM dengan berpakain ihrom sambil membaca talbiah sebanyak-banyaknya sepanjang perjalanan sampai masuk ke kota Mekkah</li>
                      <li>Sampai di pemondokkan menata barang bawaan dan jamaah tetap dengan berpakaian ihram</li>
                      <li>Selanjutnya menuju Masjidil haram dengan tetap berpakaian ihram dan diusahakn masuk Masjidil harom melalui pintu Baabussalam. Melihat ka'bah dan melintas makam nabi Ibrahim sambil berdo'a, kemudian langsung menuju rukun Hajar Aswad.</li>
                      <li>Melakukan thawaf / mengelilingi Ka'bah sebanyak 7 kali</li>
                      <li>Melakukan thawaf, yaitu berjalan antara bukit Safa dan Marwa sebanyak 7 kali balikkan</li>
                      <li>Setelah selesai melakukan sa'i yang berakhir di bukit Marwa, melakukan Tahallul / menggunting rambut minimal 3 helai rambut</li>
                      <li>Setelah itu selesailah kegiatan umroh, dan jamaah dihalalkan / dibebaskan dari larangan selama melakukan ihram, boleh melepas pakaian ihram dan berganti dengan pakaian biasa.</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>Larangan selama melaksankan ihrom umroh</p>
                    <p>&nbsp;</p>
                    <ul>
                      <li>Tidak boleh mencabut dan memotong rambut, menggaruk sampai kulit terkelupas, memotong kuku</li>
                      <li>Tidak boleh memakai wangi-wangian / parfum</li>
                      <li>Tidak boleh bertengkar</li>
                      <li>Tidak boleh melakukan hubungan suami istri</li>
                      <li>Tidak boleh bermesraan</li>
                      <li>Tidak boleh berkata kotor, perkataan yang tidak baik, bicara porno, jorok</li>
                      <li>Tidak boleh menikah atau menikahkan</li>
                      <li>Tidak boleh berburu binatang atau membantu berburu</li>
                      <li>Tidak boleh membunuh bintang, kecuali yang menganvcam jiwa</li>
                      <li>Tidak boleh memotong atau mencabut tumbuhan dan segala hal yang mengganngu kehidupan makhluk di dunia ini</li>
                      <li>Tidak boleh berhias atau berdandan<br>Pria tidak boleh memakai penutup kepala, pakaian yang berjahit, memakai alas kaki yang menutup mata kaki<br>Wanita tidak boleh menutup wajah dan memakai sarung tangan yang dapat menutup telapak tangan.</li>
                    </ul>
                  </div>
                </div>
                <div class="hklps_lis">
                  <div class="hklps_lis_heding ">Hukum Melaksanakan Umroh</div>
                  <div class="hklps_lis_konten  teks_statis">
                    <p>Hukum melaksanakan umroh sendiri adalah sunnah bagi setiap muslim yang mampu melaksanakannya, baik mampu secara materi maupun non materi.Pelaksanaan umroh sendiri dapat dilakukan kapan saja kecuali pada hari Arafah yaitu tanggal 10 Zulhijjah dan hari tasrik yaitu pada tanggal 11, 12 dan 13 Zulhijjah</p>
                    <p>Sebagian ulama berpendapat bahwa hukum melaksankan umroh adalah wajib atau fardu bagi orang yang belum melaksankan sementara dia mampu untuk melaksanakannya.Naun demiakian ada pula sebagain ulama yang mengatakan bahwa ibadah umroh itu hukummnya sunnah mu'akkad.</p>
                    <p>Sebuah hadis yang diriwayatkan oleh imam Muslim mengatakan bahwa melaksanakan ibadah umroh pada bulan Ramadhan nilainya sama dengan melaksanakan ibadah haji</p>
                  </div>
                </div>
                <div class="hklps_lis">
                  <div class="hklps_lis_heding ">Pengertian Umroh</div>
                  <div class="hklps_lis_konten  teks_statis">
                    <p>Pengertian Umroh</p>
                    <p>Umroh adalah mengunjungi Ka'bah (baitullah) untuk melaksanakan serangkaian kegiatan ibadah ( thawaf, sa'i, tahallul ) dengan syarat dan ketentuan yang telah ditetapkan dalam al-Qur'an maupun sunnah Rasulillah SAW.<br><br></p>
                  </div>
                </div>
                
                <?php
                  // If comments are open or we have at least one comment, load up the comment template.
                  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                  endif;

                endwhile; // End of the loop.
                ?>

                <!--SHARE-->
                <div class="halaman_komentar">
                  <h4>Sebarkan Ini</h4>
                  <!--SHARE THIS-->
                  <div id="social-share"></div>
                  <!--SHARE THIS-->
                  <script>
                  $("#social-share").jsSocials({
                      showLabel: false,
                      showCount: true,
                      shareIn: "popup",
                      shares: ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "whatsapp"]
                  });
                  </script>
                </div>
                <!--SHARE-->
              </div>
              <?php get_sidebar(); ?>
              <div class="clr"></div>
          </div>

      </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<!--KOLEPS--><!-- FAQ, KARIR -->
<script>
  $(document).ready(function() {
  var hklps_lis = $('.hklps_lis'),
  hklps_lis_heding = $('.hklps_lis_heding'),
  hklps_lis_konten = $('.hklps_lis_konten');

  hklps_lis.children(hklps_lis_heding).on('click', function(){
    $(this).toggleClass("hklps_lh_muka").next(hklps_lis_konten).toggleClass("hklps_lk_muka").end().parent(".hklps_lis").siblings(".hklps_lis").children(hklps_lis_heding).removeClass("hklps_lh_muka").next(hklps_lis_konten).removeClass("hklps_lk_muka");
  });

  });
  </script>
<!--KOLEPS-->

<?php
get_footer();
