<?php global $mwt_option; ?>
<!--TESTIMONIAL-->
<?php if( $mwt_option['testimonial-enabled'] == 1 ) : ?>
<section class="batasna_kabeh testimonial">
    <div class="batasna_wrap">

        <h3><?php echo $mwt_option['testimonial-title']; ?></h3>
        <div class="separator separator_hideung"><span></span></div>

        <div class="lt_kontrol">
          <a class="baten" id="prepLT"><i class="i_arahkenca2"></i></a><!--
          --><a href="<?php echo get_post_type_archive_link( 'testimonials' ); ?>" class="baten baten_dosis"><span>Lihat Semua</span></a><!--
          --><a class="baten" id="neksLT"><i class="i_arahkatuhu2"></i></a>
        </div>

        <ul id="lisTestimonial">
          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'testimonials' ),
            'post_status'            => array( 'publish' ),
            'nopaging'               => true,
            'posts_per_page'         => -1,
          );

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 0;
            while ( $query->have_posts() ) {
              $query->the_post(); ?>
              <li>
                  <div class="testimonial_blok">
                      <div class="tb_ditel">
                          <div class="clr"></div>

                          <div class="tb_ditel_kenca">
                              <div class="gbrna"><img src="<?php echo get_the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>"></div>
                          </div>
                          <div class="tb_ditel_katuhu">
                              <h4><?php the_title(); ?></h4>
                              <p><?php echo Mwt::get_field( 'job_title' ); ?></p>
                              <span></span>
                          </div>

                          <div class="clr"></div>
                      </div>

                      <?php the_content(); ?>
                  </div>
              </li>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>
        </ul>

    </div>
</section>
<script>
$(document).ready(function() {
    var lisTestimonial = $("#lisTestimonial");
    lisTestimonial.owlCarousel({
        itemsCustom: [
            [0, 1],
            [750, 2]
        ],
        pagination: false,
        navigation: false,
        mouseDrag: true,
        autoPlay: true,
        stopOnHover: true,
        lazyLoad: true,
        slideSpeed: 200,
        paginationSpeed: 800
    });

    $("#neksLT").click(function() {
        lisTestimonial.trigger('owl.next');
    })
    $("#prepLT").click(function() {
        lisTestimonial.trigger('owl.prev');
    })

});
</script>
<?php endif; ?>