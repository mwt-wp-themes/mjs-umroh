<?php global $mwt_option; ?>
<?php if( $mwt_option['video-enabled'] == 1 ) : ?>
<section class="batasna_kabeh temukan_jadwal_umroh " id="video-perkenalan">
    <div class="batasna_wrap">

        <div class="tju_kenca">
            <iframe src="https://www.youtube.com/embed/<?php echo $mwt_option['video-youtube-id']; ?>?rel=0&amp;controls=0&amp;showinfo=0" allowfullscreen="" width="100%" height="300" frameborder="0"></iframe>
        </div>
        <div class="tju_katuhu">
            <h3><?php echo $mwt_option['video-title']; ?></h3>
            <p><?php echo $mwt_option['video-description']; ?></p>
            <div class="separator separator_abu"><span></span></div>
<!--             <a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" class="baten baten_dosis baten_blok"><span><?php echo $mwt_option['jadwal-umroh-button-title']; ?></span></a> -->
        </div>

    </div>

</section>

<?php endif; ?>