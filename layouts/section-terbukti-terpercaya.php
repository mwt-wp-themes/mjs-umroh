<?php global $mwt_option; ?>
<!--TESTIMONIAL-->
<?php if( $mwt_option['terbukti-terpercaya-enabled'] == 1 ) : ?>
<section class="batasna_kabeh terbukti_terpercaya">
    <div class="batasna_wrap">

        <h3><?php echo $mwt_option['terbukti-terpercaya-title']; ?></h3>
        <h4><?php echo $mwt_option['terbukti-terpercaya-description']; ?></h4>
        <div class="separator separator_hideung"><span></span></div>
        <?php if( !empty( $mwt_option['terbukti-terpercaya-slides'] ) ) : ?>
        <div class="tt_lis">
		    		<div class="tt_lis_hulu">
		    			<a class="baten" id="prepTT"><i class="i_arahkenca2"></i></a><!--
		    			--><a class="baten" id="neksTT"><i class="i_arahkatuhu2"></i></a>
		    		</div>
		    		<div class="tt_lis_konten">
			    		<ul id="lis_terbukti_terpercaya">
                  <?php foreach( $mwt_option['terbukti-terpercaya-slides'] as $slide ) : ?>
                    <li>
                        <div class="clr"></div>

                        <div class="ttlk_kenca">
                            <a href="<?php echo $slide['url'] ;?>"><img src="<?php echo $slide['image']; ?>" alt="<?php echo $slide['title'] ;?>"></a>
                        </div>
                        <div class="ttlk_katuhu">
                            <h5><?php echo $slide['title'] ;?></h5>
                            <p><?php echo $slide['description'] ;?></p>
                        </div>

                        <div class="clr"></div>
                    </li>
                  <?php endforeach; ?>
                </ul>
            </div>

        </div>
        <?php endif; ?>
    </div>
</section>
<script>
$(document).ready(function() {
var lis_terbukti_terpercaya = $("#lis_terbukti_terpercaya");
lis_terbukti_terpercaya.owlCarousel({
  itemsCustom : [
    [0, 1],
    [750, 1]
  ],
  pagination : true, navigation : false, mouseDrag : true, autoPlay : true, stopOnHover : true, lazyLoad : true, slideSpeed : 200, paginationSpeed : 800
});

$("#neksTT").click(function(){ lis_terbukti_terpercaya.trigger('owl.next'); })
$("#prepTT").click(function(){ lis_terbukti_terpercaya.trigger('owl.prev'); })

});
</script>
<?php endif; ?>