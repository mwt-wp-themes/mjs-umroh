<?php global $mwt_option; ?>
<!--TOP PROMO-->
<?php if( $mwt_option['top-promo-enabled'] == 1 ) : ?>
<section class="batasna_kabeh top_promo">
    <div class="batasna_wrap">
        <h3><?php echo $mwt_option['top-promo-title']; ?></h3>
        <div class="separator separator_abu"><span></span></div>
        <h2><?php echo $mwt_option['top-promo-subtitle']; ?></h2>
        <h2 class="h2_kadua"><?php echo $mwt_option['top-promo-description']; ?></h2>

        <ul id="topPromo">
          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'mwt-umroh' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
            'meta_key'              => 'promo',
            'meta_value'            => 1,
            'meta_value_num'        => 1,
            'meta_compare'          => '='
          );

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 0;
            while ( $query->have_posts() ) {
              $query->the_post(); $harga = Mwt::get_field( 'harga' ); ?>
              <li>
                  <a href="<?php the_permalink(); ?>">
                      <div class="tp_hulu">
                          <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- $ <?php echo convertCurrency( intval($harga['quard']), 'IDR', 'USD' ); ?>">
                              <div class="tph_harga_blok">
                                  <span>IDR</span>
                                  <p><?php echo mwt_singkat_harga( $harga['quard'] ); ?></p>
                              </div>
                          </div>

                          <h4><?php the_title(); ?></h4>
                      </div>
                      <div class="tp_lis">
                          <div class="clr"></div>
                          <?php $penginapan = Mwt::get_field('penginapan');?>
                          <?php if( !empty( $penginapan ) ) :?>
                          <?php foreach( $penginapan as $hotel ) : ?>                        
                          <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang <?php echo $hotel['bintang']; ?>">
                              <div class="tplna_kenca tpl2ln"><i></i></div>
                              <div class="tplna_katuhu">
                                  <span><?php echo $hotel['nama_hotel']; ?></span>
                                  <p><?php echo $hotel['lokasi']; ?></p>
                              </div>

                              <div class="tplnak_bentang bentang<?php echo $hotel['bintang']; ?>"></div>
                          </div>
                          <?php endforeach; endif; ?>
                          <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Langsung Madinah / Langsung Madinah">
                              <div class="tplna_kenca tpl2ln"><i></i></div>
                              <div class="tplna_katuhu">
                                  <span>Pesawat</span>
                                  <?php $maskapai = Mwt::get_field('maskapai'); ?>
                                  <?php if( !empty( $maskapai ) ) :?>
                                  <?php foreach( $maskapai as $mskp ) : ?>
                                  <p><?php echo $mskp->post_title; ?></p>
                                  <?php endforeach; endif; ?>
                                  
                              </div>
                          </div>
                          <div class="tplna">
                              <div class="tplna_kenca"><i></i></div>
                              <div class="tplna_katuhu">
                                  <p>Durasi <?php echo Mwt::get_field('durasi'); ?> Hari</p>
                              </div>
                          </div>
                          <?php if( !empty( Mwt::get_field('shalat_jumat') ) ) : ?>
                          <div class="tplna">
                              <div class="tplna_kenca"><i></i></div>
                              <div class="tplna_katuhu">
                                  <p>Shalat jumat <?php echo Mwt::get_field('shalat_jumat'); ?>x</p>
                              </div>
                          </div>
                          <?php endif; ?>
                          <?php if( !empty( Mwt::get_field('provider') ) ) : ?>
                          <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top">
                              <div class="tplna_kenca tpl2ln"><i></i></div>
                              <div class="tplna_katuhu">
                                  <span>Provider</span>
                                  <?php $provider = Mwt::get_field('provider'); ?>
                                  <p style="position:relative">
                                    <img src="<?php echo get_the_post_thumbnail_url( $provider->ID ); ?>" style="width:35px;display:inline-block;vertical-align:middle">
                                    <?php echo $provider->post_title; ?>
                                  </p>
                              </div>
                          </div>
                          <?php endif; ?>
                          <div class="clr"></div>
                      </div>
                  </a>
              </li>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>

        </ul>

        <div class="tp_kontrol">
            <a class="baten" id="prepTP"><i class="i_arahkenca"></i></a>
            <!--
		    		--><a href="<?php echo get_post_type_archive_link('mwt-umroh'); ?>" class="baten baten_dosis hint--rounded hint--biru hint--uppercase hint--bounce hint--bottom" data-hint="Lihat Semua Jadwal Umroh Lengkap"><span>Lihat Jadwal Umroh</span></a>
            <!--
		    		--><a class="baten" id="neksTP"><i class="i_arahkatuhu"></i></a>
        </div>

    </div>
</section>
<script>
$(document).ready(function() {
var topPromo = $("#topPromo");
topPromo.owlCarousel({
  itemsCustom : [
    [0, 1],
    [650, 2],
    [960, 3]
  ],
  pagination : false, navigation : false, mouseDrag : true, autoPlay : true, stopOnHover : true, lazyLoad : true, slideSpeed : 200, paginationSpeed : 800
});

$("#neksTP").click(function(){ topPromo.trigger('owl.next'); })
$("#prepTP").click(function(){ topPromo.trigger('owl.prev'); })

});
</script>
<?php endif; ?>