<?php global $mwt_option; ?>
<?php if( $mwt_option['paket-umroh-enabled'] == 1 ) : 
$term_ids = array();
if( !empty( $mwt_option['paket-umroh-category-1'] ) ) 
  $term_ids[] = $mwt_option['paket-umroh-category-1'];
if( !empty( $mwt_option['paket-umroh-category-2'] ) ) 
  $term_ids[] = $mwt_option['paket-umroh-category-2'];
if( !empty( $mwt_option['paket-umroh-category-3'] ) ) 
  $term_ids[] = $mwt_option['paket-umroh-category-3'];
if( !empty( $mwt_option['paket-umroh-category-4'] ) ) 
  $term_ids[] = $mwt_option['paket-umroh-category-4'];
?>
<section class="batasna_kabeh paket_umroh">
    <div class="batasna_wrap">
        
      <?php if( !empty( $mwt_option['paket-umroh-title'] ) ) : ?>
        <h3><?php echo $mwt_option['paket-umroh-title']; ?></h3>
        <div class="separator separator_hideung"><span></span></div>
      <?php endif; ?>
        <div class="pu_lis">
          <?php for( $i = 0; $i < count( $term_ids ); $i++ ) : ?>
          <?php $term = get_term_by( 'id', intval( $term_ids[$i] ), 'umroh_category' ); 
          $el_classes = '';
          $el_styles = '';
          $overlay_styles = '';
          if( !empty( $mwt_option['paket-umroh-category-'.($i+1).'-bg']['url'] ) ) {
            $el_styles .= 'background: url( ' . $mwt_option['paket-umroh-category-'.($i+1).'-bg']['url'] . ' ) no-repeat center center;-webkit-background-size: cover;
  -moz-background-size: cover;-o-background-size: cover;background-size: cover;';
            $overlay_styles .= 'background:unset';
          } else {
            $el_classes .= 'pu_lis_' . $term->slug; 
            
          }
          ?>
            <a href="<?php echo esc_url( add_query_arg( array(
              'paket' => $term->term_id,
              'tgl'   => '',
              'order_by'  => ''
            ), get_post_type_archive_link( 'mwt-umroh' ) ) ); ?>" class="<?php echo $el_classes; ?>" style="background:none;">
                <div class="pu_lis_blok" style="background:none;padding:0">
                    <img src="<?php echo $mwt_option['paket-umroh-category-'.($i+1).'-bg']['url']; ?>">
                </div>
            </a>
          <?php endfor; ?>
        </div>

    </div>
</section>
<?php endif; ?>