<?php global $mwt_option; ?>

<section class="batasna_kabeh cari_jadwal_hulu bgPRLK" style="background-image: url('<?php echo $mwt_option['header-banner-img']['url']; ?>')">
    <div class="batasna_kabeh cjh_bg_trans">
        <div class="batasna_wrap">
            <h3><?php echo $mwt_option['header-title']; ?></h3>
            <p><?php echo $mwt_option['header-description']; ?></p>
            <div class="separator separator_bodas"><span></span></div>
            <div class="form cjh_form">
                <form action="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" method="get">
                    <div class="clr"></div>

                    <div class="blok_form">
                        <div class="icon icon_kabah"><span><i></i></span></div>
                        <div class="selek">
                            <select class="input" name="paket">
                                <option value="">Pilih Paket Umroh</option>
                                <?php
                                $terms = get_terms( 'umroh_category', array(
                                    'hide_empty' => false,
                                ) );
                                foreach( $terms as $term ) {
                                  echo '<option value="' . $term->term_id . '"';
                                  echo ( !empty( $_GET['paket'] ) && $term->term_id == $_GET['paket'] ) ? ' selected' : '';
                                  echo '>' . $term->name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--

        -->
                    <div class="blok_form">
                        <div class="icon icon_tanggal"><span><i></i></span></div>
                        <div class="selek">
                            <select class="input" name="tgl">
                              <option value="">Jadwal Keberangkatan</option>
                              <?php
                              $current_date = new DateTime();
                              echo '<option value="' . $current_date->format( 'Ym' ) . '">' . $current_date->format( 'F Y' ) . '</option>';
                              $current_date->add(new DateInterval('P1M'));
                              echo '<option value="' . $current_date->format( 'Ym' ) . '">' . $current_date->format( 'F Y' ) . '</option>';
                              $current_date->add(new DateInterval('P1M'));
                              echo '<option value="' . $current_date->format( 'Ym' ) . '">' . $current_date->format( 'F Y' ) . '</option>';
                              $current_date->add(new DateInterval('P1M'));
                              echo '<option value="' . $current_date->format( 'Ym' ) . '">' . $current_date->format( 'F Y' ) . '</option>';
                              ?>
                            </select>
                        </div>
                        <input type="hidden" name="order_by" value="ur_date_ASC">
                    </div>
                    <!--

        -->
                    <!--<div class="blok_form lbr2">
          <div class="icon icon_bintang"><span><i></i></span></div>
          <div class="selek">
            <select class="input">
              <option value="">Pilih Hotel</option>
              <option value="">Bintang 5</option>
              <option value="">Bintang 4</option>
              <option value="">Bintang 3</option>
            </select>
          </div>
        </div>-->
                    <!--

        -->
                    <div class="blok_form bf_trans bf_cari">
                        <button class="baten baten_icon"><i class="i_cari"></i> <span>Cari</span></button>
                    </div>

                    <div class="clr"></div>
                </form>
            </div>
        </div>

        <div class="batasna_kabeh menu_handap">
          <?php
          wp_nav_menu( array(
            'theme_location' => 'menu-2',
            'menu_id'        => 'secondary-menu',
            'container'       => 'ul'
          ) );
          ?>
        </div>

    </div>
</section>