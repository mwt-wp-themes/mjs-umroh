<?php global $mwt_option; ?>
<!--SELLING POINT-->
<?php if( $mwt_option['selling-point-enabled'] == 1 ) : ?>
<section class="batasna_kabeh selling_point">
    <div class="batasna_wrap">
        <h1><?php echo $mwt_option['selling-point-title']; ?></h1>
        <?php if( !empty( $mwt_option['selling-point-slides'] ) ) : ?>
        <div class="sl_lis">
          <?php foreach( $mwt_option['selling-point-slides'] as $slide ) : ?>
            <a href="<?php echo $slide['url'] ;?>">
                <div class="gbrna">
                    <img src="<?php echo $slide['image']; ?>" title="<?php echo $slide['title'] ;?>" alt="<?php echo $slide['title'] ;?>">
                </div>
                <h3><?php echo $slide['title'] ;?></h3>
                <div class="separator separator_abu"><span></span></div>
                <p><?php echo $slide['description'] ;?></p>
            </a>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>
    </div>
</section>
<?php endif; ?>