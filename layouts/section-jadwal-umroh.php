<?php global $mwt_option; ?>
<section class="batasna_kabeh temukan_jadwal_umroh ">
    <div class="batasna_wrap">

        <div class="tju_kenca">
            <img src="<?php echo $mwt_option['jadwal-umroh-banner-img']['url']; ?>" title="<?php echo $mwt_option['jadwal-umroh-title']; ?>" alt="<?php echo $mwt_option['jadwal-umroh-title']; ?>"> </div>
        <!--
	    		-->
        <div class="tju_katuhu">
            <h3><?php echo $mwt_option['jadwal-umroh-title']; ?></h3>
            <p><?php echo $mwt_option['jadwal-umroh-subtitle']; ?></p>
            <div class="separator separator_abu"><span></span></div>
            <a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" class="baten baten_dosis baten_blok"><span><?php echo $mwt_option['jadwal-umroh-button-title']; ?></span></a>
        </div>

    </div>

</section>