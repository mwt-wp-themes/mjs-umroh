<?php global $mwt, $mwt_option; 
$mwt->dump( $mwt_option['berita-category-2'] );
?>
<!--BERITA-->
<?php if( $mwt_option['berita-enabled'] == 1 ) : ?>
<section class="batasna_kabeh artikel">
    <div class="batasna_wrap">

        <div class="artikel_lis_blok">
            <div class="clr"></div>

            <?php if( !empty( $mwt_option['berita-category-1'] ) ) : ?>
            <?php $term = get_term( intval( $mwt_option['berita-category-1'] ), 'category' ); ?>
            <div class="alb_lis">
                <a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><h3><?php echo $term->name; ?></h3></a>
                <div class="separator separator_abu "><span></span></div>
                <div class="alb_lis_blok">
                    <?php
                    // WP_Query arguments
                    $args = array(
                      'post_type'              => array( 'post' ),
                      'post_status'            => array( 'publish' ),
                      'category__in'           => array( $term->term_id ),
                      'nopaging'               => false,
                      'posts_per_page'         => 5,
                    );

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) {
                      $count = 0;
                      while ( $query->have_posts() ) {
                        $query->the_post(); ?>
                        <?php if( $count == 0 && has_post_thumbnail() ): ?>
                        <a href="<?php the_permalink(); ?>" class="alb_lis_blok_utm">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" style="height:200px">
                            <span><?php the_author(); ?> - <?php echo get_the_date('d F Y'); ?></span>
                            <h4><?php the_title(); ?></h4>
                        </a>
                        <?php else: ?>
                        <a href="<?php the_permalink(); ?>" class="alb_lis_blok_ls"><?php the_title(); ?></a>
                        <?php endif; ?>
                        <?php
                        $count++;
                      }
                    }
                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['berita-category-2'] ) ) : ?>
            <?php $term = get_term( intval( $mwt_option['berita-category-2'] ), 'category' ); ?>
            <div class="alb_lis alb_lis_nengah">
                <a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><h3><?php echo $term->name; ?></h3></a>
                <div class="separator separator_abu "><span></span></div>
                <div class="alb_lis_blok">
                    <?php
                    // WP_Query arguments
                    $args = array(
                      'post_type'              => array( 'post' ),
                      'post_status'            => array( 'publish' ),
                      'category__in'           => array( $term->term_id ),
                      'nopaging'               => false,
                      'posts_per_page'         => 5,
                    );

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) {
                      $count = 0;
                      while ( $query->have_posts() ) {
                        $query->the_post(); ?>
                        <?php if( $count == 0 && has_post_thumbnail() ): ?>
                        <a href="<?php the_permalink(); ?>" class="alb_lis_blok_utm">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" style="height:200px">
                            <span><?php the_author(); ?> - <?php echo get_the_date('d F Y'); ?></span>
                            <h4><?php the_title(); ?></h4>
                        </a>
                        <?php else: ?>
                        <a href="<?php the_permalink(); ?>" class="alb_lis_blok_ls"><?php the_title(); ?></a>
                        <?php endif; ?>
                        <?php
                        $count++;
                      }
                    }
                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['berita-category-3'] ) ) : ?>
            <?php $term = get_term( intval( $mwt_option['berita-category-3'] ), 'category' ); ?>
            <div class="alb_lis">
                <a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><h3><?php echo $term->name; ?></h3></a>
                <div class="separator separator_abu "><span></span></div>
                <div class="alb_lis_blok">
                    <?php
                    // WP_Query arguments
                    $args = array(
                      'post_type'              => array( 'post' ),
                      'post_status'            => array( 'publish' ),
                      'category__in'           => array( $term->term_id ),
                      'nopaging'               => false,
                      'posts_per_page'         => 5,
                    );

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) {
                      $count = 0;
                      while ( $query->have_posts() ) {
                        $query->the_post(); ?>
                        <?php if( $count == 0 && has_post_thumbnail() ): ?>
                        <a href="<?php the_permalink(); ?>" class="alb_lis_blok_utm">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" style="height:200px">
                            <span><?php the_author(); ?> - <?php echo get_the_date('d F Y'); ?></span>
                            <h4><?php the_title(); ?></h4>
                        </a>
                        <?php else: ?>
                        <a href="<?php the_permalink(); ?>" class="alb_lis_blok_ls"><?php the_title(); ?></a>
                        <?php endif; ?>
                        <?php
                        $count++;
                      }
                    }
                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                </div>
            </div>
            <?php endif; ?>
            <!--RSS BLOG-->

            <!--RSS BLOG-->

            <div class="clr"></div>
        </div>

    </div>
</section>
<?php endif; ?>