<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Content', 'mwt' ),
    'id'     => 'mwt-contents-option',
    'icon'  => 'el el-website',
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Halaman', 'mwt' ),
    'id'     => 'mwt-content-option',
    'subsection'  => true,
    'fields' => array(
        array(
          'id'          => 'account-page-id',
          'type'        => 'select', 
          'title'       => __('Account Page', 'mwt'),
          'data'        => 'pages',
        ),
        array(
          'id'          => 'login-page-id',
          'type'        => 'select', 
          'title'       => __('Login Page', 'mwt'),
          'data'        => 'pages',
        ),
        array(
          'id'          => 'thankyou-page-id',
          'type'        => 'select', 
          'title'       => __('Thankyou Page', 'mwt'),
          'data'        => 'pages',
        ),
        array(
          'id'          => 'galery-page-id',
          'type'        => 'select', 
          'title'       => __('Galery Gambar Page', 'mwt'),
          'data'        => 'pages',
        ),
        array(
          'id'          => 'galery-video-page-id',
          'type'        => 'select', 
          'title'       => __('Galery Video Page', 'mwt'),
          'data'        => 'pages',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Teks Statis', 'mwt' ),
    'id'     => 'mwt-statis-option',
    'subsection'  => true,
    'fields' => array(
        array(
          'id'          => 'sdk-content',
          'type'        => 'editor', 
          'title'       => __('Syarat dan Ketentuan', 'mwt'),
          'args'        => array(
            'wpautop' => false
          )
        ),
        array(
          'id'          => 'cara-bayar-content',
          'type'        => 'editor', 
          'title'       => __('Cara Pembayaran', 'mwt'),
          'args'        => array(
            'wpautop' => false
          )
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Banner', 'mwt' ),
    'id'     => 'mwt-statis-banner-option',
    'subsection'  => true,
    'fields' => array(
        array(
          'id'          => 'banner-statis-samping',
          'type'        => 'media', 
          'title'       => __('Banner Samping', 'mwt'),
          'url'         => true
        ),
        array(
          'id'          => 'banner-statis-footer',
          'type'        => 'media', 
          'title'       => __('Banner Footer', 'mwt'),
          'url'         => true
        ),
    )
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Sidebar Detail Umroh', 'mwt' ),
    'id'     => 'mwt-statis-single-umroh-option',
    'subsection'  => true,
    'fields' => array(
        array(
          'id'          => 'sdb-single-umroh-title',
          'type'        => 'text', 
          'title'       => __('Title', 'mwt'),
        ),
        array(
          'id'          => 'sdb-single-umroh-content-1',
          'type'        => 'text', 
          'title'       => __('Text 1', 'mwt'),
        ),
        array(
          'id'          => 'sdb-single-umroh-content-2',
          'type'        => 'text', 
          'title'       => __('Text 2', 'mwt'),
        ),
        array(
          'id'          => 'sdb-single-umroh-content-3',
          'type'        => 'text', 
          'title'       => __('Text 3', 'mwt'),
        ),
        array(
          'id'          => 'sdb-single-umroh-content-4',
          'type'        => 'text', 
          'title'       => __('Text 4', 'mwt'),
        ),
        array(
          'id'          => 'sdb-single-umroh-content-5',
          'type'        => 'text', 
          'title'       => __('Text 5', 'mwt'),
        ),
    )
) );


Redux::setSection( $opt_name, array(
    'title'  => __( 'Footer', 'mwt' ),
    'id'     => 'mwt-statis-footer-option',
    'subsection'  => true,
    'fields' => array(
        array(
          'id'          => 'statis-footer-contact-text',
          'type'        => 'text', 
          'title'       => __('Contact Title Text', 'mwt'),
        ),
    )
) );