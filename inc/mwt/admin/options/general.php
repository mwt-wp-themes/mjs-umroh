<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'General', 'mwt' ),
    'id'     => 'mwt-general-option',
//     'desc'   => __( 'Basic field with no subsections.', 'mwt' ),
    'icon'   => 'el el-dashboard',
    'fields' => array(
        array(
            'id'       => 'mwt-logo',
            'type'     => 'media', 
            'url'      => true,
            'title'    => __('Logo', 'mwt'),
            'subtitle' => __('Upload logo', 'mwt'),
        ),
//         array(
//           'id'          => 'mwt-typography',
//           'type'        => 'typography', 
//           'title'       => __('Typography', 'mwt'),
//           'google'      => true, 
//           'font-backup' => true,
//           //'output'      => array('.baten_dosis'),
//           'units'       =>'px',
//           'default'     => array(
//               'font-family' => 'Roboto Slab', 
//               'google'      => true,
//           ),
//         )
    )
) );