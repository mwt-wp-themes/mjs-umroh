<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Kontak 1', 'mwt' ),
    'id'     => 'mwt-company-contact-option',
    'icon'  => 'el el-globe',
    'fields' => array(
        array(
          'id'          => 'contact-office',
          'type'        => 'text', 
          'title'       => __('Nama Kantor', 'mwt'),
        ),
        array(
          'id'          => 'contact-address',
          'type'        => 'textarea', 
          'title'       => __('Alamat Kantor 1', 'mwt'),
        ),
        array(
          'id'          => 'contact-maps',
          'type'        => 'textarea', 
          'title'       => __('Embed Google Maps', 'mwt'),
        ),
        array(
          'id'          => 'contact-jam-operasional',
          'type'        => 'text', 
          'title'       => __('Jam Operasional', 'mwt'),
        ),
        array(
          'id'          => 'contact-email',
          'type'        => 'text', 
          'title'       => __('Email', 'mwt'),
          'validation'  => 'email'
        ),
        array(
          'id'          => 'contact-phone',
          'type'        => 'text', 
          'title'       => __('Nomor Telp', 'mwt'),
        ),
        array(
          'id'          => 'contact-fax',
          'type'        => 'text', 
          'title'       => __('Nomor FAX', 'mwt'),
        ),
        array(
          'id'          => 'contact-mobile',
          'type'        => 'text', 
          'title'       => __('Nomor HP', 'mwt'),
        ),
        array(
          'id'          => 'contact-bbm',
          'type'        => 'text', 
          'title'       => __('PIN BBM', 'mwt'),
        ),
        array(
          'id'          => 'contact-wa1',
          'type'        => 'text', 
          'title'       => __('Whatsapp', 'mwt'),
          'desc'        => __('Nomor ini digunakan visitor untuk chat melalui whatsapp web.', 'mwt'),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Kontak 2', 'mwt' ),
    'id'     => 'mwt-company-contact2-option',
    'icon'  => 'el el-globe',
    'fields' => array(
        array(
          'id'          => 'contact-office2',
          'type'        => 'text', 
          'title'       => __('Nama Kantor 2', 'mwt'),
        ),
        array(
          'id'          => 'contact-address-2',
          'type'        => 'textarea', 
          'title'       => __('Alamat Kantor 2', 'mwt'),
        ),
        array(
          'id'          => 'contact-maps-2',
          'type'        => 'textarea', 
          'title'       => __('Embed Google Maps', 'mwt'),
        ),
        array(
          'id'          => 'contact-jam-operasional-2',
          'type'        => 'text', 
          'title'       => __('Jam Operasional Kantor 2', 'mwt'),
        ),
        array(
          'id'          => 'contact-email2',
          'type'        => 'text', 
          'title'       => __('Email', 'mwt'),
          'validation'  => 'email'
        ),
        array(
          'id'          => 'contact-phone2',
          'type'        => 'text', 
          'title'       => __('Nomor Telp', 'mwt'),
        ),
        array(
          'id'          => 'contact-fax2',
          'type'        => 'text', 
          'title'       => __('Nomor FAX', 'mwt'),
        ),
        array(
          'id'          => 'contact-mobile2',
          'type'        => 'text', 
          'title'       => __('Nomor HP', 'mwt'),
        ),
        array(
          'id'          => 'contact-bbm2',
          'type'        => 'text', 
          'title'       => __('PIN BBM', 'mwt'),
        ),
        array(
          'id'          => 'contact-wa2',
          'type'        => 'text', 
          'title'       => __('Whatsapp', 'mwt'),
        ),
//         array(
//           'id'          => 'contact-wa2',
//           'type'        => 'text', 
//           'title'       => __('Whatsapp 2', 'mwt'),
//         ),
//         array(
//           'id'          => 'contact-wa2-text',
//           'type'        => 'text', 
//           'title'       => __('Whatsapp 2 Teks', 'mwt'),
//         ),
//         array(
//           'id'          => 'contact-wa3',
//           'type'        => 'text', 
//           'title'       => __('Whatsapp 3', 'mwt'),
//         ),
//         array(
//           'id'          => 'contact-wa3-text',
//           'type'        => 'text', 
//           'title'       => __('Whatsapp 3 Teks', 'mwt'),
//         ),
    )
) );