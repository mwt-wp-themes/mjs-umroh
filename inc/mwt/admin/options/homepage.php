<?php

Redux::setSection( $opt_name, array(
    'title' => __( 'Homepage', 'mwt' ),
    'id'    => 'mwt-landing-page-options',
    'desc'  => __( 'Basic fields as subsections.', 'mwt' ),
    'icon'  => 'el el-home'
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Hero', 'mwt' ),
    'id'         => 'lp-header-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'header-banner-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Background Image', 'mwt'),
        ),
        array(
            'id'       => 'header-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
//         array(
//             'id'       => 'header-subtitle',
//             'type'     => 'text',
//             'title'    => __('Subtitle', 'mwt'),
//         ),
        array(
            'id'       => 'header-description',
            'type'     => 'textarea',
            'title'    => __('Description', 'mwt'),
//             'subtitle' => __('Create a new Gallery by selecting existing or uploading new images using the WordPress native uploader', 'mwt'),
//             'desc'     => __('This is the description field, again good for additional info.', 'mwt'),
        ),
//         array(
//             'id'       => 'homepage-slides-enabled',
//             'type'     => 'radio',
//             'title'    => __( 'Aktifkan Slider', 'mwt' ),
//             'options'  => array(
//                 '1' => 'Ya', 
//                 '0' => 'Tidak', 
//             ),
//         ),
//         array(
//             'id'       => 'homepage-slides',
//             'type'     => 'slides',
//             'title'    => __( 'Slider', 'mwt' ),
//             'subtitle'    => __('Unlimited slides with drag and drop sortings.', 'mwt'),
//             'desc'        => __('This field will store all slides values into a multidimensional array to use into a foreach loop.', 'mwt'),
//             'placeholder' => array(
//                 'title'           => __('This is a title', 'mwt'),
//                 'description'     => __('Description Here', 'mwt'),
//                 'url'             => __('Give us a link!', 'mwt'),
//             ),
//         ),
//         array(
//             'id'       => 'header-yt-url',
//             'type'     => 'text',
//             'title'    => __('ID Youtube Video', 'mwt'),
//             'desc'     => __('Hanya ID video, bukan URL', 'mwt'),
//         ),
//         array(
//             'id'       => 'header-enabled-button',
//             'type'     => 'radio',
//             'title'    => __('Enable Button', 'mwt'),
//             'options'  => array(
//                     '1' => 'Yes', 
//                     '2' => 'No', 
//                 ),
//             'default' => '2'
//         ),
//         array(
//             'id'       => 'header-button-text',
//             'type'     => 'text',
//             'title'    => __('Button Text', 'mwt'),
//         ),
//         array(
//             'id'       => 'header-button-url',
//             'type'     => 'text',
//             'title'    => __('Button URL', 'mwt'),
//         ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Selling Point', 'mwt' ),
    'id'         => 'mwt-selling-point-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'selling-point-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'selling-point-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
            'id'       => 'selling-point-slides',
            'type'     => 'slides',
            'title'    => __( 'Features', 'mwt' ),
            'placeholder' => array(
                'title'           => __('This is a title', 'mwt'),
                'description'     => __('Description Here', 'mwt'),
                'url'             => __('Give us a link!', 'mwt'),
            ),
        ),
//         array(
//           'id'          => 'selling-point-description',
//           'type'        => 'textarea', 
//           'title'       => __('Description', 'mwt'),
//         ),
//         array(
//           'id'          => 'selling-point-subtitle',
//           'type'        => 'text', 
//           'title'       => __('Subtitle', 'mwt'),
//         ),
//         array(
//             'id'       => 'selling-point-info-content-category',
//             'type'     => 'select',
//             'data' => 'terms',
//             'args' => array(
//                 'taxonomies' => array( 'info_content_category' ),
//             ),
//             'title'    => __('Select Category', 'mwt'),
//             'desc'     => __('Info Content Category', 'mwt'),
//         ),
    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Video', 'mwt' ),
    'id'         => 'mwt-video-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'video-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'video-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
            'id'       => 'video-description',
            'type'     => 'textarea',
            'title'    => __('Description', 'mwt'),
        ),
        array(
            'id'       => 'video-youtube-id',
            'type'     => 'text',
            'title'    => __('Youtube Video ID', 'mwt'),
        ),
    )
) );


Redux::setSection( $opt_name, array(
    'title'      => __( 'Top Promo', 'mwt' ),
    'id'         => 'mwt-top-promo-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'top-promo-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'top-promo-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
          'id'          => 'top-promo-subtitle',
          'type'        => 'text', 
          'title'       => __('Subtitle', 'mwt'),
        ),
        array(
          'id'          => 'top-promo-description',
          'type'        => 'textarea', 
          'title'       => __('Description', 'mwt'),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Testimonial', 'mwt' ),
    'id'         => 'mwt-testimonial-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'testimonial-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'testimonial-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Featured Paket Umroh', 'mwt' ),
    'id'         => 'mwt-paket-umroh-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'paket-umroh-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'paket-umroh-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-1',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'umroh_category' ),
                'hide_empty'  => false,
            ),
            'title'    => __('Featured 1', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-1-bg',
            'type'     => 'media',
            'title'    => __('Featured 1 Image', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-2',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'umroh_category' ),
                'hide_empty'  => false,
            ),
            'title'    => __('Featured 2', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-2-bg',
            'type'     => 'media',
            'title'    => __('Featured 2 Image', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-3',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'umroh_category' ),
                'hide_empty'  => false,
            ),
            'title'    => __('Featured 3', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-3-bg',
            'type'     => 'media',
            'title'    => __('Featured 3 Image', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-4',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'umroh_category' ),
                'hide_empty'  => false,
            ),
            'title'    => __('Featured 4', 'mwt'),
        ),
        array(
            'id'       => 'paket-umroh-category-4-bg',
            'type'     => 'media',
            'title'    => __('Featured 4 Image', 'mwt'),
        ),      
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Terbukti Terpercaya', 'mwt' ),
    'id'         => 'mwt-terbukti-terpercaya-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'terbukti-terpercaya-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'terbukti-terpercaya-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
            'id'       => 'terbukti-terpercaya-description',
            'type'     => 'textarea',
            'title'    => __('Description', 'mwt'),
        ),
        array(
            'id'       => 'terbukti-terpercaya-slides',
            'type'     => 'slides',
            'title'    => __( 'Content Slides', 'mwt' ),
            'placeholder' => array(
                'title'           => __('This is a title', 'mwt'),
                'description'     => __('Description Here', 'mwt'),
                'url'             => __('Give us a link!', 'mwt'),
            ),
        ),
    )
) );



Redux::setSection( $opt_name, array(
    'title'      => __( 'Featured News', 'mwt' ),
    'id'         => 'mwt-berita-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'berita-enabled',
            'type'     => 'radio',
            'title'    => __( 'Enable Section', 'mwt' ),
            'options'  => array(
                '1' => 'Yes', 
                '0' => 'No', 
            ),
            'default' => 0
        ),
        array(
            'id'       => 'berita-category-1',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'category' ),
            ),
            'title'    => __('Category 1', 'mwt'),
        ),
        array(
            'id'       => 'berita-category-2',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'category' ),
            ),
            'title'    => __('Category 2', 'mwt'),
        ),
        array(
            'id'       => 'berita-category-3',
            'type'     => 'select',
            'data' => 'terms',
            'args' => array(
                'taxonomies' => array( 'category' ),
            ),
            'title'    => __('Category 3', 'mwt'),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'      => __( 'Jadwal Umroh', 'mwt' ),
    'id'         => 'mwt-jadwal-umroh-options',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'jadwal-umroh-banner-img',
            'type'     => 'media',
            'url'      => true,
            'title'    => __('Feature Image', 'mwt'),
        ),
        array(
            'id'       => 'jadwal-umroh-title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'),
        ),
        array(
            'id'       => 'jadwal-umroh-subtitle',
            'type'     => 'text',
            'title'    => __('Subtitle', 'mwt'),
        ),
        array(
            'id'       => 'jadwal-umroh-button-title',
            'type'     => 'text',
            'title'    => __('Button Title', 'mwt'),
        ),
    )
) );
