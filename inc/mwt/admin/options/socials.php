<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Sosial Media', 'mjslp' ),
    'id'     => 'socials-option',
    'icon'   => 'el el-comment',
    'fields' => array(
        array(
          'id'          => 'social-fb',
          'type'        => 'text', 
          'title'       => __('URL Facebook', 'mjslp'),
          //'subtitle'    => __('Typography option with each property can be called individually.', 'mjslp'),
          'default'     => '#',
        ),
        array(
          'id'          => 'social-twitter',
          'type'        => 'text', 
          'title'       => __('URL Twitter', 'mjslp'),
          'default'     => '#',
        ),
        array(
          'id'          => 'social-googleplus',
          'type'        => 'text', 
          'title'       => __('URL Google+', 'mjslp'),
          'default'     => '#',
        ),
        array(
          'id'          => 'social-instagram',
          'type'        => 'text', 
          'title'       => __('URL Instagram', 'mjslp'),
          'default'     => '#',
        ),
        array(
          'id'          => 'social-youtube',
          'type'        => 'text', 
          'title'       => __('URL Youtube', 'mjslp'),
          'default'     => '#',
        ),
    )
) );