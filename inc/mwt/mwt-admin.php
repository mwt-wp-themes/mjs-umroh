<?php


// create custom plugin settings menu
add_action('admin_menu', 'mwt_create_mjs_menu');

function mwt_create_mjs_menu() {

  remove_submenu_page( 'index.php', 'clicky_analytics' );
 
	//create new top-level menu
	add_menu_page('MJS Settings', 'MJS', 'administrator', 'mwt_option', '' , 'dashicons-megaphone', 5 );
  
  add_submenu_page( 'mwt_option', 'Clicky Analytics', 'Clicky Analytics', 'manage_options', 'index.php?page=clicky_analytics');  
  add_submenu_page( 'mwt_option', 'Data Customer', 'Data Customer', 'manage_options', 'mjs_jamaah', 'mwt_display_jamaah_user_page');
  add_submenu_page( 'mwt_option', 'Data Mitra', 'Data Mitra', 'manage_options', 'mjs_mitra', 'mwt_display_mitra_user_page');
  

  // CUSTOM
  add_submenu_page( 'edit.php?post_type=mwt-umroh', 'Kategori Umroh', 'Kategori Umroh', 'manage_options', 'edit-tags.php?taxonomy=umroh_category&post_type=mwt-umroh');
  add_submenu_page( 'edit.php?post_type=mwt-umroh', 'Maskapai', 'Maskapai', 'manage_options', 'edit.php?post_type=mwt-maskapai');
//   add_submenu_page( 'edit.php?post_type=mwt-umroh', 'Kategori Umroh', 'Kategori Umroh', 'manage_options', 'edit-tags.php?taxonomy=region&post_type=mwt-umroh');

	//call register settings function
	add_action( 'admin_init', 'mwt_register_settings' );
}


function mwt_register_settings() {
	//register our settings
	register_setting( 'lzwf-settings-group', 'mwt_minimal_donasi' );
	register_setting( 'lzwf-settings-group', 'mwt_xendit_public_key' );
	register_setting( 'lzwf-settings-group', 'mwt_xendit_secret_key' );
  register_setting( 'lzwf-settings-group', 'mwt_xendit_validation_token' );
}

function mwt_display_jamaah_user_page() { ?>

    <style scoped>

        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .button-success {
            background: rgb(28, 184, 65); /* this is a green */
        }

        .button-error {
            background: rgb(202, 60, 60); /* this is a maroon */
        }

        .button-warning {
            background: rgb(223, 117, 20); /* this is an orange */
        }

        .button-secondary {
            background: rgb(66, 184, 221); /* this is a light blue */
        }

    </style>

    <div class="wrap">
      <h1 class="wp-heading-inline">Data Customer</h1>
      <a href="post-new.php?post_type=participants" class="page-title-action">Add New</a>
      
      <?php if( !empty( $_GET['deleted'] ) ) : ?>
      <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
<p><strong>Data customer berhasil dihapus.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Tutup pemberitahuan ini.</span></button></div>
      <?php endif; ?>
      
      <hr>
      
      <table id="example" class="pure-table pure-table-horizontal" width="100%">
              <thead>
                  <tr>
                      <th width="30">ID</th>
                      <th>Tanggal Daftar</th>
                      <th>Customer</th>
                      <th>Email</th>
                      <th>Nomor HP/WA</th>
                      <th>Jadwal Umroh</th>
                      <th>Mitra</th>
                      <th>Staf</th>
                      <th>&nbsp;</th>
                  </tr>
              </thead>
          </table>
			
    </div>

  <script>
  jQuery(document).ready(function($) {
      var table = $('#example').DataTable({
          order: [[ 1, "desc" ]],
          ajax: {
              url: ajaxurl,
              data: {
                action: 'mwt_get_data_jamaah'
              },
              dataSrc: 'data'
          },
          dom: 'Bfrtip',
          buttons: [
              'excel', 'pdf'
          ],
          columns: [
              { 
                 "data": "id",
                 "render": function(data, type, row, meta){
                    if(type === 'display'){
                        data = '<a href="post.php?post=' + data + '&action=edit">' + data + '</a>';
                    }
                    return data;
                 }
              }, 
              { 
                "type": "date",
                "data": "tanggal" 
              },
              { "data": "nama" },
              { "data": "email" },
              { "data": "hp" },
              { "data": "paket" },
              { "data": "mitra" },
              { "data": "staf" },
              { 
                 "data": null,
                 "render": function(data, type, row, meta){
                    if(type === 'display'){
                        data = '';
                        data += '<a href="post.php?post='+row.id+'&action=edit" class="thickbox button-primary pure-button">Edit</a>';
                        data += '<button class="delbut button-error pure-button" onclick="del_post(' + row.id + ')">Hapus</a>';
                    }
                    return data;
                 }
              }, 
          ]
      });
        
  } );
    
    
  function del_post(post_id) {
    if( confirm( 'Anda yakin ingin menghapus data ini?' ) ) {
      jQuery.post(
        ajaxurl, 
        {
            'action': 'mwt_hapus_jamaah',
            'id': post_id
        },
        function(response){
          window.location.replace('admin.php?page=mjs_jamaah&deleted=true');
        });
    }
  }
    
</script>

<?php } 

function mwt_display_general_settings_page() {
?>
    <div class="wrap" style="display:none">
    <div id="icon-themes" class="icon32"></div>
    <h2><?php _e('Follow-up Settings', 'woocommerce'); ?></h2>

			<?php settings_errors(); ?>

			<h2 class="nav-tab-wrapper">
					<a href="admin.php?page=laziswaf" class="nav-tab nav-tab-active">General</a>
					<a href="admin.php?page=lzwf-payment-gateway" class="nav-tab">Metode Pembayaran</a>
			</h2>
    
      <form method="post" action="options.php">
          <?php settings_fields( 'lzwf-settings-group' ); ?>
          <?php do_settings_sections( 'lzwf-settings-group' ); ?>
        
          <?php include __DIR__ . '/templates/lzwf-tab-general.php'; ?>

          <?php submit_button(); ?>

      </form>
      
    </div>

<?php } 

function mwt_display_mitra_user_page() { ?>

    <style scoped>

        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .button-success {
            background: rgb(28, 184, 65); /* this is a green */
        }

        .button-error {
            background: rgb(202, 60, 60); /* this is a maroon */
        }

        .button-warning {
            background: rgb(223, 117, 20); /* this is an orange */
        }

        .button-secondary {
            background: rgb(66, 184, 221); /* this is a light blue */
        }

    </style>

    <div class="wrap">
      
      <h2><?php _e('Data Mitra', 'lzwf'); ?></h2>
      
      <?php if( !empty( $_GET['deleted'] ) ) : ?>
      <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
<p><strong>Data donasi berhasil dihapus.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Tutup pemberitahuan ini.</span></button></div>
      <?php endif; ?>
      
      <hr>
      <p>Untuk menambah mitra, silahkan <a href="user-new.php?role=mitra">buat user baru</a> seperti biasa dengan memilih role "Mitra" pada halaman tambah user.</p>
      <?php add_thickbox(); ?>
      <div id="my-content-id" style="display:none;">
        <table class="form-table">
            <tr valign="top">
            <th scope="row">Nama</th>
            <td><input type="text" id="nama_mitra" /></td>
            </tr>

            <tr valign="top">
            <th scope="row">Email</th>
            <td><input type="email" id="email_mitra" /></td>
            </tr>

            <tr valign="top">
            <th scope="row">Nomor HP</th>
            <td><input type="text" id="hp_mitra" /></td>
            </tr>
          
            <tr valign="top">
            <th scope="row">Nomor WA</th>
            <td><input type="text" id="wa_mitra" /></td>
            </tr>
        </table>

        <input type="hidden" id="id_mitra" value="0">
        <p class="submit">
          <button type="button" onclick="update_mitra()" class="button button-primary"> 
            Save changes    
          </button>
        </p>
      </div>
      
      <table id="example" class="pure-table pure-table-horizontal" width="100%">
              <thead>
                  <tr>
                      <th>ID</th>
                      <th>Tanggal Daftar</th>
                      <th>Nama Mitra</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Nomor HP</th>
                      <th>Nomor WA</th>
                      <th>Staf CS</th>
                      <th>&nbsp;</th>
                  </tr>
              </thead>
          </table>
			
    </div>

  <script>
  jQuery(document).ready(function($) {
      var table = $('#example').DataTable({
          order: [[ 1, "desc" ]],
          ajax: {
              url: ajaxurl,
              data: {
                action: 'mwt_get_data_mitra'
              },
              dataSrc: 'data'
          },
          dom: 'Bfrtip',
          buttons: [
              'excel', 'pdf'
          ],
          columns: [
              { 
                 "data": "id",
                 "render": function(data, type, row, meta){
                    if(type === 'display'){
                        data = '<a href="post.php?post=' + data + '&action=edit">' + data + '</a>';
                    }
                    return data;
                 }
              }, 
              { 
                "type": "date",
                "data": "tanggal" 
              },
              { "data": "nama" },
              { "data": "username" },
              { "data": "email" },
              { "data": "hp" },
              { "data": "wa" },
              { "data": "staf" },
              { 
                 "data": null,
                 "render": function(data, type, row, meta){
                    if(type === 'display'){
                        data = '';
                        data += '<a href="#TB_inline?width=600&height=550&inlineId=my-content-id" class="thickbox button-primary pure-button" data-id="'+row.id+'" data-nama="'+row.nama+'" data-email="'+row.email+'"  data-wa="'+row.wa+'" data-hp="'+row.hp+'" onclick="set_form_data(this)">Edit</a>';
                        //data += '<button class="delbut button-error pure-button" onclick="del_post(' + row.id + ')">Hapus</a>';
                    }
                    return data;
                 }
              }, 
          ]
      });
    
      //$("div.toolbar").html('<?php echo $toolbar_html; ?>');
        
  } );
    
    
  function del_post(post_id) {
    if( confirm( 'Anda yakin ingin menghapus data ini?' ) ) {
      jQuery.post(
        ajaxurl, 
        {
            'action': 'lzwf_hapus_donasi',
            'id': post_id
        },
        function(response){
          window.location.replace('admin.php?page=laziswaf_donatur&deleted=true');
        });
    }
  }
    
  function set_form_data(el) {
    jQuery("#nama_mitra").val( jQuery(el).data('nama') );
    jQuery("#email_mitra").val( jQuery(el).data('email') );
    jQuery("#hp_mitra").val( jQuery(el).data('hp') );
    jQuery("#wa_mitra").val( jQuery(el).data('wa') );
    jQuery("#id_mitra").val( jQuery(el).data('id') );
  }
    
  function update_mitra() {
      jQuery.post(
        ajaxurl, 
        {
            'action': 'mwt_update_data_mitra',
            'id': jQuery("#id_mitra").val(),
            'nama': jQuery("#nama_mitra").val(),
            'email': jQuery("#email_mitra").val(),
            'hp': jQuery("#hp_mitra").val(),
            'wa': jQuery("#wa_mitra").val()
        },
        function(response){
          window.location.replace('admin.php?page=mjs_mitra');
        });
  }
</script>

<?php } 