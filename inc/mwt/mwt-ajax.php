<?php

add_action( 'wp_ajax_mwt_get_data_mitra', 'mwt_get_data_mitra' );
add_action( 'wp_ajax_nopriv_mwt_get_data_mitra', 'mwt_get_data_mitra' );
function mwt_get_data_mitra() {
  $result = [];
  // WP_User_Query arguments
  $args = array(
    'role'          => 'Mitra',
  );

  // The User Query
  $user_query = new WP_User_Query( $args );

  // The User Loop
  if ( ! empty( $user_query->results ) ) {
    foreach ( $user_query->results as $user ) {
      $staf = get_user_by( 'ID', intval( get_user_meta( $user->ID, '_staf', true ) ) );
      $result[] = array(
          'id'          => $user->ID,
          'tanggal'     => $user->user_registered,
          'nama'        => $user->display_name,
          'username'    => $user->user_login,
          'email'       => $user->user_email,
          'hp'          => get_user_meta( $user->ID, '_nomortelp', true ),
          'wa'          => get_user_meta( $user->ID, '_nomorhp', true ),
          'staf'        => ( is_object( $staf ) ) ? $staf->display_name : '-',
      );
      
    }
  }
  
	echo json_encode( [ 'data' => $result ] );
	wp_die(); 
  
}

add_action( 'wp_ajax_mwt_update_data_mitra', 'mwt_update_data_mitra' );
add_action( 'wp_ajax_nopriv_mwt_update_data_mitra', 'mwt_update_data_mitra' );
function mwt_update_data_mitra() {
  $result = [];
  $user_id = $_POST['id'];
  $display_name = $_POST['nama'];
  $nomorhp = $_POST['hp'];
  $nomorwa = $_POST['wa'];

  $user_id = wp_update_user( array( 
    'ID'            => $user_id, 
    'display_name'  => $display_name,
    'first_name'    => $display_name
  ) );

  if ( is_wp_error( $user_id ) ) {
    // There was an error, probably that user doesn't exist.
  } else {
    // Success!
    update_user_meta( $user_id, '_nomortelp', $nomorhp );
    update_user_meta( $user_id, '_nomorhp', $nomorwa );
  }	
  
	echo json_encode( $result );
	wp_die(); 
  
}

add_action( 'wp_ajax_mwt_get_data_jamaah', 'mwt_get_data_jamaah' );
add_action( 'wp_ajax_nopriv_mwt_get_data_jamaah', 'mwt_get_data_jamaah' );
function mwt_get_data_jamaah() {
  global $mwt;
  $result = [];
  
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'participants' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => -1,
//     'meta_name'              => '_status_verifikasi',
//     'meta_value'             => 'verified',
//     'meta_compare'           => '='
  );

  // The User Query
  $posts = get_posts( $args );

  foreach ( $posts as $post ) {
    if( !empty( Mwt::get_field( 'paket_umroh', $post->ID ) ) ) {
      $paket = Mwt::get_field( 'paket_umroh', $post->ID );
    } else {
      $paket = '';
    }
    
    $staf = Mwt::get_field( 'staf', $post->ID );
    $staf = ( !empty( $staf ) && is_object( $staf ) ) ? $staf->display_name : '';
    
    $mitra = Mwt::get_field( 'mitra', $post->ID );
    $mitra = ( !empty( $mitra ) && is_object( $mitra ) ) ? $mitra->display_name : '';
    
    $result[] = array(
        'id'          => $post->ID,
        'tanggal'     => $post->post_date,
        'nama'        => $post->post_title,
        'paket'       => ( is_object( $paket ) ) ? $paket->post_title : '',
        'email'       => Mwt::get_field( 'email', $post->ID ),
        'hp'          => Mwt::get_field( 'nomor_hp', $post->ID ),
        'staf'        => $staf,
        'mitra'        => $mitra,
    );

  }
  
	echo json_encode( [ 'data' => $result ] );
	wp_die(); 
  
}

add_action( 'wp_ajax_mwt_hapus_jamaah', 'mwt_hapus_jamaah' );
add_action( 'wp_ajax_nopriv_mwt_hapus_jamaah', 'mwt_hapus_jamaah' );
function mwt_hapus_jamaah() {
	$post_id = $_POST['id'];
  wp_delete_post( $post_id, true );
	wp_die();
}

add_action( 'wp_ajax_mwt_pesan_paket_umroh', 'mwt_pesan_paket_umroh' );
add_action( 'wp_ajax_nopriv_mwt_pesan_paket_umroh', 'mwt_pesan_paket_umroh' );
function mwt_pesan_paket_umroh() {
  global $mwt, $mwt_option, $mwt_wa;
  $result = array();
  $nonce = $_POST['security'];
  if ( ! wp_verify_nonce( $nonce, 'mwt-nonce' ) ) {
      // This nonce is not valid.
      die( 'Security check' ); 
  } else {
    
    $data = array();
    parse_str($_POST['data'], $data);
    
    // assign participant to staf
    $assignment = array();
    $user_query = new WP_User_Query( array( 'role__in' => [ 'staf_cs', 'staf_mjs' ] ) );
    foreach ( $user_query->get_results() as $staf ) {
      $participants = 0;
      $args = array( 
        'post_type'           => array( 'participants' ),
        'post_status'         => array( 'publish' ),
        'nopaging'            => true,
        'posts_per_page'      => -1,
        'meta_query'          => array(
           'relation'   => 'AND',
           array(
             'key'      => 'staf',
             'value'    => $staf->ID,
             'type'     => 'NUMERIC',
             'compare'  => '='
           ),
        ), 
      );
      $posts = get_posts( $args );
      $assignment[$staf->ID] = ( $participants+count($posts) );
    }
  
    if( count( $assignment ) > 0 ) {
      asort($assignment);
      $staf_assign = array_keys($assignment);
      $staf_id = $staf_assign[0];
    }
  
    $name = $data['nama'];
    $phone = $data['telepon'];
    $email = $data['email'];
    $paket_id = $data['paket_id'];

    $args = array(
      'post_title'    => $name,
      'post_status'   => 'publish',
      'post_author'   => 1,
      'post_type'     => 'participants'
    );
    $post_id = wp_insert_post( $args );
    $result['submit_id'] = $post_id;

    $paket = get_post( $paket_id );
    update_field( 'paket_umroh', $paket, $post_id );
    
    $mitra = get_user_by( 'id', $mwt->mitra['id'] );
    update_field( 'mitra', $mitra, $post_id );
    
    update_field( 'email', $email, $post_id );
    update_field( 'nomor_hp', $phone, $post_id );
    
    // sms verifikasi
    $kode_unik = str_pad( substr( mt_rand( 100000, time() ), 0, 6 ), 3, 0, STR_PAD_LEFT );
    update_post_meta( $post_id, '_status_verifikasi', '' );
    update_post_meta( $post_id, '_sms_kode_unik', $kode_unik );
  
    if( isset( $staf_id ) ) {
      update_field( 'staf', $staf_id, $post_id );
    }
    
    $mwt_wa = new Mwt_Wa();
    $sc = array(
      'kode_verifikasi'     => $kode_unik,
      'nama'                => $name,
      'tanggal'             => current_time('j F Y'),
      'paket_umroh'         => $paket->post_title,
      'provider'            => ( !empty( Mwt::get_field( 'provider', $paket->ID ) ) ) ? Mwt::get_field( 'provider', $paket->ID )->post_title : '',
      'staf_id'             => ( isset( $staf_id ) ) ? $staf_id : 0,
   	  'mitra_id'            => $mwt->mitra['id']
    );
    $mwt_wa->send_staf_notification( array(
      'nomor' => $phone,
    ), $sc );
    
  }

  echo json_encode( $result );
  wp_die();
}

/**
 * Verifikasi SMS Action
 */
add_action( 'wp_ajax_mwt_verifikasi_sms_action', 'mwt_verifikasi_sms_action' );
add_action( 'wp_ajax_nopriv_mwt_verifikasi_sms_action', 'mwt_verifikasi_sms_action' );
function mwt_verifikasi_sms_action() {
  global $mwt, $mwt_option;
  $result = '';
  $result['error'] = 1;
  if( !empty( $_POST['submit_id'] ) ) {
    
    $post_id = intval( $_POST['submit_id'] );
    $kode_unik = $_POST['kode_verifikasi'];
    
    if( $kode_unik == get_post_meta( $post_id, '_sms_kode_unik', true ) ) {
      update_post_meta( $post_id, '_status_verifikasi', 'verified' ); 
      $result = array(
        'error'     => 0,
        'msg'       => 'Terima kasih! Staf kami akan segera menghubungi anda.',
        'redirect'  => get_permalink( $mwt_option['thankyou-page-id'] ),
      );
      
      $staf = get_field( 'staf', $post_id );
      if( !empty( $staf ) ) {
        $to = $staf->user_email;
        $subject = 'New Assignment';
        $body = '
          <p>Halo ' . $staf->display_name . '</p>
          <p>Anda mendapatkan 1 customer untuk di follow up. Silahkan login ke akun anda untuk mengelola customer.</p>
          <p>Terima kasih</p>
        ';
        $headers = array('Content-Type: text/html; charset=UTF-8');
        $headers[] = 'From: ' . get_bloginfo('name') . ' <info@umrohbermakna.com>';
        $headers[] = 'Cc: info@umrohbermakna.com';

        wp_mail( $to, $subject, $body, $headers );
      }
      
    } else {
        $result['msg'] = 'Kode verifikasi salah!';
    }

  } else {
    $result['msg'] = 'Data tidak ditemukan!';
  }
  
	echo json_encode( $result );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_followup_result', 'mwt_update_followup_result' );
add_action( 'wp_ajax_nopriv_mwt_update_followup_result', 'mwt_update_followup_result' );
function mwt_update_followup_result() {
  global $mwt, $mwt_option;
  
  $result = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'follow_up_result', $result, $post_id );
  
	echo json_encode( $result );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_spv_followup_result', 'mwt_update_spv_followup_result' );
add_action( 'wp_ajax_nopriv_mwt_update_spv_followup_result', 'mwt_update_spv_followup_result' );
function mwt_update_spv_followup_result() {
  global $mwt, $mwt_option;
  
  $result = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'spv_notes', $result, $post_id );
  
  $post = get_post( $post_id );
  if( $post ) {
    if( !empty( get_field('spv') ) ) {
      $spv = get_field('spv');
      $nomer_wa = get_user_meta( $spv->ID, '_nomorhp', true );
      if( !empty( $nomer_wa ) ) {
        $mwt_wa = new Mwt_Wa();
        $mwt_wa->send_request( array(
          'nomor' => $nomer_wa,
          'pesan' => $result
        )); 
      }
    } 
  }
  
	echo json_encode( $result );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_komisi_status', 'mwt_update_komisi_status' );
add_action( 'wp_ajax_nopriv_mwt_update_komisi_status', 'mwt_update_komisi_status' );
function mwt_update_komisi_status() {
  global $mwt, $mwt_option;
  
  $status = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'status_komisi', $status, $post_id );
  
	echo json_encode( 1 );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_cust_status', 'mwt_update_cust_status' );
add_action( 'wp_ajax_nopriv_mwt_update_cust_status', 'mwt_update_cust_status' );
function mwt_update_cust_status() {
  global $mwt, $mwt_option;
  
  $status = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'status', $status, $post_id );
  
	echo json_encode( 1 );
	wp_die();
}

add_action( 'wp_ajax_mwt_update_komisi_mitra', 'mwt_update_komisi_mitra' );
add_action( 'wp_ajax_nopriv_mwt_update_komisi_mitra', 'mwt_update_komisi_mitra' );
function mwt_update_komisi_mitra() {
  global $mwt, $mwt_option;
  
  $komisi = $_POST['value'];
  
  $post_id = $_POST['pk'];
  update_field( 'komisi_mitra', $komisi, $post_id );
  
	echo json_encode( 1 );
	wp_die();
}

add_action( 'wp_ajax_mwt_contact_form_action', 'mwt_contact_form_action' );
add_action( 'wp_ajax_nopriv_mwt_contact_form_action', 'mwt_contact_form_action' );
function mwt_contact_form_action() {
  global $mwt, $mwt_option;
  $result = array();
  $nonce = $_POST['security'];
  if ( ! wp_verify_nonce( $nonce, 'mwt-nonce' ) ) {
      // This nonce is not valid.
      die( 'Security check' ); 
  } else {
    

    $nama = $_POST['nama'];
    $email = $_POST['email'];
    $telepon = $_POST['telepon'];
    $kota = $_POST['kota'];
    $pesan = $_POST['pesan'];
    
    $to = 'info@umrohbermakna.com';
    $subject = 'Contact form';
    $body = $pesan;
    $headers = array('Content-Type: text/html; charset=UTF-8');
    $headers[] = 'From: ' . $nama . ' <' . $email . '>';
    $headers[] = 'Cc: info@umrohbermakna.com';

    wp_mail( $to, $subject, $body, $headers );
    
  }

  echo json_encode( $result );
  wp_die();
}

add_action( 'wp_ajax_mwt_assign_spv', 'mwt_assign_spv' );
add_action( 'wp_ajax_nopriv_mwt_assign_spv', 'mwt_assign_spv' );
function mwt_assign_spv() {
  
  $result = $_POST['value'];
  $post_id = $_POST['pk'];
  $spv = get_user_by( 'ID', $result );
  update_field( 'spv', $spv, $post_id );
  
	echo json_encode( $result );
	wp_die();
}