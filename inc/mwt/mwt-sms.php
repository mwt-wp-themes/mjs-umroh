<?php

class Mwt_Sms_Notifikasi {
  
  private $userkey = "tovic24";
  private $passkey = "mitra7878";
  
  public function send( $phone, $message = '' ) {
    $phone = ltrim( $phone, "0" );
    $phone = ltrim( $phone, "62" );
    $phone = ltrim( $phone, "+62" );
    $phone = trim( "0" . $phone );
    $userkey = $this->userkey;
    $passkey = $this->passkey;
    $ch = curl_init();
    curl_setopt( $ch, CURLOPT_URL, "http://reguler.sms-notifikasi.com/apps/smsapi.php" );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, "userkey=".$userkey."&passkey=".$passkey."&nohp=".$phone."&pesan=".urlencode($message) );
    curl_setopt( $ch, CURLOPT_HEADER, 0 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
    curl_setopt( $ch, CURLOPT_POST, 1 );
    $results = curl_exec($ch);
    curl_close($ch);

  }
  
  public function get_message_tags( $params = array() ) {
    global $mwt_option;
    $message = $mwt_option['mwt-sms-content'];
    if( !empty( $params['sc'] ) ) {
      foreach( $params['sc'] as $tag => $value ) {
        $message = str_replace( "[".$tag."]", $value, $message );
      } 
    }
    return nl2br($message);
  }
  
}