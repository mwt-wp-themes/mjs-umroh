<?php

class Mwt_Phone_Finder_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mwt_phone_finder_widget', // Base ID
			esc_html__( 'SurgaTekno Device Finder', 'understrap' ), // Name
			array( 'description' => esc_html__( 'Menampilkan device finder.', 'understrap' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		//echo $args['before_widget']; 
		global $mwt_option; ?>

    <div class="card card-phone-finder-box widget">
      <div class="card-body">
        <?php
          if ( ! empty( $instance['title'] ) ) {
            echo '<h5 class="card-title text-center"><i class="icon ion-ios-search" aria-hidden="true"></i> ' . $instance['title'] . '</h5>';
          } else {
            echo '<h5 class="card-title text-center"><i class="icon ion-ios-search" aria-hidden="true"></i> Device Finder</h5>';
          }
        ?>
        <hr>
        <div class="row">
          <?php
          $terms = get_terms( 'hp_brand', array(
              'hide_empty' => true,
          ) );
          foreach( $terms as $term ) { ?>
          <div class="col-4 hp-brand-item">
            <a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><?php echo $term->name; ?></a>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="rounded-bottom mdb-color">
        <ul class="nav md-pills nav-justified">
          <li class="nav-item">
            <a class="nav-link white-text" href="<?php echo get_permalink( $mwt_option['brand-page-id'] ); ?>"><i class="icon ion-ios-list" aria-hidden="true"></i> ALL BRANDS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link white-text" href="#!"><i class="icon ion-ios-planet" aria-hidden="true"></i> RUMOR MILL</a>
          </li>
        </ul>
      </div>
    </div>

		<?php
		//echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'understrap' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}

function register_mwt_phone_finder_widget() {
    register_widget( 'Mwt_Phone_Finder_Widget' );
}
add_action( 'widgets_init', 'register_mwt_phone_finder_widget' );