<?php

class Mwt_Top_Ten_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'mwt_top_ten_widget', // Base ID
			esc_html__( 'SurgaTekno Top 10', 'understrap' ), // Name
			array( 'description' => esc_html__( '', 'understrap' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
    global $mwt_option;
		//echo $args['before_widget'];  ?>

    <div class="card widget widget_mwt_top_ten_widget">
      <div class="card-header">
        <?php
          if ( ! empty( $instance['title'] ) ) {
            echo $instance['title'];
          } else {
            echo '<h5 class="card-title">Latest Devices</h5>';
          }
        ?>
      </div>
      <table class="table table-sm table-hover table-borderless table-striped">
        <thead>
          <tr>
            <th width="25"></th>
            <th>Device</th>
            <th>Favorites</th>
          </tr>
        </thead>
        <tbody>
          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'mwt-spec-hp' ),
            'post_status'            => array( 'publish' ),
            'nopaging'               => true,
            'posts_per_page'         => 10,
            'order'                  => 'DESC',
            'orderby'                => 'date',
          );
    
          if( is_array( $instance['term_id'] ) && count($instance['term_id']) > 0 ) {
//             if( !in_array( 0, $instance['term_id'] ) ) {
//               $args['category__in'] = $instance['term_id']; 
//             }
          }

          // The Query
          $news_query = new WP_Query( $args );

          // The Loop
          if ( $news_query->have_posts() ) {
            $count = 1;
            while ( $news_query->have_posts() ) {
              $news_query->the_post(); ?>
              <tr>
                <th scope="row" class="text-right"><?php echo $count; ?></th>
                <td><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td>
                <td><?php get_post_meta( get_the_ID(), '_favorited', true ); ?></td>
              </tr>
              <?php $count++;
            }
          }

          // Restore original Post Data
          wp_reset_postdata();

          ?>
        </tbody>
      </table>
    </div>

		<?php
		//echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		?>
		<p>
		<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'understrap' ); ?></label> 
		<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}

function register_mwt_top_ten_widget() {
    register_widget( 'Mwt_Top_Ten_Widget' );
}
add_action( 'widgets_init', 'register_mwt_top_ten_widget' );