<?php

class Mwt {
  
  public $mitra;
  private $prefix = "mwt";
  
  public function __construct() {
    $this->load_dependencies();
    add_action( 'tgmpa_register', array( $this, 'register_required_plugins' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'dynamic_enqueue_scripts' ) );
    
    // hide admin bar
    add_filter('show_admin_bar', '__return_false');
    $this->mitra = $this->get_data_mitra();
    if( is_admin() ) {
      $this->metaboxes = new Mwt_MetaBoxes();
      add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );
      add_action( 'show_user_profile', array( $this, 'my_show_extra_profile_fields' ) );
      add_action( 'edit_user_profile', array( $this, 'my_show_extra_profile_fields' ) );
      add_action( 'personal_options_update', array( $this, 'my_save_extra_profile_fields' ) );
      add_action( 'edit_user_profile_update', array( $this, 'my_save_extra_profile_fields' ) );
      add_role(
          'staf_cs',
          __( 'Staf CS' ),
          array(
              'read'         => true,  // true allows this capability
              'edit_posts'   => true,
          )
      );
      add_role(
          'staf_spv',
          __( 'Staf SPV' ),
          array(
              'read'         => true,  // true allows this capability
              'edit_posts'   => true,
          )
      );
      add_role(
          'staf_manager',
          __( 'Staf Manager' ),
          array(
              'read'         => true,  // true allows this capability
              'edit_posts'   => true,
          )
      );
      add_role(
          'mitra',
          __( 'Mitra' ),
          array(
              'read'         => true,  // true allows this capability
              'edit_posts'   => true,
          )
      );
    }
  }  
  
  public function load_dependencies() {
    if( session_id() == '' ) session_start();
    if( is_admin() ) {
      require_once $this->include_path( 'utilities/tgmp-activation/class-tgm-plugin-activation.php' );
      require_once $this->include_path( 'mwt-admin.php' );
    }
    require_once $this->include_path( 'utilities/mwt-cpt-tour.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-maskapai.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-participants.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-galery.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-galery-video.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-testimonials.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-partners.php' );
    require_once $this->include_path( 'utilities/mwt-cpt-wisata-muslim.php' );
    require_once $this->include_path( 'utilities/mwt-custom-login.php' );
    require_once $this->include_path( 'utilities/mwt-metaboxes.php' );
    require_once $this->include_path( 'mwt-helpers.php' );
    require_once $this->include_path( 'admin/admin-init.php' );
    //require_once $this->include_path( 'mwt-sms.php' );
    require_once $this->include_path( 'mwt-wa.php' );
    require_once $this->include_path( 'mwt-ajax.php' );    
  }
  
  private function include_path( $file ) {
    return get_template_directory() . '/inc/' . $this->prefix . '/' . ltrim( $file, '/' );
  }
  
  public function set_mitra() {
    //header("HTTP/1.1 301 Moved Permanently");
    $username = basename( $_SERVER['REQUEST_URI'] );
    $mitra = get_user_by( 'login', $username );
    if ( !$mitra ) {
      return;
    }
    unset( $_COOKIE['_mjs_mitra'] );
    session_regenerate_id();
    $path = parse_url(get_option('siteurl'), PHP_URL_PATH);
    $host = parse_url(get_option('siteurl'), PHP_URL_HOST);
    $expiry = strtotime('+1 month');
    setcookie( '_mjs_mitra', $mitra->ID, $expiry, '/', $_SERVER['HTTP_HOST'] );
    header("Location: ".get_bloginfo('url'));
    exit();
  }
  
  public function get_data_mitra() {
    global $mwt_option;
    $data = array();
    if( !empty( $_COOKIE['_mjs_mitra'] ) ) {
      $mitra = get_userdata( $_COOKIE['_mjs_mitra'] );
    }
//     $data['id'] = ( isset( $mitra ) ) ? $mitra->ID : 0;
//     $data['nama'] = ( isset( $mitra ) ) ? $mitra->first_name : get_bloginfo('name');
//     $data['email'] = ( isset( $mitra ) ) ? $mitra->user_login : $mwt_option['contact-email'];
//     $data['hp'] = ( isset( $mitra ) ) ? get_user_meta( $mitra->ID, '_nomorhp', true ) : $mwt_option['contact-mobile'];
//     $data['telp'] = ( isset( $mitra ) ) ? get_user_meta( $mitra->ID, '_nomortelp', true ) : $mwt_option['contact-phone'];
    $data['id'] = ( isset( $mitra ) ) ? $mitra->ID : 0;
    $data['nama'] = get_bloginfo('name');
    $data['email'] = $mwt_option['contact-email'];
    $data['hp'] = $mwt_option['contact-mobile'];
    $data['telp'] = $mwt_option['contact-phone'];
    
    return $data;
  }
  
  public function register_admin_scripts( $hook ) {
    if (  !in_array( $hook, ['mjs_page_mjs_mitra', 'mjs_page_mjs_jamaah' ] ) ) {
        return;
    }
    wp_enqueue_style( 'admin_pure_css', 'https://unpkg.com/purecss@1.0.0/build/pure-min.css' );
    wp_enqueue_style( 'admin_datatable_css', get_template_directory_uri() . '/assets/DataTables/datatables.min.css' );
    wp_enqueue_script( 'admin_datatable_script', get_template_directory_uri() . '/assets/DataTables/datatables.min.js', array('jquery') );
  }
  
  public function enqueue_scripts() {
    wp_enqueue_style( 'pure-css', '//unpkg.com/purecss@1.0.0/build/pure-min.css' );
    wp_enqueue_style( 'font-dosis-css', get_template_directory_uri() . '/assets/css/dosiscss.css' );
    wp_enqueue_style( 'owl-carousel-css', '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css' );
    wp_enqueue_style( 'owl-carousel-theme-css', '//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css' );    
    wp_enqueue_style( 'font-awesome-css', '//stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'jssocials-css', '//cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css' );
    wp_enqueue_style( 'jssocials-theme-css', '//cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-minima.css' );
    wp_enqueue_style( 'main-css', get_template_directory_uri() . '/assets/css/main.min.css' );
    wp_enqueue_style( 'mwt-style', get_stylesheet_uri() );
    
    // Register scripts
    wp_enqueue_script( 'jquery-js', get_stylesheet_directory_uri() . '/assets/js/jquery.min.js', array(), '', false ); 
    wp_enqueue_script( 'loadingoverlay-scripts', '//cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js', array(), '', false ); 
    wp_enqueue_script( 'jssocials-scripts', '//cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js', array(), '', false ); 
    wp_enqueue_script( 'script-scripts', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery-js'), '', false ); // modernizr
    wp_enqueue_script( 'owl-carousel-scripts', get_stylesheet_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery-js'), '', false ); 
    wp_enqueue_script( 'app-scripts' );
    wp_register_script( 'app-scripts', get_stylesheet_directory_uri() . '/assets/js/app.js' );
    wp_localize_script( 'app-scripts', 'mjs_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'wp_nonce' => wp_create_nonce( 'mwt-nonce' ) ) );
    wp_enqueue_script( 'app-scripts' );
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }
  }
  
  public function dynamic_enqueue_scripts() {
    $scripts = '';    
    if( basename( get_page_template_slug() ) == "template-account.php" ) {
      wp_enqueue_style( 'datatable-css', get_template_directory_uri() . '/assets/DataTables/datatables.min.css' );
      wp_enqueue_style( 'jquery-editable-css', 'https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/css/jquery-editable.css' );
      wp_enqueue_script( 'jquery-poshytip-script', 'https://cdnjs.cloudflare.com/ajax/libs/poshytip/1.2/jquery.poshytip.min.js', array('jquery-js') );
      wp_enqueue_script( 'jquery-editable-script', 'https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/jquery-editable/js/jquery-editable-poshytip.min.js', array('jquery-js') );
      wp_enqueue_script( 'datatable-script',  get_template_directory_uri() . '/assets/DataTables/datatables.min.js', array('jquery-js') );
    }
    
    
    if( !is_single() || !is_singular() ) {
      wp_enqueue_script( 'player-js', get_template_directory_uri() . '/assets/js/jquery.ubaplayer.js', array( 'jquery-js' ), '', true );

      $scripts .= '
          jQuery("document").ready(function($){
            var popup_DoaTawaf = $(\'#popup_DoaTawaf\'), popup_blok_klos_DoaTawaf = $(\'#popup_blok_klos_DoaTawaf\');
            popup_DoaTawaf.hide();
            popup_blok_klos_DoaTawaf.on(\'click\', function(){ popup_DoaTawaf.hide(); });
            $(\'#buka_popup_DoaTawaf\').on(\'click\', function(){ popup_DoaTawaf.show(); });
          });
          jQuery(function(){
              jQuery("#DoaTawaf_Player").ubaPlayer({ codecs: [{name:"MP3", codec: \'audio/mpeg;\'}] });
          });
      '; 
    }
    
    $scripts .= '
        $(document).ready(function() {
          var hju_notif_klos = $("#hju_notif_klos"),
              hju_notif = $("#hju_notif");

          hju_notif_klos.click(function(){
          hju_notif.slideUp(240);			  	
          });
          
          $("body").find(".menu-item-has-children").addClass("kmm_menu_anakan");
          $(".kmm_menu_anakan").find("ul").addClass("kmm_menu ");
          $(".kmm_menu_anakan a").first().append("<i></i>");
        });
    ';
    
    wp_enqueue_script( 'app-scripts', get_template_directory_uri() . '/assets/js/app.js', array('jquery-js') );
    wp_add_inline_script( 'jquery-js', trim($scripts) );
  }
  
  public static function get_field( $name, $post_id = '' ) {
    if( !function_exists( 'get_field' ) ) {
      return;
    }
    $post_id = ( '' != $post_id ) ? $post_id : get_the_ID();
    return get_field( $name, $post_id );
  }
  
  public static function dump( $var, $exit = false ) {
    echo "<pre>"; var_dump( $var ); echo "</pre>";
    if( $exit ) exit;
  }
  
  public function register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(

      array(
        'name'        => 'Advanced Custom Fields',
        'slug'        => 'advanced-custom-fields-pro',
        'required'    => true,
      ),
      
      array(
        'name'        => 'Disqus for WordPress',
        'slug'        => 'disqus-comment-system',
      ),
      
      array(
        'name'        => 'Custom User Profile Photo',
        'slug'        => 'custom-user-profile-photo',
      ),
      
      array(
        'name'        => 'Clicky Analytics',
        'slug'        => 'clicky-analytics',
      ),
      
      array(
        'name'        => 'WordPress SEO by Yoast',
        'slug'        => 'wordpress-seo',
        'is_callable' => 'wpseo_init',
      ),

    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
      'id'           => 'mwt_tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
      'default_path' => '',                      // Default absolute path to bundled plugins.
      'menu'         => 'tgmpa-install-plugins', // Menu slug.
      'parent_slug'  => 'mwt_options',            // Parent menu slug.
      'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
      'has_notices'  => true,                    // Show admin notices or not.
      'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
      'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
      'is_automatic' => true,                   // Automatically activate plugins after installation or not.
      'message'      => '',                      // Message to output right before the plugins table.
    );

    tgmpa( $plugins, $config );
  }
  
  public function my_show_extra_profile_fields( $user ) { ?>
  <h3>Extra profile information</h3>
      <table class="form-table">
  <tr>
              <th><label for="phone">Nomor Whatsapp</label></th>
              <td>
              <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( '_nomorhp', $user->ID ) ); ?>" class="regular-text" /><br />
                  <span class="description">Please enter your phone number.</span>
              </td>
  </tr>
  </table>
  <?php }

  public function my_save_extra_profile_fields( $user_id ) {

  if ( !current_user_can( 'edit_user', $user_id ) )
      return false;

  update_usermeta( $user_id, '_nomorhp', $_POST['phone'] );
  }
}

$mwt = new Mwt();
$mwt_login = new Mwt_Login();