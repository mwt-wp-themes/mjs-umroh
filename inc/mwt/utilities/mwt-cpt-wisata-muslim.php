<?php

// Register Custom Post Type
function mwt_wisata_muslim_post_type() {

	$labels = array(
		'name'                  => _x( 'Wisata Muslim', 'Post Type General Name', 'mwt' ),
		'singular_name'         => _x( 'Wisata Muslim', 'Post Type Singular Name', 'mwt' ),
		'menu_name'             => __( 'Wisata Muslim', 'mwt' ),
		'name_admin_bar'        => __( 'Wisata Muslim', 'mwt' ),
		'archives'              => __( 'Item Archives', 'mwt' ),
		'attributes'            => __( 'Item Attributes', 'mwt' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mwt' ),
		'all_items'             => __( 'Semua Jadwal', 'mwt' ),
		'add_new_item'          => __( 'Add New Item', 'mwt' ),
		'add_new'               => __( 'Add New', 'mwt' ),
		'new_item'              => __( 'New Item', 'mwt' ),
		'edit_item'             => __( 'Edit Item', 'mwt' ),
		'update_item'           => __( 'Update Item', 'mwt' ),
		'view_item'             => __( 'View Item', 'mwt' ),
		'view_items'            => __( 'View Items', 'mwt' ),
		'search_items'          => __( 'Search Item', 'mwt' ),
		'not_found'             => __( 'Not found', 'mwt' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mwt' ),
		'featured_image'        => __( 'Featured Image', 'mwt' ),
		'set_featured_image'    => __( 'Set featured image', 'mwt' ),
		'remove_featured_image' => __( 'Remove featured image', 'mwt' ),
		'use_featured_image'    => __( 'Use as featured image', 'mwt' ),
		'insert_into_item'      => __( 'Insert into item', 'mwt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mwt' ),
		'items_list'            => __( 'Items list', 'mwt' ),
		'items_list_navigation' => __( 'Items list navigation', 'mwt' ),
		'filter_items_list'     => __( 'Filter items list', 'mwt' ),
	);
	$rewrite = array(
		'slug'                  => 'wisata-muslim',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Wisata Muslim', 'mwt' ),
		'description'           => __( 'Wisata Muslim Description', 'mwt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-tag',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'paket-wisata-muslim',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'mwt-wisata-muslim', $args );

}
add_action( 'init', 'mwt_wisata_muslim_post_type', 0 );