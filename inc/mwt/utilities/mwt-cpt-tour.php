<?php

// Register Custom Post Type
function mwt_tour_post_type() {

	$labels = array(
		'name'                  => _x( 'Paket Umroh', 'Post Type General Name', 'mwt' ),
		'singular_name'         => _x( 'Paket Umroh', 'Post Type Singular Name', 'mwt' ),
		'menu_name'             => __( 'Paket Umroh', 'mwt' ),
		'name_admin_bar'        => __( 'Paket Umroh', 'mwt' ),
		'archives'              => __( 'Item Archives', 'mwt' ),
		'attributes'            => __( 'Item Attributes', 'mwt' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mwt' ),
		'all_items'             => __( 'Semua Jadwal', 'mwt' ),
		'add_new_item'          => __( 'Add New Item', 'mwt' ),
		'add_new'               => __( 'Add New', 'mwt' ),
		'new_item'              => __( 'New Item', 'mwt' ),
		'edit_item'             => __( 'Edit Item', 'mwt' ),
		'update_item'           => __( 'Update Item', 'mwt' ),
		'view_item'             => __( 'View Item', 'mwt' ),
		'view_items'            => __( 'View Items', 'mwt' ),
		'search_items'          => __( 'Search Item', 'mwt' ),
		'not_found'             => __( 'Not found', 'mwt' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mwt' ),
		'featured_image'        => __( 'Featured Image', 'mwt' ),
		'set_featured_image'    => __( 'Set featured image', 'mwt' ),
		'remove_featured_image' => __( 'Remove featured image', 'mwt' ),
		'use_featured_image'    => __( 'Use as featured image', 'mwt' ),
		'insert_into_item'      => __( 'Insert into item', 'mwt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mwt' ),
		'items_list'            => __( 'Items list', 'mwt' ),
		'items_list_navigation' => __( 'Items list navigation', 'mwt' ),
		'filter_items_list'     => __( 'Filter items list', 'mwt' ),
	);
	$rewrite = array(
		'slug'                  => 'umroh',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Paket Umroh', 'mwt' ),
		'description'           => __( 'Paket Umroh Description', 'mwt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		//'taxonomies'            => array( 'umroh_category', 'region' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
    'menu_icon'             => 'dashicons-tag',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'paket-umroh',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page',
	);
	register_post_type( 'mwt-umroh', $args );

}
add_action( 'init', 'mwt_tour_post_type', 0 );


// Register Custom Taxonomy
function mwt_umroh_category_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Kategori Umroh', 'Taxonomy General Name', 'mwt' ),
		'singular_name'              => _x( 'Kategori Umroh', 'Taxonomy Singular Name', 'mwt' ),
		'menu_name'                  => __( 'Kategori Umroh', 'mwt' ),
		'all_items'                  => __( 'All Categories', 'mwt' ),
		'parent_item'                => __( 'Parent Category', 'mwt' ),
		'parent_item_colon'          => __( 'Parent Category:', 'mwt' ),
		'new_item_name'              => __( 'New Category Name', 'mwt' ),
		'add_new_item'               => __( 'Add New Category', 'mwt' ),
		'edit_item'                  => __( 'Edit Category', 'mwt' ),
		'update_item'                => __( 'Update Category', 'mwt' ),
		'view_item'                  => __( 'View Item', 'mwt' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'mwt' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'mwt' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'mwt' ),
		'popular_items'              => __( 'Popular Items', 'mwt' ),
		'search_items'               => __( 'Search categories', 'mwt' ),
		'not_found'                  => __( 'Not Found', 'mwt' ),
		'no_terms'                   => __( 'No items', 'mwt' ),
		'items_list'                 => __( 'Items list', 'mwt' ),
		'items_list_navigation'      => __( 'Items list navigation', 'mwt' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'umroh_category', array(), $args );

}
add_action( 'init', 'mwt_umroh_category_taxonomy', 0 );

// function mwt_region_taxonomy() {

// 	$labels = array(
// 		'name'                       => _x( 'Regions', 'Taxonomy General Name', 'mwt' ),
// 		'singular_name'              => _x( 'Region', 'Taxonomy Singular Name', 'mwt' ),
// 		'menu_name'                  => __( 'Region', 'mwt' ),
// 		'all_items'                  => __( 'All Regions', 'mwt' ),
// 		'parent_item'                => __( 'Parent Region', 'mwt' ),
// 		'parent_item_colon'          => __( 'Parent Region:', 'mwt' ),
// 		'new_item_name'              => __( 'New Region Name', 'mwt' ),
// 		'add_new_item'               => __( 'Add New Region', 'mwt' ),
// 		'edit_item'                  => __( 'Edit Region', 'mwt' ),
// 		'update_item'                => __( 'Update Region', 'mwt' ),
// 		'view_item'                  => __( 'View Item', 'mwt' ),
// 		'separate_items_with_commas' => __( 'Separate regions with commas', 'mwt' ),
// 		'add_or_remove_items'        => __( 'Add or remove regions', 'mwt' ),
// 		'choose_from_most_used'      => __( 'Choose from the most used regions', 'mwt' ),
// 		'popular_items'              => __( 'Popular Items', 'mwt' ),
// 		'search_items'               => __( 'Search regions', 'mwt' ),
// 		'not_found'                  => __( 'Not Found', 'mwt' ),
// 		'no_terms'                   => __( 'No items', 'mwt' ),
// 		'items_list'                 => __( 'Items list', 'mwt' ),
// 		'items_list_navigation'      => __( 'Items list navigation', 'mwt' ),
// 	);
// 	$args = array(
// 		'labels'                     => $labels,
// 		'hierarchical'               => true,
// 		'public'                     => true,
// 		'show_ui'                    => true,
// 		'show_admin_column'          => true,
// 		'show_in_nav_menus'          => true,
// 		'show_tagcloud'              => true,
// 	);
// 	register_taxonomy( 'region', array( 'mwt-umroh' ), $args );

// }
// add_action( 'init', 'mwt_region_taxonomy', 0 );