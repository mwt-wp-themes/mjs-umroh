<?php

// Register Custom Post Type
function mwt_galery_post_type() {

	$labels = array(
		'name'                  => _x( 'Gallery', 'Post Type General Name', 'mwt' ),
		'singular_name'         => _x( 'Galery', 'Post Type Singular Name', 'mwt' ),
		'menu_name'             => __( 'Gallery', 'mwt' ),
		'name_admin_bar'        => __( 'Galery', 'mwt' ),
		'archives'              => __( 'Item Archives', 'mwt' ),
		'attributes'            => __( 'Item Attributes', 'mwt' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mwt' ),
		'all_items'             => __( 'All Items', 'mwt' ),
		'add_new_item'          => __( 'Add New Item', 'mwt' ),
		'add_new'               => __( 'Add New', 'mwt' ),
		'new_item'              => __( 'New Item', 'mwt' ),
		'edit_item'             => __( 'Edit Item', 'mwt' ),
		'update_item'           => __( 'Update Item', 'mwt' ),
		'view_item'             => __( 'View Item', 'mwt' ),
		'view_items'            => __( 'View Items', 'mwt' ),
		'search_items'          => __( 'Search Item', 'mwt' ),
		'not_found'             => __( 'Not found', 'mwt' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mwt' ),
		'featured_image'        => __( 'Featured Image', 'mwt' ),
		'set_featured_image'    => __( 'Set featured image', 'mwt' ),
		'remove_featured_image' => __( 'Remove featured image', 'mwt' ),
		'use_featured_image'    => __( 'Use as featured image', 'mwt' ),
		'insert_into_item'      => __( 'Insert into item', 'mwt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mwt' ),
		'items_list'            => __( 'Items list', 'mwt' ),
		'items_list_navigation' => __( 'Items list navigation', 'mwt' ),
		'filter_items_list'     => __( 'Filter items list', 'mwt' ),
	);
	$args = array(
		'label'                 => __( 'Galery', 'mwt' ),
		'description'           => __( 'Galery information page.', 'mwt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-gallery',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'mwt-galery', $args );

}
add_action( 'init', 'mwt_galery_post_type', 0 );

// Register Custom Taxonomy
function mwt_galery_category_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Kategori Foto', 'Taxonomy General Name', 'mwt' ),
		'singular_name'              => _x( 'Kategori Foto', 'Taxonomy Singular Name', 'mwt' ),
		'menu_name'                  => __( 'Kategori Foto', 'mwt' ),
		'all_items'                  => __( 'All Categories', 'mwt' ),
		'parent_item'                => __( 'Parent Category', 'mwt' ),
		'parent_item_colon'          => __( 'Parent Category:', 'mwt' ),
		'new_item_name'              => __( 'New Category Name', 'mwt' ),
		'add_new_item'               => __( 'Add New Category', 'mwt' ),
		'edit_item'                  => __( 'Edit Category', 'mwt' ),
		'update_item'                => __( 'Update Category', 'mwt' ),
		'view_item'                  => __( 'View Item', 'mwt' ),
		'separate_items_with_commas' => __( 'Separate categories with commas', 'mwt' ),
		'add_or_remove_items'        => __( 'Add or remove categories', 'mwt' ),
		'choose_from_most_used'      => __( 'Choose from the most used categories', 'mwt' ),
		'popular_items'              => __( 'Popular Items', 'mwt' ),
		'search_items'               => __( 'Search categories', 'mwt' ),
		'not_found'                  => __( 'Not Found', 'mwt' ),
		'no_terms'                   => __( 'No items', 'mwt' ),
		'items_list'                 => __( 'Items list', 'mwt' ),
		'items_list_navigation'      => __( 'Items list navigation', 'mwt' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'galery_category', array('mwt-galery'), $args );

}
add_action( 'init', 'mwt_galery_category_taxonomy', 0 );