<?php


class Mwt_MetaBoxes {
 
    public function __construct() {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes' ) );
        add_action( 'save_post', array( $this, 'save_meta_box' ) );
    }

    public function add_meta_boxes() {
        add_meta_box(
            'kuota-metabox',
            "Kuota",
            array( $this, 'display_kuota_meta_box' ),
            'mwt-umroh',
            'side',
            'high'
        );
 
    }
 
    public function display_kuota_meta_box() {
      global $post;
      $sisa_kuota = get_post_meta( $post->ID, 'sisa_kuota', true );
      wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
      ?>
      <table class="form-table">
          <tr valign="top">
          <th scope="row">Sisa kuota tersedia</th>
          <td><input type="number" name="sisa_kuota" value="<?php echo $sisa_kuota; ?>" /></td>
          </tr>
      </table>
    <?php
    }
  
    public function save_meta_box( $post_id ) {
        // Bail if we're doing an auto save
        if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

        // if our nonce isn't there, or we can't verify it, bail
        if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'my_meta_box_nonce' ) ) return;

        // if our current user can't edit this post, bail
        if( !current_user_can( 'edit_post' ) ) return;

        // now we can actually save the data
        $allowed = array( 
            'a' => array( // on allow a tags
                'href' => array() // and those anchors can only have href attribute
            )
        );

        // Make sure your data is set before trying to save it
        if( isset( $_POST['sisa_kuota'] ) )
            update_post_meta( $post_id, 'sisa_kuota', wp_kses( $_POST['sisa_kuota'], $allowed ) );
      

    }
 
}