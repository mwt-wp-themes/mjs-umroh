<?php

// Register Custom Post Type
function participant_post_type() {

	$labels = array(
		'name'                  => _x( 'Participants', 'Post Type General Name', 'mjslp' ),
		'singular_name'         => _x( 'Participant', 'Post Type Singular Name', 'mjslp' ),
		'menu_name'             => __( 'Participants', 'mjslp' ),
		'name_admin_bar'        => __( 'Participant', 'mjslp' ),
		'archives'              => __( 'Item Archives', 'mjslp' ),
		'attributes'            => __( 'Item Attributes', 'mjslp' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mjslp' ),
		'all_items'             => __( 'All Items', 'mjslp' ),
		'add_new_item'          => __( 'Add New Item', 'mjslp' ),
		'add_new'               => __( 'Add New', 'mjslp' ),
		'new_item'              => __( 'New Item', 'mjslp' ),
		'edit_item'             => __( 'Edit Item', 'mjslp' ),
		'update_item'           => __( 'Update Item', 'mjslp' ),
		'view_item'             => __( 'View Item', 'mjslp' ),
		'view_items'            => __( 'View Items', 'mjslp' ),
		'search_items'          => __( 'Search Item', 'mjslp' ),
		'not_found'             => __( 'Not found', 'mjslp' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mjslp' ),
		'featured_image'        => __( 'Featured Image', 'mjslp' ),
		'set_featured_image'    => __( 'Set featured image', 'mjslp' ),
		'remove_featured_image' => __( 'Remove featured image', 'mjslp' ),
		'use_featured_image'    => __( 'Use as featured image', 'mjslp' ),
		'insert_into_item'      => __( 'Insert into item', 'mjslp' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mjslp' ),
		'items_list'            => __( 'Items list', 'mjslp' ),
		'items_list_navigation' => __( 'Items list navigation', 'mjslp' ),
		'filter_items_list'     => __( 'Filter items list', 'mjslp' ),
	);
	$args = array(
		'label'                 => __( 'Participant', 'mjslp' ),
		'description'           => __( 'Participant information page.', 'mjslp' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'revisions' ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'participants', $args );

}
add_action( 'init', 'participant_post_type', 0 );