<?php

function mwt_currency( $amount, $prefix = '' ) {  
  $result = '';
  $result .= ( !empty( $prefix ) ) ? $prefix : '';
  $result .= number_format( $amount, 0, ',', '.' );
  return $result;
}

function user_update_profile_action() {
  if( isset( $_POST['submit'] ) ) {
    $first_name = esc_attr($_POST['first_name']);
    $nomorhp = $_POST['nomor_hp'];
    $nomortelp = $_POST['nomor_telp'];

    $user_id = wp_update_user( array( 
      'ID'            => get_current_user_id(),
      'first_name'    => $first_name, 
      'display_name'  => $first_name,
    ) );

    if ( is_wp_error( $user_id ) ) {
      // There was an error, probably that user doesn't exist.
    } else {
      update_user_meta( $user_id, '_nomorhp', $nomorhp );
      update_user_meta( $user_id, '_nomortelp', $nomortelp );
      
      if( !empty( $_POST['nama_bank'] ) ) {
        update_user_meta( $user_id, '_nama_bank', $_POST['nama_bank'] );
      }
      if( !empty( $_POST['nomor_rekening'] ) ) {
        update_user_meta( $user_id, '_norek', $_POST['nomor_rekening'] );
      }
      
      echo '<div class="box-message">';
      echo 'Profil berhasil diperbarui';
      echo '</div>';
    }	 
  }
}

function mwt_pagination() {
  $args = array(
//     'base'               => '%_%',
//     'format'             => '?paged=%#%',
  // 	'total'              => 1,
  // 	'current'            => 0,
  // 	'show_all'           => false,
  // 	'end_size'           => 1,
  // 	'mid_size'           => 2,
  // 	'prev_next'          => true,
  // 	'prev_text'          => __('« Previous'),
//     'next_text'          => __('Next »'),
    'type'               => 'list',
//     'add_args'           => false,
//     'add_fragment'       => '',
//     'before_page_number' => '',
//     'after_page_number'  => ''
  );
  return paginate_links( $args );
  $links = paginate_links( $args );
  foreach( $links as $link ) {
    echo $link;
  }
}

function convertCurrency($amount,$from_currency,$to_currency){
  $apikey = '';

  $from_Currency = urlencode($from_currency);
  $to_Currency = urlencode($to_currency);
  $query =  $from_Currency."_".$to_Currency;

  
  $ch = curl_init('https://free.currencyconverterapi.com/api/v6/convert?q='.$query.'&compact=ultra&apiKey='.$apikey);   
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // get the (still encoded) JSON data:
  $json = curl_exec($ch);
  curl_close($ch);

  // Decode JSON response:
  $conversionResult = json_decode($json, true);
  $total = $conversionResult[$query];
  $total = $total * $amount;
  return number_format($total, 2, '.', '');
}

function mwt_mitra_registration_form_action() {
    if ( isset($_POST['submit-mitra'] ) ) {
        registration_validation(
          $_POST['username'],
          $_POST['password'],
          $_POST['email'],
          $_POST['full_name'],
          $_POST['alamat'],
          $_POST['nomor_hp'],
          $_POST['nomor_wa']
        );
         
        // sanitize user form input
        global $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $full_name  =   sanitize_text_field( $_POST['full_name'] );
        $alamat     =   esc_textarea( $_POST['alamat'] );
        $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
        $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
          $username,
          $password,
          $email,
          $full_name,
          $alamat,
          $nomor_hp,
          $nomor_wa
        );
    }
}

function registration_validation( $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa )  {
  global $reg_errors;
  $reg_errors = new WP_Error;

  if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
      $reg_errors->add('field', 'Required form field is missing');
  }

  if ( 4 > strlen( $username ) ) {
      $reg_errors->add( 'username_length', 'Username too short. At least 4 characters is required' );
  }

  if ( username_exists( $username ) )
      $reg_errors->add('user_name', 'Username sudah digunakan!');

  if ( ! validate_username( $username ) ) {
      $reg_errors->add( 'username_invalid', 'Sorry, the username you entered is not valid' );
  }

  if ( 5 > strlen( $password ) ) {
      $reg_errors->add( 'password', 'Password length must be greater than 5' );
  }

  if ( !is_email( $email ) ) {
      $reg_errors->add( 'email_invalid', 'Email tidak valid!' );
  }

  if ( email_exists( $email ) ) {
      $reg_errors->add( 'email', 'Alamat email sudah digunakan!' );
  }

//   if ( ! empty( $website ) ) {
//       if ( ! filter_var( $website, FILTER_VALIDATE_URL ) ) {
//           $reg_errors->add( 'website', 'Website is not a valid URL' );
//       }
//   }

  if ( is_wp_error( $reg_errors ) ) {

      foreach ( $reg_errors->get_error_messages() as $error ) {

          echo '<div class="alert alert-error">';
          echo '<strong>ERROR</strong>:';
          echo $error . '<br/>';
          echo '</div>';
        
          break;

      }

  }
}


function complete_registration( $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa, $staf ) {
    global $reg_errors, $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa, $staf;
    if ( 1 > count( $reg_errors->get_error_messages() ) ) {
        $userdata = array(
          'user_login'    =>   $username,
          'user_email'    =>   $email,
          'user_pass'     =>   $password,
          'first_name'    =>   $full_name,
          'last_name'     =>   '',
          'nickname'      =>   $username,
          'role'          =>   'mitra',
        //'description'   =>   $bio,
        );
        $user_id = wp_insert_user( $userdata );
        if( $user_id ) {
          update_user_meta( $user_id, '_alamat', $alamat );
          update_user_meta( $user_id, '_nomorhp', $nomor_hp );
          update_user_meta( $user_id, '_nomortelp', $nomor_wa );
          update_user_meta( $user_id, '_staf', $staf );
        }
        echo '<div class="alert alert-success">';
        echo '<strong></strong>';
        echo 'Mitra berhasil ditambahkan.';
        echo '</div>';
    }
}

// Hijack the option, the role will follow!
add_filter('pre_option_default_role', function($default_role){
    // You can also add conditional tags here and return whatever
    return ( !empty( $_GET['role'] ) ) ? $_GET['role'] : $default_role;
});

function mwt_singkat_harga( $harga ) {
  $text = '';
  $rounded = floor( intval( $harga ) );
  $harga = mwt_currency($rounded);
  $array = explode( '.', $harga );
  
  if( strlen( $array[0] ) > 1 && count( $array ) >= 3 && count( $array ) < 6) {
    $harga = $array[0];
    $decimal = substr($array[1],0,1);
    if( $decimal > 0 ) {
     $harga .= '.' . $decimal; 
    }
    if( count( $array ) == 5 ) {
      $text = 'T';
    } elseif( count( $array ) == 4 ) {
      $text = 'M';
    } elseif( count( $array ) == 3 ) {
      $text = 'Jt';
    } 
  }
  
  $harga = ( $harga != '' ) ? $harga . $text : '';
  return $harga;
}

function mwt_add_every_five_minutes( $schedules ) {
	$schedules['every_five_minutes'] = array(
		'interval' => 300,
		'display' => __('Every Five Minutes')
	);
	return $schedules;
}
add_filter( 'cron_schedules', 'mwt_add_every_five_minutes' ); 

register_activation_hook(__FILE__, 'mwt_activation');
function mwt_activation() {
    if (! wp_next_scheduled ( 'mwt_every_five_minutes_event' )) {
  wp_schedule_event( time(), 'every_five_minutes', 'mwt_every_five_minutes_event' );
    }
}

add_action('mwt_every_five_minutes_event', 'do_this_every_five_minutes');
function do_this_every_five_minutes() {
  // do something every hour
  $args = array(
    'post_type'              => array( 'participants' ),
    'post_status'            => array( 'publish' ),
    'nopaging'               => true,
    'posts_per_page'         => -1,
    'meta_name'              => '_status_verifikasi',
    'meta_value'             => 'verified',
    'meta_compare'           => '!='
  );
  $event_posts = get_posts( $args );
  foreach( $event_posts as $event_post ) {
    wp_delete_post( $event_post->ID, true );
  }
}

register_deactivation_hook(__FILE__, 'mwt_deactivation');
function mwt_deactivation() {
  wp_clear_scheduled_hook('mwt_every_five_minutes_event');
}
