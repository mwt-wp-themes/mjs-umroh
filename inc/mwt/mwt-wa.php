<?php

class Mwt_Wa {
  
  public function __construct() {
    $this->redux_option_init();
    add_action( 'wp_ajax_wa_callback', array( $this, 'wa_callback' ) );
    add_action( 'wp_ajax_nopriv_wa_callback', array( $this, 'wa_callback' ) );
  }
  
  public function redux_option_init() {
    if( class_exists('Redux') ) {
      Redux::setSection( 'mwt_option', array(
          'title'  => __( 'WA Setting', 'mwt' ),
          'id'     => 'mwt-wa-option',
          'icon'   => 'el el-envelope',
          'fields' => array(
              array(
                  'id'       => 'mwt-wa-url',
                  'type'     => 'text', 
                  'title'    => __('URL Lokal', 'mwt'),
              ),
              array(
                  'id'       => 'mwt-wa-pesan-primary',
                  'type'     => 'textarea', 
                  'title'    => __('Pesan Customer', 'mwt'),
                  'desc'     => __('-- SHORTCODES: <br> <code>[kode_verifikasi]</code>: Kode verifikasi. <br> <code>[tanggal]</code>: Tanggal/Jadwal umroh. <br> <code>[nama]</code>: Nama customer <br> <code>[paket_umroh]</code>: Nama Paket Umroh <br> <code>[provider]</code>: Provider Umroh', 'mwt'),
              ),
              array(
                  'id'       => 'mwt-wa-pesan-secondary-staf',
                  'type'     => 'textarea', 
                  'title'    => __('Pesan Staf', 'mwt'),
              ),
//               array(
//                   'id'       => 'mwt-wa-pesan-secondary-spv',
//                   'type'     => 'textarea', 
//                   'title'    => __('Pesan SPV', 'mwt'),
//               ),
              array(
                  'id'       => 'mwt-wa-pesan-secondary-manager',
                  'type'     => 'textarea', 
                  'title'    => __('Pesan Manager', 'mwt'),
              ),
              array(
                  'id'       => 'mwt-wa-pesan-secondary-mitra',
                  'type'     => 'textarea', 
                  'title'    => __('Pesan Mitra', 'mwt'),
              ),
          )
      ) );
    }
  }
  
  public function send_staf_notification( $data, $shortcode = [] ) {
    global $mwt_option;
    
    if( !empty( $data['nomor'] ) ) :

      // PRIMARY
      $primary = array(
        'nomor' => $data['nomor'],
        'pesan' => $this->get_message_tags( [ 'sc' => $shortcode ] )
      );

      // SECONDARY
      $roles = [ 
        'staf_cs', 
        //'staf_spv', 
        'staf_manager', 
        'mitra' ];
      $secondary = array();
      $user_query = new WP_User_Query( array( 'role__in' => $roles ) );
      foreach ( $user_query->get_results() as $staf ) {
        $role = implode(', ', $staf->roles);
        $nomer_wa = get_user_meta( $staf->ID, '_nomorhp', true );

        if( $role == 'staf_cs' && !empty( $shortcode['staf_id'] ) && intval( $shortcode['staf_id'] ) == $staf->ID ) {
          
          $pesannya = $mwt_option['mwt-wa-pesan-secondary-staf'];
          
        } elseif( $role == 'staf_spv' ) {
          
          $pesannya = $mwt_option['mwt-wa-pesan-secondary-spv'];
          
        } elseif( $role == 'staf_manager' ) {
          
          $pesannya = $mwt_option['mwt-wa-pesan-secondary-manager'];
          
        } elseif( $role == 'mitra' && !empty( $shortcode['mitra_id'] ) && intval( $shortcode['mitra_id'] ) == $staf->ID ) {
          
          $pesannya = $mwt_option['mwt-wa-pesan-secondary-mitra'];
          
        } else {
          $pesannya = '';
        }

        // nomer ama pesan gak boleh kosong
        if( !empty( $nomer_wa ) && !empty( $pesannya ) ) {
          $nomer_wa = ltrim( $nomer_wa, "0" );
          $nomer_wa = ltrim( $nomer_wa, "62" );
          $nomer_wa = ltrim( $nomer_wa, "+62" );
          $nomer_wa = trim( "0" . $nomer_wa );

          $pesan = $this->get_message_tags(array( 
            'sc' => $shortcode
          ), $pesannya);
          $secondary[] = array(
            'nomor' => $nomer_wa,
            'pesan' => $pesan
          );
        }
      }

      $result = $this->send_request( $primary, $secondary );
    
    endif;
  }
  
  public function send_request( $primary, $secondary = [] ) {
    global $mwt_option;
    if( !empty( $mwt_option['mwt-wa-url'] ) && !empty( $primary['nomor'] ) ) {

      $data = array(
          'primary'   => array(
            'nomor'  => $primary['nomor'],
            'pesan'  => ( !empty( $primary['pesan'] ) ) ? $primary['pesan'] : ''
          ),
          'secondary' => $secondary
      );

      $ch = curl_init();
      curl_setopt( $ch, CURLOPT_URL, $mwt_option['mwt-wa-url'] );
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
      curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
      curl_setopt( $ch, CURLOPT_POST, 1 );
      $headers = array();
      $headers[] = "Content-Type: application/x-www-form-urlencoded";
      curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
      $response = curl_exec($ch);

      if($response === FALSE){
          return; //die(curl_error($ch));
      }
      return $response;
    }
  }
  
  public function get_message_tags( $params = array(), $message = '' ) {
    global $mwt_option;
    $message = ( $message != '' ) ? $message : $mwt_option['mwt-wa-pesan-primary'];
    if( !empty( $params['sc'] ) ) {
      foreach( $params['sc'] as $tag => $value ) {
        $message = str_replace( "[".$tag."]", $value, $message );
      } 
    }
    return nl2br( strip_tags( $message ) );
  }
  
  public function wa_callback() {
    $result = array();
    echo json_encode( $result );
    wp_die(); 
  }
  
}

$mwt_wa = new Mwt_Wa();