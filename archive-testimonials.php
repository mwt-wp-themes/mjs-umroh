<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

get_header();
?>


<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi">
	<main id="main" class="site-main batasna_kabeh halaman halaman_testimonial">

			<div class="batasna_wrap">
				<div class="breadcumb">
					<div class="clr"></div>
					<!-- PERUBAHAN 10 April 2016 -->
					<div class="brdcmb_knc">
						<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
						<a href="<?php echo esc_url( home_url('/') ); ?>" itemprop="url">
						<strong><span itemprop="title">Beranda</span></strong>
						</a>
						</span>
						<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
						<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" title="" itemprop="url">/
						<?php the_archive_title( '<span itemprop="title">', '</span>' ); ?>
						</a>
						</span>
					</div>
					<div class="brdcmb_kth"></div>
					<div class="clr"></div>
				</div>
				<h1>Jamaah <?php echo get_bloginfo('name'); ?> Yang Sudah Menggunakan Layanan Kami</h1>
				<div class="separator separator_hideung"><span></span></div>
				<div class="halaman_konten halaman_sidebar">
					<div class="clr"></div>
					<div class="hs_kenca">
						<div class="testimonial">
							<ul id="lisTestimonial">
								<?php if ( have_posts() ) : ?>
								<?php
									/* Start the Loop */
									$count = 1;
									while ( have_posts() ) :
									  the_post();
									
									  /*
									   * Include the Post-Type-specific template for the content.
									   * If you want to override this in a child theme, then include a file
									   * called content-___.php (where ___ is the Post Type name) and that will be used instead.
									   */
									  ?>
								<li>
									<div class="testimonial_blok ">
										<div class="tb_ditel">
											<div class="clr"></div>
											<div class="tb_ditel_kenca">
												<div class="gbrna"><img src="<?php echo get_the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>"></div>
											</div>
											<div class="tb_ditel_katuhu">
												<h4><?php the_title(); ?></h4>
												<p><?php echo Mwt::get_field( 'job_title' ); ?></p>
												<span></span>
											</div>
											<div class="clr"></div>
										</div>
										<?php the_content(); ?>
									</div>
								</li>
								<?php $count++; endwhile;
									else :
									
									get_template_part( 'template-parts/content', 'none' );
									
									endif;
									?>
							</ul>
						</div>
						<!--PEGING-->
						<div class="halaman_peging">
							<?php the_posts_pagination(); ?>
              <script>
                $(".nav-links").find(".page-numbers").addClass('baten');
              </script>
              <br>
						</div>
						<!--PEGING-->
						<!--SHARE-->
						<div class="halaman_komentar">
							<h4>Sebarkan Ini</h4>
							<!--SHARE THIS-->
							<div id="social-share"></div>
							<!--SHARE THIS-->
              <script>
              $("#social-share").jsSocials({
                  showLabel: false,
                  showCount: true,
                  shareIn: "popup",
                  shares: ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "whatsapp"]
              });
              </script>
						</div>
						<!--SHARE-->
						<!--MASONRY-->
						<script type="text/javascript">
							$('#lisTestimonial').masonry({
							 "gutter": 15,
							  itemSelector: 'li'
							});
						</script>
						<!--!MASONRY-->
					</div>
					<?php get_sidebar(); ?>
					<div class="clr"></div>
				</div>
			</div>
    
	</main>
	<!-- #main -->
  <?php get_template_part( 'layouts/section', 'banner-handap' ); ?>
</div>
<!-- #primary -->


<?php
get_footer();
