<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package MJS Tour
 */

global $mwt;
$mwt->set_mitra();
get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi"><!--primary-->
		<main id="main" class="site-main batasna_kabeh halaman">

			<section class="error-404 not-found">
          <div class="batasna_wrap">

              <div class="breadcumb">
                  <div class="clr"></div>

                  <div class="brdcmb_knc"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title=""><strong>Beranda</strong></a> <a href="#" title="">/ Halaman Tidak Ditemukan</a></div>
                  <div class="brdcmb_kth">

                  </div>

                  <div class="clr"></div>
              </div>

              <div class="tengah"><i class="ic_baeud"></i></div>
              <br>
              <h1 class="teks_ageung">Halaman Tidak Ditemukan</h1>
              <div class="separator separator_hideung"><span></span></div>

              <!--TAMBAHAN 07/03/16-->
              <div class="halaman_konten ">
                  <div class="teks_statis tengah">
                      <p>Mohon maaf, Halaman yang Anda akses tidak ditemukan.
                      </p>
                      <p>&nbsp;</p>
                      <p>&nbsp;</p>
                      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="baten baten_dosis" title="Kembali ke Beranda"><span class="teks_ageung">Kembali ke Beranda</span></a>
                      <br>
                      <br>
                      <a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" class="baten baten_dosis" title="Temukan Jadwal Umroh Anda"><span class="teks_ageung">Temukan Jadwal Umroh Anda</span></a>
                  </div>
              </div>
              <!--TAMBAHAN 07/03/16-->

          </div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
