

<div class="hdu_ditail responsive-tabs">
	<h2>Overview</h2>
	<div class="teks_statis">
		<p><strong>DETAIL PAKET UMROH PLUS ISTANBUL 11 HARI</strong></p>
		<p>&#160;<strong>&#160;</strong></p>
		<p><strong>Harga&#160;</strong></p>
		<p><strong>Quard&#160; &#160; : 3.000 USD</strong></p>
		<p><strong>Triple&#160; &#160; &#160;: 3.050 USD</strong></p>
		<p><strong>Double&#160; : 3.100 USD</strong></p>
		<p>&#160;&#160;</p>
		<p><strong>Keberangkatan&#160; : </strong>26 Desember 2018</p>
		<p><strong>Flight&#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; &#160; :</strong> Saudi Airlines</p>
		<p><strong>Hotel Istanbul&#160; &#160; :</strong> Grand Yafuz / Setaraf</p>
		<p><strong>Hotel Madinah&#160; &#160;:</strong> Mubarok Silver / Setaraf</p>
		<p><strong>Hotel Makkah&#160; &#160; &#160;:</strong> Al Marsa / Setaraf</p>
		<p>&#160;&#160;</p>
		<p><strong>Harga sudah termasuk : </strong></p>
		<ol>
			<li>Akomodasi dan hotel sesuai program</li>
			<li>Pengurusan visa umroh dan Turki</li>
			<li>Tiket pesawat sesuai program ekonomi class</li>
			<li>Makan 3 x sehari</li>
			<li>Bimbingan manasik umroh</li>
			<li>Transportasi bus AC</li>
			<li>Bagasi sesuai ketentuan penerbangan</li>
			<li>Tuthawwif / Guide berpengalaman</li>
			<li>Air Zamzam 5 liter</li>
			<li>DVD / VCD Memory perjalanan</li>
			<li>P3K dan Obat obatan</li>
		</ol>
		<p>&#160;</p>
		<p><strong>Harga belum termasuk :</strong></p>
		<ol>
			<li>Handling, lounge, perlengkapan dan transport Cirebon Jakarta PP</li>
			<li>Laundry, fax, telp, kelebihan bagasi, dll yang bersifat pribadi</li>
			<li>Tour di luar program yang sudah di tetapkan</li>
			<li>Biaya yang timbul akibat keterlambatan, penundaan &amp; pembatalan oleh penerbangan</li>
			<li>Biaya pembuatan passport dan suntik vaksin meningitis</li>
		</ol>
		<p>&#160;</p>
		<p><strong>Pembatalan : </strong></p>
		<ol>
			<li>50 % Sejak pendaftaran sampai 20 hari sebelum keberangkatan</li>
			<li>70 % Sejak 15 sampai 10 hari sebelum keberangkatan</li>
			<li>100 % Sejak 10 hari sebelum keberangkatan</li>
		</ol>
	</div>
	<h2>Itenary</h2>
	<div class="teks_statis hdu_itenary">
		<p><strong>ITINERARY UMROH PLUS ISTANBUL 11 HARI</strong></p>
		<p>&#160;</p>
		<p><strong>Hari ke 1&#160;</strong></p>
		<ul>
			<li>Diharapkan jamaah sudah mandi sunah dan shalat safar 2 rakaat sebelum berangkat ke bandara.</li>
			<li>Jamaah berkumpul di louge umroh bandara soekarno hatta untuk istirahat, sholat, makan dan pemantapan materi umroh</li>
			<li>Menuju Terminal 3 Keberangkatan Internasional untuk proses chek in, imigrasi dll.</li>
			<li>16.00 Take off menuju Madinah dengan pesawat Saudiairlines SV 825</li>
			<li>22.10 Tiba di Airport Madinah, pengurusan bagasi dan imigrasi.</li>
			<li>Transfer to Hotel Madinah, chek in dan istirahat</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 2</strong></p>
		<ul>
			<li>Ziarah makam Rasulullah dan Raudhah</li>
			<li>Memperbanyak ibadah di Masjid Nabawi</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 3</strong></p>
		<ul>
			<li>Ziarah sekitar kota Madinah : Masjid Quba, Jabal Uhud dan Kebun Kurma</li>
			<li>Memperbanyak ibadah di Masjid Nabawi</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 4</strong></p>
		<ul>
			<li>Chek out hotel bada Dzuhur</li>
			<li>Melanjutkan perjalanan menuju Makkah</li>
			<li>Mengambil miqat di Bir Ali dan melaksanakan sholat sunah ihram 2 rakaat</li>
			<li>Tiba di Makkah langsung chek in hotel, setelah istirahat sejenak kemudian bersiap-siap melaksanakan rangkaian ibadah umroh (Tawaf, Sa&#8217;I dan Tahalul) di Masjidil Haram</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 5</strong></p>
		<ul>
			<li>Memperbanyak ibadah di Masjidil Haram</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 6</strong></p>
		<ul>
			<li>Ziarah sekitar kota Makkah : Jabal Rahmah, Jabal Tsur, Padang Arafah, Muzdhalifah, Mina, Jabal Nur (Gua Hira) dan Ji&#8217;ronah untuk mengambil miqat sebelum melaksanakan umroh yang ke 2</li>
			<li>Memper banyak ibadah di Masjidil Haram</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 7</strong></p>
		<ul>
			<li>Melaksanakan tawaf wada</li>
			<li>Chek out hotel, dilanjutkan perjalanan menuju Jeddah</li>
			<li>Take off menuju Istanbul dengan pesawat Saudiairlines SV257</li>
			<li>Tiba di Istanbul, pengurusan bagasi dan imigrasi</li>
			<li>Pejalanan di lanjutkan menuju Bursa</li>
			<li>Mengunjungi Grand Mosque salah satu masjid bersejarah di kota Bursa</li>
			<li>Chek in Hotel dan istirahat</li>
		</ul>
		<p><br /><strong>Hari ke 8</strong></p>
		<ul>
			<li>Chek out hotel</li>
			<li>City tour Bursa : mengunjungi Green Mosque, Green Mausoleum, Slik Bazar, Menaiki Gunung Uludag dengan Cabel Car, menikmati keindahan Gunung Uludag dengan salju tebal.</li>
			<li>Perjalanan dilanjutkan menuju Istanbul</li>
			<li>Tiba di istanbul chek in hotel dan istirahat</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 9</strong></p>
		<ul>
			<li>City tour Istanbul : mengunjungi Blue Mosque, Hippodrome, Ayasofya Museum, Topkapi Palace, Grand Bazaar</li>
			<li>Kembali ke hotel dan istirahat</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 10</strong></p>
		<ul>
			<li>Chek out hotel</li>
			<li>City tour Istanbul : Melintasi Sungai Bosphorus dengan kapal wisata sambil menikmati keindahan kota Istanbul</li>
			<li>Transfer to Airport Istanbul</li>
			<li>16.30 Take off menuju Riyadh dengan pesawat Saudiairlines SV264</li>
			<li>20.35 Tiba di Airport Riyadh transit sejenak</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Hari ke 11</strong></p>
		<ul>
			<li>01.15 Take off menuju Jakarta dengan pesawat Saudiairlines SV822</li>
			<li>14.00 Insya Allah tiba di Jakarta dengan selamat. Semoga Umroh Anda Makbul dan menjadi perjalanan yang mengesankan.</li>
		</ul>
	</div>
	<h2>Syarat & Ketentuan</h2>
	<div class="teks_statis">
		<p>Syarat untuk melakukan pendaftaran umroh dengan kami:</p>
		<p>&#160;</p>
		<ul>
			<li>Passport yang masih berlaku min 9 bulan dengan 3 suku kata , Contoh : Ismail shaleh tasja.</li>
			<li>kartu kuning (suntik vaksin meningitis)?.</li>
			<li>Akte lahir untuk anak (jika bersama anak).</li>
			<li>Surat nikah (jika bersama suami/istri).</li>
			<li>Foto berwarna , background putih ukuran 4x6 = 2 lbr.</li>
			<li>DP dalam rupiah senilah 1.200 USD.</li>
		</ul>
		<p>&#160;</p>
		<p>&#160;<strong>Harga sudah termasuk</strong></p>
		<ul>
			<li>Akomodasi dan hotel sesuai program</li>
			<li>Pengurusan Visa Umrah</li>
			<li>Tiket pesawat udara Jakarta - Madinah / Jeddah PP. (Kelas Ekonomi)</li>
			<li>Makan 3X sehari menu Indonesia</li>
			<li>Bimbingan Manasik Umrah</li>
			<li>Ziarah di Saudi dengan transportasi bus AC</li>
			<li>Bagasi sesuai ketentuan penerbangan</li>
			<li>Muthawwif berpengalaman</li>
			<li>Air zam-zam @ 5 liter</li>
			<li>DVD / VCD Memory Umroh</li>
			<li>P3K dan obat-obatan</li>
		</ul>
		<p><br /><strong>Harga Belum Termasuk</strong>&#160;</p>
		<ul>
			<li>Handling, Lounge dan Perlengkapan&#160;Rp 1.250.000,-</li>
			<li>Laundry, fax, telp, kelebihan bagasi, dll. yang bersifat pribadi</li>
			<li>Tour di luar program yang sudah ditetapkan</li>
			<li>Biaya yg timbul akibat keterlambatan, penundaan &amp; pembatalan oleh penerbangan</li>
			<li>Biaya pembuatan passport via travel agent</li>
			<li>Biaya Suntik Vaksin</li>
		</ul>
		<p><br /><strong>Pembatalan Umroh</strong></p>
		<ul>
			<li>70% sejak pendaftaran sampai dengan 14 hari sebelum keberangkatan dari biaya paket</li>
			<li>80% sejak 13 sampai 10 hari sebelum keberangkatan dari biaya paket</li>
			<li>90% sejak 9 hari sampai dengan tanggal keberangkatan dari biaya paket</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Buruan pesan umroh reguler sekarang juga seat terbatas !!!</strong></p>
		<p>Segera Hubungi Kami di 021&#160;22869393</p>
		<p>atau Whatsapp kami di 0857 24313 696 (Jakarta) dan 0857 2405 5627 (Cirebon) Anda juga bisa langsung chat dengan Customer Service kami (sabtu, minggu kantor tetap buka)</p>
	</div>
	<h2>Cara Pembayaran</h2>
	<div class="teks_statis">
		<p>Jika sudah menentukan tanggal keberangkatan sesuai dengan keinginan , anda bisa langsung melakukan pembayaran dengan cara :</p>
		<p>&#160;</p>
		<ul>
			<li><strong>Transfer</strong>&#160;<br />untuk melakukan transaksi melalu transfer kami mempunyai 2 nomor rekening a.n PT.Salam Sejahtera Wisata :<br />BNI Syariah : 013 665 9684<br />Bank Syariah Mandiri : 700 4014 299</li>
		</ul>
		<ul>
			<li><strong>Cash</strong><br />untuk melakukan transaksi cash bisa langsung datang ke kantor PT.Salam Sejahtera Wisata yang berada di Cirebon atau Jakarta ..</li>
		</ul>
		<p>&#160;</p>
		<p><strong>Buruan pesan umroh reguler sekarang juga seat terbatas !!!</strong></p>
		<p>Segera Hubungi Kami di 021&#160;22869393</p>
		<p>atau Whatsapp kami di 0857 24313 696 (Jakarta) dan 0857 2405 5627 (Cirebon) Anda juga bisa langsung chat dengan Customer Service kami (sabtu, minggu kantor tetap buka)</p>
	</div>
</div>

