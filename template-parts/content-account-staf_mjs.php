<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option, $user_info, $section, $role;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if( $section == "assignment" ) : ?>

    <table id="datatable1" class="pure-table pure-table-bordered" width="100%">
        <thead>
            <tr>
                <th width="25">#</th>
                <th>Tanggal</th>
                <th>Customer</th>
                <th>Contact</th>
                <th>Paket Umroh</th>
                <th>Provider</th>
                <th>Mitra</th>
                <th>Follow Up</th>
                <th>Status</th>
            </tr>
        </thead>

        <tbody>
          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'participants' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
            'meta_query' => array(                  //(array) - Custom field parameters (available with Version 3.1).
               'relation' => 'AND',                 //(string) - Possible values are 'AND', 'OR'. The logical relationship between each inner meta_query array when there is more than one. Do not use with a single inner meta_query array.
//                array(
//                  'key' => '_status_verifikasi',                  //(string) - Custom field key.
//                  'value' => 'verified'                  //(string/array) - Custom field value (Note: Array support is limited to a compare value of 'IN', 'NOT IN', 'BETWEEN', or 'NOT BETWEEN') Using WP < 3.9? Check out this page for details: http://codex.wordpress.org/Class_Reference/WP_Query#Custom_Field_Parameters
//                  'type' => 'CHAR',                  //(string) - Custom field type. Possible values are 'NUMERIC', 'BINARY', 'CHAR', 'DATE', 'DATETIME', 'DECIMAL', 'SIGNED', 'TIME', 'UNSIGNED'. Default value is 'CHAR'. The 'type' DATE works with the 'compare' value BETWEEN only if the date is stored at the format YYYYMMDD and tested with this format.
//                                                     //NOTE: The 'type' DATE works with the 'compare' value BETWEEN only if the date is stored at the format YYYYMMDD and tested with this format.
//                  'compare' => '='
//                ),
               array(
                 'key' => 'staf',
                 'value' => $user_info->ID,
                 'type' => 'NUMERIC',
                 'compare' => '='
               )
            ), 
          );

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 1;
            while ( $query->have_posts() ) {
              $query->the_post(); 
              $paket = Mwt::get_field( 'paket_umroh' );  
              $provider = Mwt::get_field( 'provider', $paket->ID ); ?>
              <tr>
                  <td><?php echo $count; ?></td>
                  <td><?php echo get_the_date('d/m/Y'); ?></td>
                  <td><?php the_title(); ?></td>
                  <td>
                    Email: <strong><?php echo Mwt::get_field('email'); ?></strong><br>
                    HP: <strong><?php echo Mwt::get_field('nomor_hp'); ?></strong>
                  </td>
                  <td><?php echo $paket->post_title; ?></td>
                  <td>
                    <?php if( !empty( $provider ) ): ?>
                    <?php echo $provider->post_title; ?>
                    <?php endif; ?>
                  </td>
                  <td><?php echo ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->display_name : ''; ?></td>
                  <td>
                    <a href="#" class="followup-result-input" data-type="textarea" data-pk="<?php the_ID(); ?>"><?php echo Mwt::get_field( 'follow_up_result' ); ?></a>
                  </td>
<!--                   <td>
                    <?php 
                    $spv = Mwt::get_field('spv');
                    echo '<a href="#" class="supervisor-field" data-type="select" data-pk="'.get_the_ID().'" data-url="'.admin_url('admin-ajax.php').'?action=mwt_assign_spv" data-title="Pilih Supervisor">';
                    echo ( !empty( $spv ) ) ? $spv->display_name : '';   
                    echo '</a>';
                    ?>
                  </td> -->
                
                
                  <td>
                    <a href="#" class="cust-status" data-type="select" data-pk="<?php the_ID(); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>?action=mwt_update_cust_status" data-title="Select status"><?php echo ( Mwt::get_field( 'status' ) == "Confirmed" ) ? 'Confirmed' : 'Pending'; ?></a>
                    <br><span class="spv-field-<?php the_ID(); ?>" style="<?php echo ( Mwt::get_field( 'status' ) == "Confirmed" ) ? '' : 'display:none'; ?>">
                    SPV: 
                    <?php 
                    $spv = Mwt::get_field('spv');
                    echo '<a href="#" class="supervisor-field" data-type="select" data-pk="'.get_the_ID().'" data-url="'.admin_url('admin-ajax.php').'?action=mwt_assign_spv" data-title="Pilih Supervisor">';
                    echo ( !empty( $spv ) ) ? $spv->display_name : '';   
                    echo '</a>';
                    ?></span>
                  </td>
              </tr>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>
        </tbody>
    </table>
    <?php if( $role == 'staf_mjs' ) { ?> 
    <script>
    jQuery(function(){
        jQuery('.supervisor-field').editable({   
            source: [
                  <?php 
                  $spv_query = new WP_User_Query( array( 'role__in' => [ 'staf_spv' ] ) );
                  $counts = count( $spv_query->get_results() );
                  $count = 1;
                  foreach ( $spv_query->get_results() as $spv ) {
                    echo '{value: '.$spv->ID.', text: "'.$spv->display_name.'"}';
                    echo ( $count < $counts ) ? ',' : '';
                    $count++;
                  }
                  ?>
               ]
        });
    });
    </script>
    <?php } ?>
    <script>
    jQuery(function($){
        $('.followup-result-input').editable({
            url: '<?php echo admin_url('admin-ajax.php'); ?>?action=mwt_update_followup_result',
            title: 'Enter comments',
            rows: 10
        });
    });
    jQuery(function($){
        $('.cust-status').editable({
            source: [
                  {value: 'Pending', text: 'Pending'},
                  {value: 'Confirmed', text: 'Confirmed'}
               ],
            success: function(response, newValue) {
                if(!response.success) window.location.reload();
            }
        });
    });
    </script>
  
    <?php elseif( $section == "mitra" ) : ?>
  
      <?php if( !empty( $_GET['edit'] ) ) : ?>
  
        <?php
        $mitra = get_user_by( 'ID', $_GET['edit'] );
        if( !$mitra ) {
          echo '<div class="alert alert-error">';
          echo 'Mitra tidak ditemukan!';
          echo '</div>';
        } else {
          if( isset( $_POST['update-mitra'] ) ) {
            $user_id = intval( $_GET['edit'] );
            $full_name  =   sanitize_text_field( $_POST['full_name'] );
            $alamat     =   esc_textarea( $_POST['alamat'] );
            $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
            $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );

            $user_id = wp_update_user( array( 
              'ID'          => $user_id, 
              'first_name'  => $full_name,
              'last_name'   => '',
            ) );

            if ( is_wp_error( $user_id ) ) {
              // There was an error, probably that user doesn't exist.
            } else {
              // Success!
              update_user_meta( $user_id, '_alamat', $alamat );
              update_user_meta( $user_id, '_nomorhp', $nomor_hp );
              update_user_meta( $user_id, '_nomortelp', $nomor_wa );
              update_user_meta( $user_id, '_staf', get_current_user_id() );
              if( !empty( $_POST['nama_bank'] ) ) {
                update_user_meta( $user_id, '_nama_bank', $_POST['nama_bank'] );
              }
              if( !empty( $_POST['nomor_rekening'] ) ) {
                update_user_meta( $user_id, '_norek', $_POST['nomor_rekening'] );
              }
              echo '<div class="alert alert-error">';
              echo 'Data mitra berhasil diupdate.';
              echo '</div>';
            }	
          }
        ?>
  
        <form method="post" action="#" class="pure-form pure-form-aligned">
            <fieldset>
                <div class="pure-control-group">
                    <label for="full_name">Nama Lengkap</label>
                    <input id="full_name" name="full_name" type="text" value="<?php echo $mitra->first_name . ' ' . $user->last_name; ?>" required>
                </div>

                <div class="pure-control-group">
                    <label for="username">Username</label>
                    <input id="username" type="text" name="username" value="<?php echo ( isset( $mitra->user_login ) ? $mitra->user_login : null ); ?>" disabled>
                    <span class="pure-form-message-inline">Username tidak dapat diubah.</span>
                </div>

                <div class="pure-control-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" name="email" value="<?php echo $mitra->user_email; ?>" disabled>
                </div>

                <div class="pure-control-group">
                    <label for="alamat">Alamat</label>
                    <textarea id="alamat" name="alamat"><?php echo get_user_meta( $mitra->ID, '_alamat', true ); ?></textarea>
                </div>

                <div class="pure-control-group">
                    <label for="nomor_hp">Nomor HP</label>
                    <input id="nomor_hp" name="nomor_hp" type="text" value="<?php echo get_user_meta( $mitra->ID, '_nomorhp', true ); ?>">
                </div>

                <div class="pure-control-group">
                    <label for="nomor_wa">Nomor WA</label>
                    <input id="nomor_wa" name="nomor_wa" type="text" value="<?php echo get_user_meta( $mitra->ID, '_nomortelp', true ); ?>">
                </div>

                <div class="pure-controls">
                <p>
                  Rekening Bank (Untuk penerimaan komisi)
                  </p><br>
                </div>
                <div class="pure-control-group">
                    <label for="nama_bank">Nama Bank</label>
                    <input id="nama_bank" name="nama_bank" type="text" value="<?php echo get_user_meta( $mitra->ID, '_nama_bank', true ); ?>">
                </div>
                <div class="pure-control-group">
                    <label for="nomor_rekening">Nomor Rekening</label>
                    <input id="nomor_rekening" name="nomor_rekening" type="text" value="<?php echo get_user_meta( $mitra->ID, '_norek', true ); ?>">
                </div>
              
                <div class="pure-controls">
                    <button type="submit" name="update-mitra" class="pure-button pure-button-primary">Submit</button>
                </div>
            </fieldset>
        </form>
  
        <?php } ?>
  
      <?php else: ?>
      
      <table id="datatable1" width="100%">
          <thead>
              <tr>
                  <th width="25">#</th>
                  <th>Tanggal Daftar</th>
                  <th>Username</th>
                  <th>Nama Lengkap</th>
                  <th>Email</th>
                  <th>Nomor HP</th>
                  <th>Nomor WA</th>
                  <th>Rekening Bank</th>
                  <th></th>
              </tr>
          </thead>

          <tbody>
            <?php
            $args = array(
              'role'           => 'Mitra',
              'meta_key'      => '_staf',
              'meta_value'    => get_current_user_id(),
              'meta_value_num'=> get_current_user_id(),
              'meta_compare'  => '='
            );

            // The User Query
            $user_query = new WP_User_Query( $args );

            // The User Loop
            if ( ! empty( $user_query->results ) ) {
              $count = 1;
              foreach ( $user_query->results as $user ) { ?>

                <tr>
                    <td><?php echo $count; ?></td>
                    <td><?php echo get_the_date('d/m/Y'); ?></td>
                    <td><?php echo $user->user_login; ?></td>
                    <td><?php echo $user->first_name . ' ' . $user->last_name; ?></td>
                    <td><?php echo $user->user_email; ?></td>
                    <td><?php echo get_user_meta( $user->ID, '_nomortelp', true ); ?></td>
                    <td><?php echo get_user_meta( $user->ID, '_nomorhp', true ); ?></td>
                    <td>
                      <?php echo get_user_meta( $user->ID, '_nama_bank', true ); ?> - <?php echo get_user_meta( $user->ID, '_norek', true ); ?>
                    </td>
                    <td><a class="pure-button pure-button-primary" href="<?php echo add_query_arg( array( 'section' => 'mitra', 'edit' => $user->ID ), get_permalink() ); ?>">Edit</a></td>
                </tr>

              <?php $count++; }
            } ?>
          </tbody>
      </table>
  
      <?php endif; ?>
  
    <?php elseif( $section == "add-mitra" ) : ?>

    <?php 
    if ( isset( $_POST['submit-mitra'] ) ) {
      
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $full_name  =   sanitize_text_field( $_POST['full_name'] );
        $alamat     =   esc_textarea( $_POST['alamat'] );
        $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
        $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );
      
        registration_validation(
          $_POST['username'],
          $_POST['password'],
          $_POST['email'],
          $_POST['full_name'],
          $_POST['alamat'],
          $_POST['nomor_hp'],
          $_POST['nomor_wa']
        );
         
        // sanitize user form input
        global $username, $password, $email, $full_name, $alamat, $nomor_hp, $nomor_wa, $staf;
        $username   =   sanitize_user( $_POST['username'] );
        $password   =   esc_attr( $_POST['password'] );
        $email      =   sanitize_email( $_POST['email'] );
        $full_name  =   sanitize_text_field( $_POST['full_name'] );
        $alamat     =   esc_textarea( $_POST['alamat'] );
        $nomor_hp   =   sanitize_text_field( $_POST['nomor_hp'] );
        $nomor_wa   =   sanitize_text_field( $_POST['nomor_wa'] );
        $staf       =   get_current_user_id();
 
        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
          $username,
          $password,
          $email,
          $full_name,
          $alamat,
          $nomor_hp,
          $nomor_wa,
          $staf
        );
    }
    ?>
  
    <form method="post" action="#" class="pure-form pure-form-aligned">
        <fieldset>
            <div class="pure-control-group">
                <label for="full_name">Nama Lengkap</label>
                <input id="full_name" name="full_name" type="text" value="<?php echo ( isset( $_REQUEST['full_name'] ) ? $_REQUEST['full_name'] : null ); ?>" required>
            </div>
          
            <div class="pure-control-group">
                <label for="username">Username</label>
                <input id="username" type="text" name="username" value="<?php echo ( isset( $_REQUEST['username'] ) ? $_REQUEST['username'] : null ); ?>" required>
                <span class="pure-form-message-inline">Username akan menjadi link mitra.</span>
            </div>

            <div class="pure-control-group">
                <label for="password">Password</label>
                <input id="password" type="password" name="password" value="<?php echo ( isset( $_REQUEST['password'] ) ? $_REQUEST['password'] : null ); ?>" required>
            </div>

            <div class="pure-control-group">
                <label for="email">Email</label>
                <input id="email" type="email" name="email" value="<?php echo ( isset( $_REQUEST['email'] ) ? $_REQUEST['email'] : null ); ?>" required>
            </div>

            <div class="pure-control-group">
                <label for="alamat">Alamat</label>
                <textarea id="alamat" name="alamat"><?php echo ( isset( $_REQUEST['alamat'] ) ? $_REQUEST['alamat'] : null ); ?></textarea>
            </div>
          
            <div class="pure-control-group">
                <label for="nomor_hp">Nomor HP</label>
                <input id="nomor_hp" name="nomor_hp" type="text" value="<?php echo ( isset( $_REQUEST['nomor_hp'] ) ? $_REQUEST['nomor_hp'] : null ); ?>">
            </div>
          
            <div class="pure-control-group">
                <label for="nomor_wa">Nomor WA</label>
                <input id="nomor_wa" name="nomor_wa" type="text" value="<?php echo ( isset( $_REQUEST['nomor_wa'] ) ? $_REQUEST['nomor_wa'] : null ); ?>">
            </div>

                              <div class="pure-controls">
                              <p>
                                Rekening Bank (Untuk penerimaan komisi)
                                </p><br>
                              </div>
                              <div class="pure-control-group">
                                  <label for="nama_bank">Nama Bank</label>
                                  <input id="nama_bank" name="nama_bank" type="text" value="<?php echo ( isset( $_REQUEST['nama_bank'] ) ? $_REQUEST['nama_bank'] : null ); ?>">
                              </div>
                              <div class="pure-control-group">
                                  <label for="nomor_rekening">Nomor Rekening</label>
                                  <input id="nomor_rekening" name="nomor_rekening" type="text" value="<?php echo ( isset( $_REQUEST['nomor_rekening'] ) ? $_REQUEST['nomor_rekening'] : null ); ?>">
                              </div>
          
            <div class="pure-controls">
                <label for="cb" class="pure-checkbox">
                    <input id="cb" type="checkbox"> Kirim email pemberitahuan ke mitra.
                </label>

                <button type="submit" name="submit-mitra" class="pure-button pure-button-primary">Submit</button>
            </div>
        </fieldset>
    </form>
  
    <?php else: ?>

    <?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
