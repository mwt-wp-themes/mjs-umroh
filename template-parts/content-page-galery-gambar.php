<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt_option;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="hla_tab">
      <a href="<?php echo get_permalink( $mwt_option['galery-page-id'] ); ?>" title="" class="<?php echo ( empty( $_GET['category'] ) ) ? 'hla_tab_aktip' : ''; ?>">Semua</a>
      <?php
      $terms = get_terms( 'galery_category', array(
          'hide_empty' => true,
      ) );
      foreach( $terms as $term ) {
        echo '<a href="' . esc_url( add_query_arg( array(
'category' => $term->term_id ), get_permalink( $mwt_option['galery-page-id'] ) ) ) . '" ';
        echo ( !empty( $_GET['category'] ) && $_GET['category'] == $term->term_id ) ? 'class="hla_tab_aktip"' : '';
        echo 'title="">' . $term->name . '</a>';
      }
      ?>
    </div>

    <div class="hla_lisna hla_lisna2 magnificPopup" id="hla_lisna">
      <div class="clr"></div>

          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'mwt-galery' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
          );
          $tax_query = array();
          if( !empty( $_GET['category'] ) ) {
            $tax_query[] = array(
              'taxonomy'  => 'galery_category',
              'field'     => 'id',
              'terms'     => array( intval( $_GET['category'] ) ),
              'include_children' => true,
              'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
            );
          }
          if( count( $tax_query ) > 0 ) {
            $args['tax_query'] = array(
              'relation' => 'AND', //(string) - 'AND' or 'OR'
              $tax_query
            );
          }

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 0;
            while ( $query->have_posts() ) {
              $query->the_post(); ?>
                <?php
                $thumbnail_url = ( has_post_thumbnail() ) ? get_the_post_thumbnail_url() : get_template_directory_uri() . '/assets/images/placeholder-image.jpg';
                ?>
                <a href="<?php echo $thumbnail_url; ?>" title="" class="hla_lisna_blok">
                  <div class="hla_lb_gbr">
                  <img src="<?php echo $thumbnail_url; ?>" title="<?php the_title(); ?>" alt="<?php echo strip_tags( get_the_content() ); ?>" style="height:200px"> 
                  </div>
                  <?php $terms = get_the_terms( $post->ID, 'galery_category' ); ?>
                  <?php if( !empty( $terms[0] ) ) : ?>
                  <span><?php echo $terms[0]->name; ?></span>
                  <?php endif; ?>
                  <h4><?php the_title(); ?></h4>
                </a>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>
      <div class="clr"></div>
    </div>

    <!--PEGING-->
    <div class="halaman_peging ">
      <?php the_posts_pagination(); ?>
      <script>
        $(".nav-links .page-numbers").addClass('baten');
      </script>
      <br>
    </div>
    <!--PEGING-->
		
			<!--MASONRY-->
			<script type="text/javascript">
				$('#hla_lisna').masonry({
				 "gutter": 15,
				  itemSelector: 'a'
				});
			</script>
			<!--!MASONRY-->

			
			<!--MAGNIFIC POPUP-->
			<script type="text/javascript">
		      $(document).ready(function() {
		        $('.magnificPopup').magnificPopup({
		          delegate: 'a',
		          type: 'image',
		          closeOnContentClick: false,
		          closeBtnInside: false,
		          mainClass: 'mfp-with-zoom mfp-img-mobile',
		          image: {
		            verticalFit: true
		          },
		          gallery: {
		            enabled: true
		          },
		          zoom: {
		            enabled: true,
		            duration: 300, // don't foget to change the duration also in CSS
		            opener: function(element) {
		              return element.find('img');
		            }
		          }
		          
		        });
		      });
		    </script>
			<!--MAGNIFIC POPUP-->
</article><!-- #post-<?php the_ID(); ?> -->
