<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option, $user_info, $section;
$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : 'profile';
/*
Mitra meta:
_nomorhp
_nomortelp aka WA
_alamat
*/
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if( $section == "sales" ) : ?>

    <table id="datatable1" class="pure-table pure-table-bordered" width="100%">
        <thead>
            <tr>
                <th width="25">#</th>
                <th>Nama Jamaah</th>
                <th>Paket Umroh</th>
                <th>Tanggal Pesan</th>
                <th>Staf</th>
                <th>Follow Up</th>
                <th>Komisi</th>
                <th>Status Komisi</th>
            </tr>
        </thead>

        <tbody>
          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'participants' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
            'meta_query'             => array(
              'relation'  => 'AND',
              array(
                'key'     => 'mitra',
                'value'   => $user_info->ID,
                'type'    => 'NUMERIC',
                'compare' => '='
              ),
//               array(
//                 'key'     => 'status',
//                 'value'   => 'Confirmed',
//                 'type'    => 'CHAR',
//                 'compare' => '='
//               ),
            )
          );

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 1;
            while ( $query->have_posts() ) {
              $query->the_post(); 
              $paket = Mwt::get_field( 'paket_umroh' );  ?>
              <tr>
                  <td><?php echo $count; ?></td>
                  <td><?php the_title(); ?></td>
                  <td><?php echo $paket->post_title; ?></td>
                  <td><?php echo get_the_date('d/m/Y'); ?></td>
                  <td>
                    <?php 
                    $staf = get_user_by( 'id', intval( get_user_meta( get_current_user_id(), '_staf', true ) ) );
                    if( $staf ) echo $staf->display_name;
                    ?>
                  </td>
                  <td>
                    <?php echo Mwt::get_field( 'follow_up_result' ); ?>
                    <br>
                    Status: <?php echo Mwt::get_field( 'status' ); ?>
                  </td>
                  <td><?php echo ( !empty( Mwt::get_field( 'komisi_mitra' ) ) ) ? mwt_currency( Mwt::get_field( 'komisi_mitra' ), 'Rp' ) : 0; ?></td>
                  <td><?php echo Mwt::get_field( 'status_komisi' ); ?></td>
              </tr>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>
        </tbody>
    </table>

    <?php else: ?>

    <?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
