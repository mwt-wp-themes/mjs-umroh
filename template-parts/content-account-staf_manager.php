<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option, $user_info, $section;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
        <?php if( $section == "assignment" ) : ?>
        
        <table id="datatable1" class="pure-table pure-table-bordered" width="100%">
            <thead>
                <tr>
                    <th width="25">#</th>
                    <th>Tanggal</th>
                    <th>Customer</th>
                    <th>Paket Umroh</th>
                    <th>Provider</th>
                    <th>Mitra</th>
                    <th>Staf</th>
                    <th>Staf Follow-up</th>
                    <th>SPV Follow-up</th>
                    <th>Komisi Mitra</th>
                    <th>Rekening Mitra / Status</th>
                </tr>
            </thead>

            <tbody>
              <?php
              // WP_Query arguments
              $args = array(
                'post_type'              => array( 'participants' ),
                'post_status'            => array( 'publish' ),
                'posts_per_page'         => -1,
//                 'meta_name'              => '_status_verifikasi',
//                 'meta_value'             => 'verified',
//                 'meta_compare'           => '='
              );

              // The Query
              $query = new WP_Query( $args );

              // The Loop
              if ( $query->have_posts() ) {
                $count = 1;
                while ( $query->have_posts() ) {
                  $query->the_post(); 
                  $paket = Mwt::get_field( 'paket_umroh' );
                  $provider = Mwt::get_field( 'provider', $paket->ID ); ?>
                  <tr>
                      <td><?php echo $count; ?></td>
                      <td><?php echo get_the_date('d/m/Y'); ?></td>
                      <td><?php the_title(); ?></td>
                      <td><?php echo $paket->post_title; ?></td>
                      <td>
                        <?php if( !empty( $provider ) ): ?>
                        <?php echo $provider->post_title; ?>
                        <?php endif; ?>
                      </td>
                      <td><?php echo ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->display_name : ''; ?></td>
                      <td>
                        <?php 
//                         $staf = get_user_by( 'id', intval( get_user_meta( ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->ID : '', '_staf', true ) ) );
//                         if( $staf ) echo $staf->display_name;
                            echo ( !empty( Mwt::get_field('staf') ) ) ? Mwt::get_field('staf')->display_name : '';
                        ?>
                      </td>
<!--                       <td><?php echo Mwt::get_field('status'); ?></td> -->
                      <td><?php echo Mwt::get_field( 'follow_up_result' ); ?></td>
                      <td><?php echo Mwt::get_field( 'spv_notes' ); ?></td>
                      <td>
                        Rp <a href="#" class="komisi_mitra-input" data-type="number" data-pk="<?php the_ID(); ?>"><?php echo ( !empty( Mwt::get_field( 'komisi_mitra' ) ) ) ? mwt_currency( Mwt::get_field( 'komisi_mitra' ) ) : 0; ?></a>
                      </td>
                      <td>
                        <?php echo get_post_meta( ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->ID : 0, '_nama_bank', true ); ?> - <?php echo get_post_meta( ( !empty( Mwt::get_field('mitra') ) ) ? Mwt::get_field('mitra')->ID : 0, '_norek', true ); ?><br>
                        Status: <a href="#" class="komisi-status" data-type="select" data-pk="<?php the_ID(); ?>" data-url="<?php echo admin_url('admin-ajax.php'); ?>?action=mwt_update_komisi_status" data-title="Select status"><?php echo Mwt::get_field( 'status_komisi' ); ?></a>
                      </td>
                  </tr>
                  <?php
                  $count++;
                }
              }
              // Restore original Post Data
              wp_reset_postdata();
              ?>
            </tbody>
        </table>
  
        <script>
        jQuery(function($){
            $('.komisi_mitra-input').editable({
                url: '<?php echo admin_url('admin-ajax.php'); ?>?action=mwt_update_komisi_mitra',
                title: 'Input komisi'
            });
        });
        </script>

        <script>
        jQuery(function($){
            $('.komisi-status').editable({
                source: [
                      {value: 'Pending', text: 'Pending'},
                      {value: 'Sudah dibayar', text: 'Sudah dibayar'}
                   ]
            });
        });
        </script>
        
        <?php else: ?>
        
        <?php endif; ?>

</article><!-- #post-<?php the_ID(); ?> -->
