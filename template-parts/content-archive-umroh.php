<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

$classes = ( Mwt::get_field('promo') ) ? 'hjupl_tanda_promo' : '';
?>
<?php if( !empty( Mwt::get_field( 'tanggal_berangkat' ) ) ) : 
$date = new DateTime( Mwt::get_field( 'tanggal_berangkat' ) );
$now = new DateTime("now");
$interval_date = new DateTime( Mwt::get_field( 'tanggal_berangkat' ) );
$sub = $interval_date->sub(new DateInterval('P14D')); // interval 2 minggu sblm tgl berangkat sudah sold out
if( $now <= $sub ) : 
?>
<tr  id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<td class="hjupl_berangkat hjupl_nonmobel ">
		<div class="baten ">
			<div class="hjupl_berangkat_kenca ">
				<span><?php echo $date->format("j"); ?></span>
			</div>
			<!--
				-->
			<div class="hjupl_berangkat_katuhu ">
				<span><?php echo $date->format("F"); ?></span>
				<span><?php echo $date->format("Y"); ?></span>
			</div>
		</div>
	</td>
	<td class="hjupl_paket hjupl_nonmobel ">
		<div class="hjupl_td "><?php the_title(); ?></div>
		<!-- nama paket atau nama umroh? -->
	</td>
	<td class="hjupl_durasi hjupl_nonmobel ">
		<div class="hjupl_td "><?php echo Mwt::get_field( 'durasi' ); ?> Hari</div>
	</td>
	<td class="hjupl_durasi hjupl_nonmobel ">
		<div class="hjupl_td "><?php echo ( !empty( Mwt::get_field( 'shalat_jumat' ) ) ) ? Mwt::get_field( 'shalat_jumat' ) . 'x' : ''; ?></div>
	</td>
	<td class="hjupl_durasi hjupl_nonmobel ">
		<div class="hjupl_td "><?php echo ( !empty( Mwt::get_field( 'provider' ) ) ) ? Mwt::get_field( 'provider' )->post_title : ''; ?></div>
	</td>
	<td class="hjupl_pesawat hjupl_nonmobel ">
		<?php $maskapai = Mwt::get_field('maskapai'); 
			if( !empty( $maskapai ) ) {
			  foreach( $maskapai as $mskp ) {
			    echo '<div class="hjupl_pesawat_ikon hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="'.$mskp->post_title.'"><img src="'.get_the_post_thumbnail_url( $mskp->ID ).'" alt="'.$mskp->post_title.' Logo Title "></div>';
			  }
			}
			?>
	</td>
	<td class="hjupl_hotel hjupl_nonmobel ">
		<?php $penginapan = Mwt::get_field('penginapan');
			if( !empty( $penginapan ) ) {
			  foreach( $penginapan as $hotel ) {
			    echo '<i class="ic_bentang ic_bentang'.$hotel['bintang'].' hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="'.$hotel['nama_hotel'].'"></i>';    
			  }
			}
			?>
	</td>
	<td class="hjupl_harga hjupl_nonmobel ">
    <?php $harga = Mwt::get_field( 'harga' ); ?>
		<div class="hjupl_harga_normal hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="+- <?php echo ( !empty( $harga['quard'] ) ) ? '$ ' . mwt_currency( convertCurrency( $harga['quard'], 'IDR', 'USD' ) ) : ''; ?>"> <?php echo ( !empty( $harga['quard'] ) ) ? 'Rp' . mwt_singkat_harga( $harga['quard'] ) : ''; ?></div>
		<!--<div class="hjupl_harga_normal hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="USD 1800 ">Rp 27.5 JT</div>
			<div class="hjupl_harga_diskon ">Rp 30 JT</div>-->
	</td>
	<td class="hjupl_promo hjupl_nonmobel">
    <?php if( Mwt::get_field('promo') ) : ?>
		<div class="ic_promo"></div>
    <?php endif; ?>
    <?php 
    if( intval( get_post_meta( get_the_ID(), 'sisa_kuota', true ) ) < 1 || $now > $sub ) : 
    ?>
		<div class="ic_soldout"></div>
    <?php endif; ?>
	</td>
	<td class="hjupl_pesan hjupl_nonmobel ">
		<a href="<?php the_permalink(); ?>#pesan_paket_2 " class="baten baten_dosis "><span>Pesan Sekarang</span></a>
		<a href="<?php the_permalink(); ?> " class="baten baten_oren baten_dosis "><span>Detail Paket</span></a>
	</td>
  
	<!--LIST PAKET MOBILE-->
	<td class="hjupl_mobel ">
		<div class="clr "></div>
		<div class="hjupl_mobel_kenca ">
			<div class="hjupl_berangkat ">
        <?php if( !empty( Mwt::get_field( 'tanggal_berangkat' ) ) ) : ?>
				<div class="baten ">
					<div class="hjupl_berangkat_kenca ">
						<span><?php echo $date->format('j'); ?></span>
					</div>
					<!--
						-->
					<div class="hjupl_berangkat_katuhu ">
						<span><?php echo $date->format('F'); ?></span>
						<span><?php echo $date->format('Y'); ?></span>
					</div>
				</div>
        <?php endif; ?>
			</div>
		</div>
		<div class="hjupl_mobel_katuhu ">
			<div class="hjupl_durasi ">
				<div class="hjupl_td "><?php the_title(); ?></div>
				<div class="hjupl_td "><strong>Durasi : </strong> <?php echo Mwt::get_field( 'durasi' ); ?> Hari</div>
			</div>
			<div class="hjupl_durasi ">
				<div class="hjupl_td "><strong>Pesawat : </strong></div>
				<?php $maskapai = Mwt::get_field('maskapai'); 
            if( !empty( $maskapai ) ) {
              foreach( $maskapai as $mskp ) {
                echo '<div class="hjupl_pesawat_ikon hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="'.$mskp->post_title.'"><img src="'.get_the_post_thumbnail_url( $mskp->ID ).'" alt="'.$mskp->post_title.' Logo Title "></div>';
              }
            }
					?>
			</div>
			<div class="hjupl_durasi hjupl_hotel ">
				<div class="hjupl_td "><strong>Hotel : </strong></div>
        <?php
        if( !empty( $penginapan ) ) {
          foreach( $penginapan as $hotel ) {
            echo '<i class="ic_bentang ic_bentang'.$hotel['bintang'].' hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="'.$hotel['nama_hotel'].'"></i>';    
          }
        }
        ?>
			</div>
			<div class="hjupl_harga ">
				<div class="hjupl_harga_normal hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="-+ <?php echo ( !empty( $harga['quard'] ) ) ? '$ ' . mwt_currency( convertCurrency( $harga['quard'], 'IDR', 'USD' ) ) : ''; ?>"> <?php echo ( !empty( $harga['quard'] ) ) ? 'Rp' . mwt_singkat_harga( $harga['quard'] ) : ''; ?></div>
				<!--<div class="hjupl_harga_normal hint--rounded hint--biru hint--uppercase hint--bounce hint--top " data-hint="USD 1800 ">Rp 27.5 JT</div>
					<div class="hjupl_harga_diskon ">Rp 30 JT</div>-->
			</div>
			<div class="hjupl_pesan ">
				<a href="<?php the_permalink(); ?>#pesan_paket_2 " class="baten baten_dosis "><span>Pesan Sekarang</span></a>
				<a href="<?php the_permalink(); ?>" class="baten baten_oren baten_dosis "><span>Detail Paket</span></a>
			</div>
		</div>
		<div class="clr "></div>
	</td>
	<!--LIST PAKET MOBILE-->
</tr><!-- #post-<?php the_ID(); ?> -->
<?php endif; endif; ?>