<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="tengah">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/demo/thankyou.png" alt=" ">
  </div>
  <br>
  <!--TAMBAHAN 07/03/16-->
  <h1 class="teks_ageung">Terima Kasih</h1>
  <div class="separator separator_hideung"><span></span></div>

  <div class="halaman_konten ">
      <div class="teks_statis tengah">
          <p>
              <h2 style="text-align: center;">Terima kasih sudah melakukan pemesanan Umroh.</h2>
              <p style="text-align: center;">&#160;</p>
              <p style="text-align: center;">&#160;</p>
              <p style="text-align: center;">Kami Akan Segera Menghubungi Anda, untuk konfirmasi pemesanan.</p>
              <p style="text-align: center;">&#160;</p>
              <p style="text-align: center;"><?php echo get_bloginfo('name'); ?> Tour Travel Layanan Umroh dan Haji Plus Terbaik Sejak Tahun 2008.</p>
              <p style="text-align: center;">Telah Mengantar Tamu Allah Sejak Tahun 2008</p>
              <p style="text-align: center;">&#160;</p>
              <p style="text-align: center;">Salam hangat :)</p>
          </p>

          <p>&nbsp;</p>
          <p>&nbsp;</p>
          <a href="<?php echo esc_url( home_url('/') ); ?>" class="baten baten_dosis" title="Kembali ke Beranda"><span class="teks_ageung">Kembali ke Beranda</span></a>
      </div>
  </div>

</div>

</article><!-- #post-<?php the_ID(); ?> -->
