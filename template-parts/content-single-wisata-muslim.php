<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option;
// $mwt->dump( Mwt::get_field('itenary') );
$harga = Mwt::get_field( 'harga' );
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="hs_kenca">
		<div class="hdu_foto">
			<div class="hdu_foto_gede hdu_foto_gede_YR">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt=" ">
			</div>
			<div class="hdu_foto_leutik">
        <div class="hdu_fl_peg">
          <span class="baten" id="prepHDUFL"><i class="i_arahkenca"></i></span>
          <span class="baten" id="neksHDUFL"><i class="i_arahkatuhu"></i></span>
        </div>
        
        <?php $galeries = Mwt::get_field( 'gambar' ); ?>
        <?php if( !empty( $galeries ) ) : ?>
        <ul id="hdu_foto_leutik">
          <?php $count = 0; foreach( $galeries as $galeri ): ?>
            <li>
              <a class="<?php echo ( $count == 0 ) ? 'hdu_fl_aktip' : '' ; ?>" data-full="<?php echo $galeri['url']; ?>"><img src="<?php echo $galeri['sizes']['thumbnail']; ?>" title="<?php echo $galeri['name']; ?>" alt="<?php echo $galeri['name']; ?>"></a>
            </li>
          <?php $count++; endforeach; ?>
        </ul>
        <script>
        $(document).ready(function() {
          var hdu_foto_leutik = $("#hdu_foto_leutik");
          hdu_foto_leutik.owlCarousel({
            itemsCustom : [
              [0, 1],
              [300, 2],
              [340, 3],
              [450, 4],
              [650, 5]
            ],
            pagination : false, navigation : false, mouseDrag : true, autoPlay : true, stopOnHover : true, lazyLoad : true, slideSpeed : 200, paginationSpeed : 800
          });

          $("#neksHDUFL").click(function(){ hdu_foto_leutik.trigger('owl.next'); })
          $("#prepHDUFL").click(function(){ hdu_foto_leutik.trigger('owl.prev'); })

          });
        </script>
        <?php endif; ?>
			</div>
		</div>
		<div class="separator separator_border"><span></span></div>
		<div class="hdu_inpo">
			<div class="clr"></div>
			<div class="hdu_inpo_baris">
				<div class="hdu_inpo_baris_kolom hdu_ibk_2">
					<h4 class="teks_ageung">Tanggal Keberangkatan</h4>
					<p><?php echo Mwt::get_field( 'tanggal_berangkat' ); ?></p>
				</div>
				<div class="hdu_inpo_baris_kolom hdu_ibk_25">
					<h4 class="teks_ageung">Durasi</h4>
					<p><?php echo Mwt::get_field( 'durasi' ); ?> Hari</p>
				</div>

			</div>
			<div class="hdu_inpo_baris">
				<div class="hdu_inpo_baris_kolom hdu_ibk_2">
					<h4 class="teks_ageung">Penerbangan</h4>
					<p class="hdu_ib_penerbangan">
            <?php $maskapai = Mwt::get_field('maskapai'); ?>
            <?php if( !empty( $maskapai ) ) :?>
            <?php foreach( $maskapai as $mskp ) : ?>
            <span class="hdu_ib_penerbangan_logo"><img src="<?php echo get_the_post_thumbnail_url( $mskp->ID );?>" alt="<?php echo $mskp->post_title; ?> Logo Title"></span> <?php echo $mskp->post_title; ?>
            <?php endforeach; endif; ?>
						<br>
            <?php $transit = Mwt::get_field( 'transit' ); ?>
            <?php if( !$transit ) : ?>
            No Transit
            <?php else : ?>
            <?php echo Mwt::get_field( 'lokasi_transit' ); ?>
            <?php endif; ?>
						<!-- TEKS PENERBANGAN MADINAH -->
					</p>
				</div>
				<div class="hdu_inpo_baris_kolom hdu_ibk_2">
					<h4 class="teks_ageung">Tour Leader / Guide</h4>
					<p><?php echo Mwt::get_field( 'pembimbing' ); ?></p>
				</div>
			</div>
			<div class="hdu_inpo_baris teu_margin">
        <?php $penginapan = Mwt::get_field('penginapan');?>
        <?php if( !empty( $penginapan ) ) :?>
        <?php foreach( $penginapan as $hotel ) : ?>
				<div class="hdu_inpo_baris_kolom hdu_ibk_2">
					<h4 class="teks_ageung hdu_ib_bentang"><?php echo $hotel['nama_hotel']; ?> <i class="ic_bentang ic_bentang<?php echo $hotel['bintang']; ?> hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang <?php echo $hotel['bintang']; ?>"></i></h4>
					<p><?php echo $hotel['lokasi']; ?></p>
				</div>
				<?php endforeach; endif; ?>
			</div>
			<div class="hdu_inpo_baris hdu_inpo_baris_mobel">
				<div class="hdu_inpo_baris_kolom hdu_ibk_2">
					<h4 class="teks_ageung">Harga Paket</h4>
					<p>
            IDR <?php echo mwt_currency( $harga['quard'] ); ?> (Quard)<br> 
            IDR <?php echo mwt_currency( $harga['triple'] ); ?> (Triple)<br>
            IDR <?php echo mwt_currency( $harga['double'] ); ?> (Double)
					</p>
				</div>
				<div class="hdu_inpo_baris_kolom hdu_ibk_2">
					<h4 class="teks_ageung">Sisa Seat</h4>
					<p><?php echo intval( get_post_meta( get_the_ID(), 'sisa_kuota', true ) ); ?> Seat</p>
				</div>
			</div>
			<div class="clr"></div>
		</div>
		<div class="separator separator_border"><span></span></div>
    <div class="hdu_ditail responsive-tabs">
      <h2>Overview</h2>
      <div class="teks_statis entry-content">
        
        <?php the_content(); ?>
        
      </div>
      <h2>Itenary</h2>
      <div class="teks_statis hdu_itenary">
        <p><strong>ITINERARY <?php echo strtoupper(get_the_title()); ?></strong></p>
        <?php $itinerary = Mwt::get_field( 'itenary' ); ?>
        <?php echo $itinerary; ?>
      </div>
      <h2>Syarat & Ketentuan</h2>
      <div class="teks_statis">
        <?php echo $mwt_option['sdk-content']; ?>
      </div>
      <h2>Cara Pembayaran</h2>
      <div class="teks_statis">
        <?php echo $mwt_option['cara-bayar-content']; ?>
      </div>
    </div>

    <!--SHARE-->
    <div class="halaman_komentar">
      <h4>Sebarkan Ini</h4>
      <!--SHARE THIS-->
      <div id="social-share"></div>
      <!--SHARE THIS-->
      <script>
      $("#social-share").jsSocials({
          showLabel: false,
          showCount: true,
          shareIn: "popup",
          shares: ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "whatsapp"]
      });
      </script>
    </div>
    <!--SHARE-->
	</div>
	<div class="widget-area">
		<div class="hdu_sidebar hdu_seat">
      <?php if( intval( get_post_meta( get_the_ID(), 'sisa_kuota', true ) ) < 1 ) : ?>
      <div class="hdu_seat_soldout"></div>
      <?php endif; ?>
			<div class="hdu_seat_inpo">
				<h4 class="teks_ageung">Sisa Seat Sekarang</h4>
				<div class="hdu_seat_inpo_angka">
					<span><?php echo intval( get_post_meta( get_the_ID(), 'sisa_kuota', true ) ); ?></span>
				</div>
			</div>
			<div class="separator separator_abu"><span></span></div>
			<a href="#pesan_paket_2" title="Pesan Sekarang">
				<h4 class="teks_ageung">Pesan Sekarang!</h4>
			</a>
		</div>
		<div class="separator">&nbsp;</div>
		<div class="hdu_sidebar hdu_harga">
			<!-- BLOK HARGA -->
			<h4 class="teks_ageung">Harga Paket</h4>
			<div class="separator separator_abu ngenca"><span></span></div>
      <?php if( !empty( $harga['quard'] ) ) : ?>
			<div class="hdu_harga_inpo hint--rounded hint--biru hint--bounce hint--top hint--13" data-hint="+- $ <?php echo mwt_currency( convertCurrency( $harga['quard'], 'IDR', 'USD' ) ); ?>">
				<h5>Rp <?php echo mwt_currency( $harga['quard'] ); ?></h5>
				<!--DISKON-->
				<span class="teks_ageung">Quard</span>
			</div>
      <?php endif; ?>
      <?php if( !empty( $harga['triple'] ) ) : ?>
			<div class="hdu_harga_inpo hint--rounded hint--biru hint--bounce hint--top hint--13" data-hint="+- $ <?php echo mwt_currency( convertCurrency( $harga['triple'], 'IDR', 'USD' ) ); ?>" id="pesan_paket">
				<!--ID PESAN SINGKAT UNTUK MENGARAHKAN USER KE FORM PEMESANAN DARI KLIK PESAN SEKARANG DI SISA SEAT-->
				<h5>Rp <?php echo mwt_currency( $harga['triple'] ); ?></h5>
				<!--DISKON-->
				<span class="teks_ageung">Triple</span>
			</div>
      <?php endif; ?>
      <?php if( !empty( $harga['double'] ) ) : ?>
			<div class="hdu_harga_inpo hint--rounded hint--biru hint--bounce hint--top hint--13" data-hint="+- $ <?php echo mwt_currency( convertCurrency( $harga['double'], 'IDR', 'USD' ) ); ?>" id="pesan_paket_2">
				<!--ID PESAN SINGKAT UNTUK MENGARAHKAN USER KE FORM PEMESANAN DARI KLIK PESAN SEKARANG DI SISA SEAT-->
				<h5>Rp <?php echo mwt_currency( $harga['double'] ); ?></h5>
				<!--DISKON-->
				<span class="teks_ageung">Double</span>
			</div>
      <?php endif; ?>
		</div>
		<div class="separator">&nbsp;</div>
		<div class="hdu_sidebar hdu_pesan">
			<h4 class="tengah">Pesan Sekarang! <br> Kami Siap Mendampingi Kekhusyukan Ibadah Anda!</h4>
			<div class="separator separator_abu"><span></span></div>
			<div class="form form_border">
				<form class="form_pesan_ur_1" action="#" method="post" id="form-pesanan">
					<div class="clr"></div>
					<div class="blok_form blok_form_100">
						<input name="nama" class="input input_pad nama_ur_1" placeholder="Nama Lengkap" type="text">
					</div>
					<div class="blok_form blok_form_100">
						<input name="email" class="input input_pad email_ur_1" placeholder="Alamat Email" type="email">
					</div>
					<div class="blok_form blok_form_100">
						<input name="telepon" class="input input_pad telepon_ur_1" placeholder="No. Handphone" type="text">
					</div>
					<div class="blok_form blok_form_100 teu_margin">
            <input type="hidden" name="paket_id" value="<?php echo get_the_ID(); ?>">
						<button type="submit" class="baten baten_icon submitPesan_ur_1" name="submit"><i class="i_kursor"></i> <span class="teks_ageung">Pesan</span></button>
					</div>
					<div class="clr"></div>
				</form>
			</div>
			<div class="pesan_status_ur_1"></div>
			<div class="separator separator_border"><span></span></div>
			<a href="#lkyt" class="lkyt_hrep tengah bukaPopup">
				<h5><?php echo $mwt_option['sdb-single-umroh-title']; ?></h5>
			</a>
			<div class="lkyt_ikon">
				<div class="lkyt_ikon_blok">
					<div class="lkyt_ikon_blok_ic"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon4.jpg" style="border-radius:100%"></div>
					<h3><a href="#"><?php echo $mwt_option['sdb-single-umroh-content-1']; ?></a></h3>
				</div>
				<div class="lkyt_ikon_blok">
					<div class="lkyt_ikon_blok_ic"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon1.jpg" style="border-radius:100%"></div>
					<h3><?php echo $mwt_option['sdb-single-umroh-content-2']; ?></h3>
				</div>
				<div class="lkyt_ikon_blok">
					<div class="lkyt_ikon_blok_ic"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon3.jpg" style="border-radius:100%"></div>
					<h3><?php echo $mwt_option['sdb-single-umroh-content-3']; ?></h3>
				</div>
				<div class="lkyt_ikon_blok">
					<div class="lkyt_ikon_blok_ic"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon5.jpg" style="border-radius:100%"></div>
					<h3><?php echo $mwt_option['sdb-single-umroh-content-4']; ?></h3>
				</div>
				<div class="lkyt_ikon_blok">
					<div class="lkyt_ikon_blok_ic"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon2.jpg" style="border-radius:100%"></div>
					<h3 class="lkyt_ikon_blok_1line"><?php echo $mwt_option['sdb-single-umroh-content-5']; ?></h3>
				</div>
			</div>
		</div>
	</div>
</article>
<!-- #post-<?php the_ID(); ?> -->

<!--FOTO-->
<script>
$(document).ready(function(){
  $('.hdu_foto_leutik a').click(function(){
      var largeImage = $(this).attr('data-full');
    $('.hdu_fl_aktip').removeClass();
    $(this).addClass('hdu_fl_aktip');
    $('.hdu_foto_gede_YR img').hide();
    $('.hdu_foto_gede_YR img').attr('src', largeImage);
    $('.hdu_foto_gede_YR img').fadeIn();
  }); 
}); 
</script>
<!--FOTO-->

<!--TAB RESPONSIVE-->
<script type="text/javascript">
/*ResponsiveTabs.js | Version:1.10 | Author:Pete Love | www.petelove.com*/
var RESPONSIVEUI={};(function($){RESPONSIVEUI.responsiveTabs=function(){var $tabSets=$(".responsive-tabs");if(!$tabSets.hasClass("responsive-tabs--enabled")){$tabSets.addClass("responsive-tabs--enabled");var tablistcount=1;$tabSets.each(function(){var $tabs=$(this);$tabs.children(":header").addClass("responsive-tabs__heading");$tabs.children("div").addClass("responsive-tabs__panel");var $activePanel=$tabs.find(".responsive-tabs__panel--active");if(!$activePanel.length){$activePanel=$tabs.find(".responsive-tabs__panel").first().addClass("responsive-tabs__panel--active")}$tabs.find(".responsive-tabs__panel").not(".responsive-tabs__panel--active").hide().attr("aria-hidden","true");$activePanel.attr("aria-hidden","false");$activePanel.addClass("responsive-tabs__panel--closed-accordion-only");var $tabsWrapper=$("<div/>",{"class":"responsive-tabs-wrapper"});$tabs.wrap($tabsWrapper);var highestHeight=0;$tabs.find(".responsive-tabs__panel").each(function(){var tabHeight=$(this).height();if(tabHeight>highestHeight){highestHeight=tabHeight}});var $tabList=$("<ul/>",{"class":"responsive-tabs__list",role:"tablist"});var tabcount=1;$tabs.find(".responsive-tabs__heading").each(function(){var $tabHeading=$(this);var $tabPanel=$(this).next();$tabHeading.attr("tabindex",0);var $tabListItem=$("<li/>",{"class":"responsive-tabs__list__item",id:"tablist"+tablistcount+"-tab"+tabcount,"aria-controls":"tablist"+tablistcount+"-panel"+tabcount,role:"tab",tabindex:0,text:$tabHeading.text(),keydown:function(objEvent){if(objEvent.keyCode===13){$tabListItem.click()}},click:function(){$tabsWrapper.css("height",highestHeight);$tabs.find(".responsive-tabs__panel--closed-accordion-only").removeClass("responsive-tabs__panel--closed-accordion-only");$tabs.find(".responsive-tabs__panel--active").toggle().removeClass("responsive-tabs__panel--active").attr("aria-hidden","true").prev().removeClass("responsive-tabs__heading--active");$tabPanel.toggle().addClass("responsive-tabs__panel--active").attr("aria-hidden","false");$tabHeading.addClass("responsive-tabs__heading--active");$tabList.find(".responsive-tabs__list__item--active").removeClass("responsive-tabs__list__item--active");$tabListItem.addClass("responsive-tabs__list__item--active");$tabsWrapper.css("height","auto")}});$tabPanel.attr({role:"tabpanel","aria-labelledby":$tabListItem.attr("id"),id:"tablist"+tablistcount+"-panel"+tabcount});if($tabPanel.hasClass("responsive-tabs__panel--active")){$tabListItem.addClass("responsive-tabs__list__item--active")}$tabList.append($tabListItem);$tabHeading.keydown(function(objEvent){if(objEvent.keyCode===13){$tabHeading.click()}});$tabHeading.click(function(){$tabs.find(".responsive-tabs__panel--closed-accordion-only").removeClass("responsive-tabs__panel--closed-accordion-only");if(!$tabHeading.hasClass("responsive-tabs__heading--active")){var oldActivePos,$activeHeading=$tabs.find(".responsive-tabs__heading--active");if($activeHeading.length){oldActivePos=$activeHeading.offset().top}$tabs.find(".responsive-tabs__panel--active").slideToggle().removeClass("responsive-tabs__panel--active").prev().removeClass("responsive-tabs__heading--active");$tabs.find(".responsive-tabs__panel").hide().attr("aria-hidden","true");$tabPanel.slideToggle().addClass("responsive-tabs__panel--active").attr("aria-hidden","false");$tabHeading.addClass("responsive-tabs__heading--active");var $currentActive=$tabs.find(".responsive-tabs__list__item--active");$currentActive.removeClass("responsive-tabs__list__item--active");var panelId=$tabPanel.attr("id");var tabId=panelId.replace("panel","tab");$("#"+tabId).addClass("responsive-tabs__list__item--active");var tabsPos=$tabs.offset().top;var newActivePos=($tabHeading.offset().top)-15;if(oldActivePos<newActivePos){$("html, body").animate({scrollTop:tabsPos},0).animate({scrollTop:newActivePos},400)}}else{$tabPanel.removeClass("responsive-tabs__panel--active").slideToggle(function(){$(this).addClass("responsive-tabs__panel--closed-accordion-only")});$tabHeading.removeClass("responsive-tabs__heading--active")}});tabcount++});$tabs.prepend($tabList);tablistcount++})}}})(jQuery);
</script>
<script>
$(document).ready(function() {
  RESPONSIVEUI.responsiveTabs();
})
</script>
<!--TAB RESPONSIVE-->