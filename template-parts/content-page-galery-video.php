<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */
$video_type = Mwt::get_field('tipe_video');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="hla_lisna hla_lisna2 magnificPopup" id="hla_lisna">
      <div class="clr"></div>

          <?php
          // WP_Query arguments
          $args = array(
            'post_type'              => array( 'mwt-galery-video' ),
            'post_status'            => array( 'publish' ),
            'posts_per_page'         => -1,
          );

          // The Query
          $query = new WP_Query( $args );

          // The Loop
          if ( $query->have_posts() ) {
            $count = 0;
            while ( $query->have_posts() ) {
              $query->the_post(); ?>
	    				
		    			<a href="<?php echo ( $video_type == 'Local' ) ? Mwt::get_field( 'video' ) : Mwt::get_field( 'url_video' ); ?>" title="<?php the_title(); ?>" class="hla_lisna_blok popup-youtube">
		    				<div class="hla_lb_gbr">
                  <?php
                  $thumbnail_url = 'https://img.youtube.com/vi/' . str_replace('https://www.youtube.com/watch?v=', '', Mwt::get_field( 'url_video' )) . '/default.jpg';
                  ?>
                  <img src="<?php echo $thumbnail_url; ?>" title="<?php the_title(); ?>" alt="<?php echo strip_tags( get_the_content() ); ?>"> 
                  <div class="hla_lb_gbr_pidio"><i class="ic_play"></i></div> 
                </div>
		    				<span><?php the_title(); ?></span>
		    				<h4><?php the_title(); ?></h4>
		    			</a>
              <?php
              $count++;
            }
          }
          // Restore original Post Data
          wp_reset_postdata();
          ?>
      <div class="clr"></div>
    </div>

    <!--PEGING-->
    <div class="halaman_peging ">
      <?php the_posts_pagination(); ?>
      <script>
        $(".nav-links .page-numbers").addClass('baten');
      </script>
      <br>
    </div>
    <!--PEGING-->

</article><!-- #post-<?php the_ID(); ?> -->

			<!--MAGNIFIC POPUP-->
			<script type="text/javascript">
        $(document).ready(function() {
            $('.popup-youtube').magnificPopup({
                type: 'iframe',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                preloader: false,

                fixedContentPos: false
            });
        });
		    </script>
			<!--MAGNIFIC POPUP-->
