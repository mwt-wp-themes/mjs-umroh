<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content">
    
<div class="pure-g">
    <div class="pure-u-1-3"></div>
    <div class="pure-u-1-3"><?php echo do_shortcode('[custom-login-form]'); ?></div>
    <div class="pure-u-1-3"></div>
</div>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
