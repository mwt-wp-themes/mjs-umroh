<?php
/**
 * Template name: Karir
 */
global $mwt, $mwt_option;
$page_id = get_the_ID();
$custom_pages = array( $mwt_option['galery-page-id'], $mwt_option['galery-video-page-id'], $mwt_option['account-page-id'], $mwt_option['login-page-id'], $mwt_option['thankyou-page-id'] );
$sidebar = ( in_array( $page_id, $custom_pages ) ) ? false : true;
if( $page_id == $mwt_option['galery-page-id'] || $page_id == $mwt_option['galery-video-page-id'] ) {
  $gal_type = ( $page_id == $mwt_option['galery-video-page-id'] ) ? "video" : "gambar";
  $container = 'halaman halaman_lis_artikel';
} else {
  $container = 'halaman'; 
}
get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi"><!--primary-->
		<main id="main" class="site-main batasna_kabeh halaman halaman_koleps">
      
      <div class="batasna_wrap">

          <div class="breadcumb">
              <div class="clr"></div>

              <!-- PERUBAHAN 10 April 2016 -->
              <div class="brdcmb_knc">
                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
                        <strong><span itemprop="title">Beranda</span></strong>
                  </a>
                  </span>

                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo get_the_permalink(); ?>" itemprop="url">/
                        <span itemprop="title"><?php echo get_the_title(); ?></span>
                  </a>
                  </span>
              </div>

              <div class="brdcmb_kth"></div>

              <div class="clr"></div>
          </div>

          <?php if( $page_id != $mwt_option['thankyou-page-id'] ) : ?>
          <h1><?php echo get_the_title(); ?></h1>
          <div class="separator separator_hideung"><span></span></div>
          <?php endif; ?>
        
          <div class="halaman_konten <?php echo ( $sidebar ) ? 'halaman_sidebar' : ''; ?>">
              <div class="clr"></div>

              
              <div class="hs_kenca">
              
                
                <?php
                while ( have_posts() ) :
                  the_post(); ?>



                  <div class="hklps_lis">
                    <div class="hklps_lis_heding hklps_lh_muka"><?php echo Mwt::get_field('lowongan'); ?></div>
                    <div class="hklps_lis_konten teks_statis hklps_lk_muka">
                      <?php the_content(); ?>
                    </div>
                  </div>

                
                <?php
                  // If comments are open or we have at least one comment, load up the comment template.
                  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                  endif;

                endwhile; // End of the loop.
                ?>

                <!--SHARE-->
                <div class="halaman_komentar">
                  <h4>Sebarkan Ini</h4>
                  <!--SHARE THIS-->
                  <div id="social-share"></div>
                  <!--SHARE THIS-->
                  <script>
                  $("#social-share").jsSocials({
                      showLabel: false,
                      showCount: true,
                      shareIn: "popup",
                      shares: ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "whatsapp"]
                  });
                  </script>
                </div>
                <!--SHARE-->
              </div>
              <?php get_sidebar(); ?>
              <div class="clr"></div>
          </div>

      </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<!--KOLEPS--><!-- FAQ, KARIR -->
<script>
  $(document).ready(function() {
  var hklps_lis = $('.hklps_lis'),
  hklps_lis_heding = $('.hklps_lis_heding'),
  hklps_lis_konten = $('.hklps_lis_konten');

  hklps_lis.children(hklps_lis_heding).on('click', function(){
    $(this).toggleClass("hklps_lh_muka").next(hklps_lis_konten).toggleClass("hklps_lk_muka").end().parent(".hklps_lis").siblings(".hklps_lis").children(hklps_lis_heding).removeClass("hklps_lh_muka").next(hklps_lis_konten).removeClass("hklps_lk_muka");
  });

  });
  </script>
<!--KOLEPS-->

<?php
get_footer();
