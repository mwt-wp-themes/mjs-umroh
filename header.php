<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="format-detection" content="telephone=no">
  <meta name="webcrawlers" content="all" />
  <meta http-equiv="cache-control" content="max-age=0" />
  <meta http-equiv="cache-control" content="no-cache" />
  <meta http-equiv="expires" content="0" />
  <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
  <meta http-equiv="pragma" content="no-cache" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
  <?php wp_head(); ?>
  <!--[if lte IE 8]>
      <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-old-ie-min.css">
  <![endif]-->
  <!--[if gt IE 8]><!-->
      <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">
  <!--<![endif]-->
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
  
    <!--KALUHUR-->
    <a id="kaluhur" class="baten" onclick="jQuery('html, body').animate({scrollTop:0}, 'slow');" href="javascript:void(0);" style="display: none;"><i></i></a>
    <!--KALUHUR-->

    <?php if( !is_single() || !is_singular() ) : ?>
    <!--POPUP DOA TAWAF-->
    <!-- TAMBAH CLASS POSISI DOA saaat detail haji dan wisata-->
    <a id="buka_popup_DoaTawaf" class="baten baten_dosis "><span class="teks_ageung">Doa Tawaf</span></a>
    <div class="batasna_kabeh popup" id="popup_DoaTawaf" style="display: none;">
        <div class="batasna_wrap">

            <div class="popup_blok">
                <h2>Doa Tawaf <span class="popup_blok_klos" id="popup_blok_klos_DoaTawaf">x</span></h2>
                <div class="popup_blok_konten popup_doa_tawaf">

                    <div id="DoaTawaf_Player"></div>
                    <ul class="ubaplayer-controls">
                        <div class="clr"></div>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.1_5awK1e2q3v.mp3">Do'a Tawaf 1</a></li>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.2_8OuS70e30o.mp3">Do'a Tawaf 2</a></li>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.3_qN2ay1i209.mp3">Do'a Tawaf 3</a></li>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.4_45624eNb82.mp3">Do'a Tawaf 4</a></li>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.5_79klV4EnU6.mp3">Do'a Tawaf 5</a></li>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.6_291Q144zi2.mp3">Do'a Tawaf 6</a></li>

                        <li><a class="ubaplayer-button" href="<?php echo get_template_directory_uri(); ?>/assets/media/doa_tawaf/doa.tawaf.7_06ic4JKU34.mp3">Do'a Tawaf 7</a></li>

                        <div class="clr"></div>
                    </ul>

                </div>
            </div>

        </div>
    </div>
    <!--POPUP DOA TAWAF-->
    <?php endif; ?>
    <!--KONTEN MENU MOBILE-->
    <div class="konten_menu_mobile" id="konten_menu_mobile">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="kmm_logo" title="<?php echo get_bloginfo( 'description' ); ?>"><img src="<?php echo $mwt_option['mwt-logo']['url']; ?>" style="width:50px; height: 60px;" title="<?php echo get_bloginfo( 'description' ); ?>" alt="<?php echo get_bloginfo('name'); ?>"></a>

        <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-1',
          'menu_class'        => 'kmm_menu',
          'container'       => 'ul'
        ) );
        ?>
        <span class="kmm_border"></span>
        <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-2',
          'menu_class'        => 'kmm_menu kmm_menu_kadua',
          'container'       => 'ul'
        ) );
        ?>
        <div class="kmm_layanan">
            <a href="tel:<?php echo $mwt_option['contact-phone']; ?>"><?php echo $mwt_option['contact-phone']; ?></a>
            <a href="#">Layanan 7 Hari 24 Jam</a>
        </div>

    </div>
    <!--KONTEN MENU MOBILE-->
    <!-- start menu -->
    <header class="batasna_kabeh" id="header_menu">

        <div class="batasna_kabeh menu_utama_v2 <?php echo ( !is_home() || !is_front_page() ) ? 'muv2_skrol' : ''; ?>">
            <div class="muv2_kenca">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>" class="logo_caang"><img src="<?php echo $mwt_option['mwt-logo']['url']; ?>" style="width:50px; height: 60px;" title="<?php bloginfo( 'name' ); ?>" alt="<?php bloginfo( 'name' ); ?>"></a>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>" class="logo_poek"><img src="<?php echo $mwt_option['mwt-logo']['url']; ?>" style="width:50px; height: 60px;" title="<?php bloginfo( 'name' ); ?>"></a>
            </div>
            <div class="muv2_tengah">
              <?php
              wp_nav_menu( array(
                'theme_location' => 'menu-1',
                'menu_id'        => 'primary-menu',
                'container'       => 'ul'
              ) );
              ?>
            </div>
            <div class="muv2_katuhu">
                <a href="tel:<?php echo $mwt_option['contact-phone']; ?>" title="" class="baten baten_icon"><i class="i_telpon"></i><span><?php echo $mwt_option['contact-phone']; ?></span></a>
            </div>
        </div>

        <div class="batasna_kabeh menu_mobile">
            <div class="batasna_wrap">

                <div class="mm_kenca">
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $mwt_option['mwt-logo']['url']; ?>" style="width:50px; height: 60x;" alt=" "></a>
                </div>

                <div class="mm_katuhu">
                    <a id="menu_mobile">
                        <span class="mmk_teks">Menu</span>
                        <span class="mmk_ikon"></span>
                    </a>
                </div>

            </div>
        </div>

    </header>

    <div class="batasna_kabeh menu_handap mh_skrol">
        <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-2',
          'menu_id'        => 'secondary-menu',
          'container'       => 'ul'
        ) );
        ?>
    </div>

	<div id="content" class="site-content">