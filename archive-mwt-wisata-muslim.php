<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

get_header();
?>


	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi">
		<main id="main" class="site-main batasna_kabeh halaman halaman_lis_artikel halaman_lis_wisata_muslim">
    <div class="batasna_wrap">

        <div class="breadcumb">
            <div class="clr"></div>

            <!-- PERUBAHAN 10 April 2016 -->
            <div class="brdcmb_knc">
                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		    				<a href="<?php echo esc_url( home_url('/') ); ?>" itemprop="url">
		    					<strong><span itemprop="title">Beranda</span></strong>
                </a>
                </span>

                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	    					<a href="<?php echo get_post_type_archive_link( 'mwt-wisata-muslim' ); ?>" itemprop="url">/
	    						<span itemprop="title">Wisata Muslim</span>
                </a>
                </span>

            </div>
          
            <div class="brdcmb_kth"></div>

            <div class="clr"></div>
        </div>

        <h1>Wisata Muslim</h1>
        <div class="separator separator_hideung"><span></span></div>

        <div class="halaman_konten">



        <section class="top_promo">
                    <?php if ( have_posts() ) : ?>
                    <ul id="wm_lisna">
                      <?php
                        /* Start the Loop */
                        while ( have_posts() ) :
                          the_post();

                          /*
                           * Include the Post-Type-specific template for the content.
                           * If you want to override this in a child theme, then include a file
                           * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                           */
                          //get_template_part( 'template-parts/content', 'archive' );
                          ?>

                        <li>
                          <?php $harga = Mwt::get_field( 'harga' ); ?>
                          <a href="<?php the_permalink(); ?>" title="">
                            <div class="tp_hulu">
                              <div class="tph_harga hint--rounded hint--biru hint--bounce hint--bottom hint--13" data-hint="+- $ <?php echo mwt_currency( convertCurrency( $harga['quard'], 'IDR', 'USD' ) ); ?>">
                                <div class="tph_harga_blok">
                                  <p><?php echo ( !empty( $harga['quard'] ) ) ? '<span>IDR</span> ' . mwt_singkat_harga( $harga['quard'] ) : ''; ?></p>
                                </div>
                              </div>
                              <h4><?php the_title(); ?></h4>
                            </div>
                            <div class="tp_lis">
                              <div class="clr"></div>
                              <div class="tplna">
                                <div class="tplna_kenca tpl2ln"><i></i></div>
                                <div class="tplna_katuhu">
                                  <span>Tour Leader / Guide</span>
                                  <p><?php echo Mwt::get_field('pembimbing'); ?></p>
                                </div>
                              </div>
                            <?php $penginapan = Mwt::get_field('penginapan');
                              if( !empty( $penginapan ) ) {
                                foreach( $penginapan as $hotel ) { ?>
                              <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="Hotel Bintang <?php echo $hotel['bintang']; ?>">
                                <div class="tplna_kenca tpl2ln"><i></i></div>
                                <div class="tplna_katuhu">
                                  <span><?php echo $hotel['nama_hotel']; ?></span>
                                  <p><?php echo $hotel['lokasi']; ?></p>
                                </div>
                                <div class="tplnak_bentang bentang<?php echo $hotel['bintang']; ?>"></div>
                              </div>
                              <?php
                                }
                              }
                              ?>
                            <?php $maskapai = Mwt::get_field('maskapai'); 
                              if( !empty( $maskapai ) ) {
                                foreach( $maskapai as $mskp ) { ?>
                              <div class="tplna hint--rounded hint--biru hint--uppercase hint--bounce hint--top" data-hint="<?php echo $mskp->post_title; ?>">
                                <div class="tplna_kenca tpl2ln"><i></i></div>
                                <div class="tplna_katuhu">
                                  <span>Pesawat</span>
                                  <p><?php echo $mskp->post_title; ?></p>
                                </div>
                              </div>
                              <?php
                                }
                              }
                              ?>
                              <?php if( !empty( Mwt::get_field('durasi') ) ) : ?>
                              <div class="tplna">
                                <div class="tplna_kenca"><i></i></div>
                                <div class="tplna_katuhu">
                                  <p><?php echo ( !empty( Mwt::get_field('durasi') ) ) ? 'Durasi ' . Mwt::get_field('durasi') . ' Hari' : ''; ?></p>
                                </div>
                              </div>
                              <?php endif; ?>
                              <div class="clr"></div>
                            </div>
                          </a>
                        </li>

                      <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
        </section>

						<!--MASONRY-->
			<script type="text/javascript">
				$('#wm_lisna').masonry({
				 "gutter": 45,
				  itemSelector: 'li'
				});
			</script>
			<!--!MASONRY-->

		    	</div>

	    	</div>

		</main><!-- #main -->
	</div><!-- #primary -->

  <script>
    $(function() {
      var nama_paket = $('select[name="paket"]').find(":selected").text();
      if( nama_paket !== '' && nama_paket !== 'Pilih Paket Umroh' ) {
        $("font.nama_paket_x").text(nama_paket);
      }
    });
  </script>

<?php
get_footer();
