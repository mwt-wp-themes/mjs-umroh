<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

get_header();
?>


<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi">
	<main id="main" class="site-main batasna_kabeh halaman halaman_lis_artikel">
		<!--LIST ARTIKEL-->
	
			<div class="batasna_wrap">
				<div class="breadcumb">
					<div class="clr"></div>
					<!-- PERUBAHAN 10 April 2016 -->
					<div class="brdcmb_knc">
						<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <a href="<?php echo esc_url( home_url('/') ); ?>" itemprop="url">
              <strong><span itemprop="title">Beranda</span></strong>
              </a>
						</span>
						<span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
              <a href="<?php echo $_SERVER['REQUEST_URI']; ?>" title="" itemprop="url">/
                <?php the_archive_title( '<span itemprop="title">', '</span>' ); ?>
              </a>
						</span>
					</div>
					<!-- PERUBAHAN 10 April 2016 -->
					<div class="brdcmb_kth"></div>
					<div class="clr"></div>
				</div>
				<?php the_archive_title( '<h1>', '</h1>' ); ?>
				<div class="separator separator_hideung"><span></span></div>
				<div class="halaman_konten halaman_sidebar">
					<div class="clr"></div>
					<div class="hs_kenca">
            <?php if ( have_posts() ) : ?>
						<div class="hla_lisna" id="hla_lisna">
							<div class="clr"></div>
							<?php
								/* Start the Loop */
								while ( have_posts() ) :
								  the_post();
								
								  /*
								   * Include the Post-Type-specific template for the content.
								   * If you want to override this in a child theme, then include a file
								   * called content-___.php (where ___ is the Post Type name) and that will be used instead.
								   */
								  //get_template_part( 'template-parts/content', 'archive' );
								  ?>
							<a href="<?php the_permalink(); ?>" title="" class="hla_lisna_blok" style="">
								<div class="hla_lb_gbr">
									<?php the_post_thumbnail('thumbnail'); ?>
								</div>
								<span><?php the_author(); ?> - <?php echo get_the_date('d F Y'); ?></span>
								<h4><?php the_title(); ?></h4>
							</a>
							<?php endwhile; ?>
							<div class="clr"></div>
						</div>
						<!--PEGING-->
						<div class="halaman_peging">
							<?php the_posts_pagination(); ?>
              <script>
                $(".nav-links .page-numbers").addClass('baten');
              </script>
              <br>
						</div>
						<!--PEGING-->
            <?php endif; ?>
					</div>
					<?php get_sidebar(); ?>
					<div class="clr"></div>
				</div>
			</div>
		
		<!--LIST ARTIKEL-->
		<?php get_template_part( 'layouts/section', 'banner-handap' ); ?>
	</main>
	<!-- #main -->
</div>
<!-- #primary -->

			<script type="text/javascript">
				$('#hla_lisna').masonry({
				 "gutter": 15,
				  itemSelector: 'a'
				});
			</script>
<?php
get_footer();
