<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */
global $mwt, $mwt_option;
$page_id = get_the_ID();
$custom_pages = array( $mwt_option['galery-page-id'], $mwt_option['galery-video-page-id'], $mwt_option['account-page-id'], $mwt_option['login-page-id'], $mwt_option['thankyou-page-id'] );
$sidebar = ( in_array( $page_id, $custom_pages ) ) ? false : true;
if( $page_id == $mwt_option['galery-page-id'] || $page_id == $mwt_option['galery-video-page-id'] ) {
  $gal_type = ( $page_id == $mwt_option['galery-video-page-id'] ) ? "video" : "gambar";
  $container = 'halaman halaman_lis_artikel';
} else {
  $container = 'halaman'; 
}
get_header();
?>

	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi"><!--primary-->
		<main id="main" class="site-main batasna_kabeh <?php echo $container; ?>">
      
      <div class="batasna_wrap">

          <div class="breadcumb">
              <div class="clr"></div>

              <!-- PERUBAHAN 10 April 2016 -->
              <div class="brdcmb_knc">
                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo esc_url( home_url('/') ); ?>" title="" itemprop="url">
                        <strong><span itemprop="title">Beranda</span></strong>
                  </a>
                  </span>

                  <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                      <a href="<?php echo get_the_permalink(); ?>" itemprop="url">/
                        <span itemprop="title"><?php echo get_the_title(); ?></span>
                  </a>
                  </span>
              </div>

              <div class="brdcmb_kth"></div>

              <div class="clr"></div>
          </div>

          <?php if( $page_id != $mwt_option['thankyou-page-id'] ) : ?>
          <h1><?php echo get_the_title(); ?></h1>
          <div class="separator separator_hideung"><span></span></div>
          <?php endif; ?>
        
          <div class="halaman_konten <?php echo ( $sidebar ) ? 'halaman_sidebar' : ''; ?>">
              <div class="clr"></div>

              <?php if( $sidebar ) : ?>
              <div class="hs_kenca teks_statis">
              <?php endif; ?>
                
                <?php
                while ( have_posts() ) :
                  the_post();
                  
                  if( $page_id == $mwt_option['galery-page-id'] || $page_id == $mwt_option['galery-video-page-id'] ) {
                    
                    get_template_part( 'template-parts/content', 'page-galery-' . $gal_type );
                  } elseif( $page_id == $mwt_option['login-page-id'] ) {
                    
                    get_template_part( 'template-parts/content', 'page-login' );
                    
                  } elseif( $page_id == $mwt_option['account-page-id'] ) {
                    
                    get_template_part( 'template-parts/content', 'page-account' );
                    
                  } elseif( $page_id == $mwt_option['thankyou-page-id'] ) {
                    
                    get_template_part( 'template-parts/content', 'page-thankyou' );
                    
                  } else {
                    get_template_part( 'template-parts/content', 'page' ); 
                  }

                  // If comments are open or we have at least one comment, load up the comment template.
                  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                  endif;

                endwhile; // End of the loop.
                ?>
                <?php if( $sidebar ) : ?>
                <!--SHARE-->
                <div class="halaman_komentar">
                  <h4>Sebarkan Ini</h4>
                  <!--SHARE THIS-->
                  <div id="social-share"></div>
                  <!--SHARE THIS-->
                  <script>
                  $("#social-share").jsSocials({
                      showLabel: false,
                      showCount: true,
                      shareIn: "popup",
                      shares: ["facebook", "twitter", "googleplus", "linkedin", "pinterest", "whatsapp"]
                  });
                  </script>
                </div>
                <!--SHARE-->
              </div>
              <?php get_sidebar(); ?>
              <?php endif; ?>
              <div class="clr"></div>
          </div>

      </div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
