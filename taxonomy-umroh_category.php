<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

get_header();
?>


	<div id="konten_isi" class="content-area batasna_kabeh konten_isi_halaman konten_isi">
		<main id="main" class="site-main batasna_kabeh halaman halaman_jadwal_umroh">
    <div class="batasna_wrap">

        <div class="breadcumb">
            <div class="clr"></div>

            <!-- PERUBAHAN 10 April 2016 -->
            <div class="brdcmb_knc">
                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
		    				<a href="<?php echo esc_url( home_url('/') ); ?>" itemprop="url">
		    					<strong><span itemprop="title">Beranda</span></strong>
                </a>
                </span>

                <span itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
	    					<a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" itemprop="url">/
	    						<span itemprop="title">Jadwal Umroh</span>
                </a>
                </span>

            </div>
          
            <div class="brdcmb_kth"></div>

            <div class="clr"></div>
        </div>

        <h1><font class="nama_paket_x"><?php single_term_title(); ?></font> Dengan Pelayanan Terbaik &amp; Ternyaman</h1>
        <div class="separator separator_hideung"><span></span></div>

        <div class="halaman_konten">

            <div class="hju_notif" id="hju_notif">
                Berikut daftar paket umroh yang tersedia di <?php echo get_bloginfo( 'name' ); ?>, tersedia paket Promo sampai Diskon untuk setiap jamaah. Hotel Mekah yang digunakan mulai dari Bintang 4 sampai Bintang 5 dengan penawaran paling murah. Hotel Madinah yang digunakan mulai dari Bintang 4 sampai Bintang 5 dengan penawaran paling murah.

                <span id="hju_notif_klos">x</span>
            </div>

            <div class="form form_border hju_form">
                <form action="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" method="get">
                    <div class="clr"></div>

                    <div class="blok_form">
                        <div class="icon icon_kabah"><span><i></i></span></div>
                        <div class="selek">
                            <select class="input" name="paket">
                                <option value="">Pilih Paket Umroh</option>
                                <?php
                                $terms = get_terms( 'umroh_category', array(
                                    'hide_empty' => false,
                                ) );
                                foreach( $terms as $term ) {
                                  echo '<option value="' . $term->term_id . '"';
                                  echo ( !empty( $_REQUEST['paket'] ) && $term->term_id == $_REQUEST['paket'] ) ? ' selected' : '';
                                  echo '>' . $term->name . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <!--

		    				-->
                    <div class="blok_form">
                        <div class="icon icon_tanggal"><span><i></i></span></div>
                        <div class="selek">
                            <select class="input" name="tgl">
                              <option value="">Jadwal Keberangkatan</option>
                              <?php
                              $current_date = new DateTime();
                              echo '<option value="' . $current_date->format( 'Ym' ) . '"';
                              echo ( !empty( $_REQUEST['tgl'] ) && $current_date->format( 'Ym' ) == $_REQUEST['tgl'] ) ? ' selected' : '';
                              echo '>' . $current_date->format( 'F Y' ) . '</option>';
                              $current_date->add(new DateInterval('P1M'));
                              echo '<option value="' . $current_date->format( 'Ym' ) . '"';
                              echo ( !empty( $_REQUEST['tgl'] ) && $current_date->format( 'Ym' ) == $_REQUEST['tgl'] ) ? ' selected' : '';
                              echo '>' . $current_date->format( 'F Y' ) . '</option>';
                              $current_date->add(new DateInterval('P1M'));
                              echo '<option value="' . $current_date->format( 'Ym' ) . '"';
                              echo ( !empty( $_REQUEST['tgl'] ) && $current_date->format( 'Ym' ) == $_REQUEST['tgl'] ) ? ' selected' : '';
                              echo '>' . $current_date->format( 'F Y' ) . '</option>';
                              $current_date->add(new DateInterval('P1M'));
                              echo '<option value="' . $current_date->format( 'Ym' ) . '"';
                              echo ( !empty( $_REQUEST['tgl'] ) && $current_date->format( 'Ym' ) == $_REQUEST['tgl'] ) ? ' selected' : '';
                              echo '>' . $current_date->format( 'F Y' ) . '</option>';
                              ?>
                            </select>
                        </div>
                        <input name="order_by" value="" type="hidden">
                    </div>
                    <!--

		    				-->
                    <!--<div class="blok_form lbr2">
		    					<div class="icon icon_bintang"><span><i></i></span></div>
		    					<div class="selek">
		    						<select class="input">
			    						<option value="">Pilih Hotel</option>
			    						<option value="">Bintang 5</option>
			    						<option value="">Bintang 4</option>
			    						<option value="">Bintang 3</option>
			    					</select>
			    				</div>
		    				</div>-->
                    <!--

		    				-->
                    <div class="blok_form bf_trans bf_cari">
                        <button type="submit" class="baten baten_icon"><i class="i_cari"></i> <span>Cari</span></button>
                    </div>
                    <!--

		    				-->
                    <div class="blok_form bf_trans bf_sj">
                        <a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>" class="baten baten_icon"><span>Semua Jadwal</span></a>
                    </div>

                    <div class="clr"></div>
                </form>
            </div>

            <div class="hju_pencarian" id="hasil_pencarian">
                <!--TAMBAHAN 09/04/16-->
                <div class="hju_p_hasilnotif">
                    <h4>Hasil Pencarian</h4>
<!--                     <p>3 jadwal keberangkatan ditemukan</p> -->
                    <div class="hju_p_hn_urutkan">
                        <div class="hju_p_hn_urutkan_text">Urutkan Berdasarkan</div>
                        <!--
		    					-->
                        <div class="form form_border hju_p_hn_form">
                            <form method="get" action="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>">
                                <div class="blok_form">
                                    <input name="paket" value="<?php echo ( !empty( $_GET['paket'] ) ) ? $_GET['paket'] : ''; ?>" type="hidden">
                                    <input name="tgl" value="<?php echo ( !empty( $_GET['tgl'] ) ) ? $_GET['tgl'] : ''; ?>" type="hidden">
                                    <div class="selek selek_no_ikon">
                                        <select onchange="this.form.submit()" class="input" name="order_by">
                                            <!-- <option value="ur_date" >Tanggal Keberangkatan Terbaru</option>
						    						<option value="ur_duration" >Durasi</option> -->
                                            <option value="ur_date_DESC">Tanggal Keberangkatan Terbaru</option>
                                            <option value="ur_date_ASC" selected="">Tanggal Keberangkatan Terdekat</option>
                                            <option value="ur_duration_DESC">Durasi Terlama</option>
                                            <option value="ur_duration_ASC">Durasi Pendek</option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="hju_p_lis">

                  <?php if( !empty( $_GET['paket'] ) || !empty( $_GET['tgl'] ) ) : ?>
                  
                    <?php
                    // WP_Query arguments
                    $args = array(
                      'post_type'              => array( 'mwt-umroh' ),
                      'post_status'            => array( 'publish' )
                    );
                  
                    $tax_query = array();
                    if( !empty( $_GET['paket'] ) ) {
                      $term = get_term_by( 'id', $_GET['paket'], 'umroh_category' );
                      $tax_query[] = array(
                        'taxonomy'  => 'umroh_category',
                        'field'     => 'id',
                        'terms'     => array( intval( $_GET['paket'] ) ),
                        'include_children' => true,
                        'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
                      );
                    }
                    if( !empty( $_GET['tgl'] ) ) {
                      $args['meta_name'] = 'tanggal_berangkat';
                      $args['meta_value'] = $_GET['tgl'];
                      $args['meta_compare'] = 'LIKE';
                    }
                    if( count( $tax_query ) > 0 ) {
                      $args['tax_query'] = array(
                        'relation' => 'AND', //(string) - 'AND' or 'OR'
                        $tax_query
                      );
                    }

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) { ?>
                      <!--DITEMUKAN-->
                      <table>
                          <thead>
                              <tr>
                                  <th class="hjupl_nonmobel">Berangkat</th>
                                  <th class="hjupl_nonmobel">Paket</th>
                                  <th class="hjupl_nonmobel">Durasi</th>
                                  <th class="hjupl_nonmobel">Shalat Jumat</th>
                                  <th class="hjupl_nonmobel">Provider</th>
                                  <th class="hjupl_nonmobel" colspan="2">Pesawat/Hotel</th>
                                  <th class="hjupl_nonmobel">Biaya</th>
                                  <th class="hjupl_nonmobel">&nbsp;</th>
                                  <th class="hjupl_nonmobel">&nbsp;</th>
                                  <th class="hjupl_mobel">Keterangan Paket</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php while ( $query->have_posts() ) {
                            $query->the_post();
                            get_template_part( 'template-parts/content', 'archive-umroh' );
                            } ?>
                          </tbody>
                        </table>
                  
                        <div class="hjupl_peging halaman_peging ">
                          <?php the_posts_pagination(); ?>
                          <script>
                            $(".nav-links .page-numbers").addClass('baten');
                          </script>
                          <br>
                        </div>
                  
                      <?php 
                    } else {
                      get_template_part( 'template-parts/content', 'archive-umroh-kosong' );
                    }
                    // Restore original Post Data
                    wp_reset_postdata();
                    ?>
                  
                  <?php else: ?>
                  
                    <?php if ( have_posts() ) : ?>

                      <!--DITEMUKAN-->
                      <table>
                          <thead>
                              <tr>
                                  <th class="hjupl_nonmobel">Berangkat</th>
                                  <th class="hjupl_nonmobel">Paket</th>
                                  <th class="hjupl_nonmobel">Durasi</th>
                                  <th class="hjupl_nonmobel">Shalat Jumat</th>
                                  <th class="hjupl_nonmobel">Provider</th>
                                  <th class="hjupl_nonmobel" colspan="2">Pesawat/Hotel</th>
                                  <th class="hjupl_nonmobel">Biaya</th>
                                  <th class="hjupl_nonmobel">&nbsp;</th>
                                  <th class="hjupl_nonmobel">&nbsp;</th>
                                  <th class="hjupl_mobel">Keterangan Paket</th>
                              </tr>
                          </thead>
                          <tbody>

                            <?php
                            /* Start the Loop */
                            while ( have_posts() ) : the_post();

                              /*
                               * Include the Post-Type-specific template for the content.
                               * If you want to override this in a child theme, then include a file
                               * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                               */
                              get_template_part( 'template-parts/content', 'archive-umroh' );
                              ?>

                            <?php endwhile; ?>

                          </tbody>
                        </table>

                      <div class="hjupl_peging halaman_peging ">
                        <?php the_posts_pagination(); ?>
                        <script>
                          $(".nav-links .page-numbers").addClass('baten');
                        </script>
                        <br>
                      </div>

                    <?php else : ?>

                        <?php get_template_part( 'template-parts/content', 'archive-umroh-kosong' ); ?>

                    <?php endif; ?>
                  
                  <?php endif; ?>

		    			</div>

		    		</div>

		    	</div>

	    	</div>

		</main><!-- #main -->
	</div><!-- #primary -->

  <script>
    $(function() {
      var nama_paket = $('select[name="paket"]').find(":selected").text();
      if( nama_paket !== '' && nama_paket !== 'Pilih Paket Umroh' ) {
        $("font.nama_paket_x").text(nama_paket);
      }
    });
  </script>

<?php
get_footer();
