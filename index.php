<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MJS Tour
 */

global $mwt_option;

get_header();
?>

	
		<div id="konten_isi" class="site-main batasna_kabeh konten_isi">
      
      <?php get_template_part( 'layouts/section', 'hero-cari-jadwal' ); ?>

      <?php get_template_part( 'layouts/section', 'selling-point' ); ?>
      
      <?php get_template_part( 'layouts/section', 'video' ); ?>

		</div><!-- #main -->
    
    <?php get_template_part( 'layouts/section', 'top-promo' ); ?>

    <?php get_template_part( 'layouts/section', 'testimonial' ); ?>

    <?php get_template_part( 'layouts/section', 'paket-umroh' ); ?>

    <?php get_template_part( 'layouts/section', 'terbukti-terpercaya' ); ?>
    
    <?php get_template_part( 'layouts/section', 'artikel' ); ?>

    <!--MENU UTAMA-->
    <script type="text/javascript">
    jQuery("document").ready(function($){
      var menu_utama_v2 = $('.menu_utama_v2');
      var menu_handap = $('.menu_handap');
      $(window).scroll(function () {
        if($(this).scrollTop() > 70) {
          menu_utama_v2.addClass('muv2_skrol');
        }else{
          menu_utama_v2.removeClass('muv2_skrol');
        }

        if($(this).scrollTop() > 320) {
          menu_handap.addClass('mh_skrol');
        }else{
          menu_handap.removeClass('mh_skrol');
        }
      }); 
    });
    </script>
    <!--MENU UTAMA-->

<?php
get_footer();
