<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MJS Tour
 */

global $mwt, $mwt_option;
?>

      <?php get_template_part( 'layouts/section', 'jadwal-umroh' ); ?>

	</div><!-- #content -->

  <!--FOOTER-->
  <footer class="batasna_kabeh">
      <div class="handap_lis">
          <div class="batasna_wrap">
              <div class="clr"></div>

              <div class="hl_sitemap">
                  <h4>Sitemap</h4>
                  <nav>
                      <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="">Beranda</a>
                      <a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>">Jadwal Umroh</a>
                      <a href="<?php echo get_post_type_archive_link( 'mwt-umroh' ); ?>">Umroh</a>
                      <a href="<?php echo get_post_type_archive_link( 'mwt-wisata-muslim' ); ?>">Wisata Muslim</a>
                      <a href="http://www.umrohbermakna.com/kontak-kami/">Kontak Kami</a>
                      <a href="http://www.umrohbermakna.com/faq/">FAQ</a>
                      <a href="<?php echo get_post_type_archive_link( 'testimonials' ); ?>">Testimonial</a>
                  </nav>
              </div>
              <div class="hl_sitemap">
                  <h4>Artikel Terkini</h4>
                  <nav>
                    <?php
                    if( !empty( $mwt_option['berita-category-1'] ) ) {
                      $term1 = get_term( intval( $mwt_option['berita-category-1'] ), 'category' );  
                      echo '<a href="' . esc_url( get_term_link( $term1 ) ) . '">' . $term1->name . '</a>';
                    }
                    if( !empty( $mwt_option['berita-category-2'] ) ) {
                      $term2 = get_term( intval( $mwt_option['berita-category-2'] ), 'category' );  
                      echo '<a href="' . esc_url( get_term_link( $term2 ) ) . '">' . $term2->name . '</a>';
                    }
                    if( !empty( $mwt_option['berita-category-3'] ) ) {
                      $term3 = get_term( intval( $mwt_option['berita-category-3'] ), 'category' );  
                      echo '<a href="' . esc_url( get_term_link( $term3 ) ) . '">' . $term3->name . '</a>';
                    }
                    ?>
                  </nav>
              </div>
              <div class="hl_sitemap">
                  <h4>Paket Umroh</h4>
                  <nav>
                      <?php
                      $terms = get_terms( 'umroh_category', array(
                          'hide_empty' => false,
                      ) );
                      foreach( $terms as $term ) {
                        echo '<a href="' . esc_url( add_query_arg( array( 'paket' => $term->term_id ), get_post_type_archive_link( 'mwt-umroh' ) ) ) . '" title="">' . $term->name . '</a>';
                      }
                      ?>
                      
                  </nav>
              </div>
              <div class="hl_sitemap">
                  <h4>Partners</h4>
                  <nav>
                  <?php
                  // WP_Query arguments
                  $args = array(
                    'post_type'         => array( 'partners' ),
                    'post_status'       => array( 'publish' ),
                    'nopaging'          => false,
                    'posts_per_page'    => 3
                  );

                  // The Query
                  $query = new WP_Query( $args );

                  // The Loop
                  if ( $query->have_posts() ) {
                    while ( $query->have_posts() ) {
                      $query->the_post(); ?>
                      <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" target="_blank" rel="nofollow"><?php the_title(); ?></a>
                      <?php
                    }
                  }
                  // Restore original Post Data
                  wp_reset_postdata();
                  ?>
                  </nav>
              </div>

              <div class="hl_layanan">
                  <h3><?php echo $mwt_option['statis-footer-contact-text']; ?></h3>
                  <div class="separator separator_hideung"><span></span></div>
                  <div class="clr"></div>

                  <div class="hl_layanan_telpon">
                      <p>TELEPON</p>
                      <p><?php echo $mwt_option['contact-phone']; ?></p>
                  </div>

                  <div class="hl_layanan_telpon hll_telepon_katuhu">
                      <p>HANDPHONE</p>
                      <p><?php echo $mwt_option['contact-mobile']; ?></p>
                  </div>

                  <div class="clr"></div>
              </div>

              <div class="clr"></div>

              <div class="hl_ikon"><i></i></div>

          </div>

          <div class="batasna_wrap hl_info">
            <p class="hli_pktk hli_pading">
                Anda butuh bantuan? <br>
                Whatsapp / Telp <a href="tel:<?php echo $mwt_option['contact-mobile']; ?> ?>"><strong><?php echo $mwt_option['contact-mobile']; ?></strong></a> 
              <?php if( !empty( $mwt_option['contact-bbm'] ) ) : ?>
                atau Invite BBM Kami di <strong><?php echo $mwt_option['contact-bbm']; ?></strong>
              <?php endif; ?>
              <?php if( !empty( $mwt_option['contact-email'] ) ) : ?>
                atau Email <a href="mailto:<?php echo $mwt_option['contact-email']; ?>"> <strong><?php echo $mwt_option['contact-email']; ?></strong></a>
              <?php endif; ?>
            </p>
            <?php if( !empty( $mwt_option['contact-address'] ) ) : ?>
              <div class="separator separator_abu"><span></span></div>
              <p><?php echo $mwt_option['contact-address']; ?></p>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['contact-address-2'] ) ) : ?>
              <div class="separator separator_abu"><span></span></div>
              <p><?php echo $mwt_option['contact-address-2']; ?></p>
            <?php endif; ?>
          </div>

          <div class="batasna_wrap hl_sosmed">
            <?php if( !empty( $mwt_option['social-fb'] ) ) : ?>
              <a target="_blank" href="<?php echo $mwt_option['social-fb']; ?>"><i class="fa fa-facebook"></i></a>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['social-twitter'] ) ) : ?>
              <a target="_blank" href="<?php echo $mwt_option['social-twitter']; ?>"><i class="fa fa-twitter"></i></a>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['social-googleplus'] ) ) : ?>
              <a target="_blank" href="<?php echo $mwt_option['social-googleplus']; ?>"><i class="fa fa-google-plus"></i></a>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['social-instagram'] ) ) : ?>
              <a target="_blank" href="<?php echo $mwt_option['social-instagram']; ?>"><i class="fa fa-instagram"></i></a>
            <?php endif; ?>
            <?php if( !empty( $mwt_option['social-youtube'] ) ) : ?>
              <a target="_blank" href="<?php echo $mwt_option['social-youtube']; ?>"><i class="fa fa-youtube"></i></a>
            <?php endif; ?>
          </div>

      </div>
      <!-- end section -->
      <!-- start footer -->
      <div class="handap_kopireg">
          <div class="batasna_wrap">
              <div class="clr"></div>

              <div class="hk_kenca">
                  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="Beranda" style="width:65px"><img src="<?php echo $mwt_option['mwt-logo']['url']; ?>" style="width:50px; height: 60px;" alt="<?php echo get_bloginfo( 'name' ); ?>"></a>
                  <p>Copyright &copy; <?php echo date("Y"); ?> <strong><?php echo get_bloginfo( 'name' ); ?>.</strong>
                      <br>All Rights Reserved.</p>
              </div>
            
              <!--
              <div class="hk_katuhu">
                  <span>Didukung oleh</span>
                  <a target="_blank" href="http://cisfullday.com/" rel="nofollow" title="Cisfullday"><img src="" title="CIS Fullday" alt="Logo CIS Fullday"></a>
                  <a target="_blank" href="" title=""><img src="" title="Madany Laundry" alt="Logo Madany Laundry"></a>
              </div>
              -->
              <div class="clr"></div>
          </div>
      </div>

  </footer>
  <!--FOOTER-->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
