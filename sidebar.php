<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MJS Tour
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>



<aside id="secondary" class="widget-area">
  <?php global $mwt_option; ?>
  <?php if( !empty( $mwt_option['banner-statis-samping']['url'] ) ) : ?>
  <a href="#" title="Sidebar Kanan List Artikel" class="hs_katuhu_banner"><img src="<?php echo $mwt_option['banner-statis-samping']['url']; ?>" style="width: 300px; height: 250px;" title="Sidebar List Artikel" alt="Sidebar List Artikel"></a>
  <?php endif; ?>
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside>
<!-- #secondary -->

